﻿using UnityEngine;
using System.Collections;

public class RateUs : MonoBehaviour {

    public static RateUs Instance;

    public GameObject rateUsPanelGO;
    public GameObject backroundGO;
    public Animator yesBtnAnimator;
    [Space]
    public string myAppleIDForRating = "1183768324";
    public bool displayRateUsPanel = true;

    void Awake()
    {
        Instance = this;
    }

    public void OpenRateUsPanel()
    {
        if (displayRateUsPanel)
        {
            if (PlayerPrefs.GetInt("Rated") == 0)
            {
                PlayerPrefs.SetInt("Rated", 1);
                UpgradesShopUI.Instance.OnClick_CloseUpgradesPanel();
                Invoke("OpenRatePanelDelay", 0.5f);
            }
        }
    }

    void RequestBanner()
    {
        AdsInterstitialManager.Instance.RequestAdmobSmartBanner();
    }

    void OpenRatePanelDelay()
    {
        rateUsPanelGO.SetActive(true);
        yesBtnAnimator.SetTrigger("Bounce");
        backroundGO.SetActive(true);
        rateUsPanelGO.GetComponent<Animator>().SetTrigger("Appear");
        CatBubble.Instance.DisplayCatBubble("5 Stars help us to keep the updates coming", true);
    }

    public void OnClick_YesRate()
    {
        AnalyticsManager.Instance.UserPressYesRateOnHouseX(UpgradesShopUI.Instance.currentZone.zoneIndex);

#if UNITY_ANDROID
        Application.OpenURL("market://details?id=" + Application.bundleIdentifier);

#endif
#if UNITY_IPHONE
        string appStoreUrl = "http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=" +
            myAppleIDForRating + "&pageNumber=0&sortOrdering=2&type=Purple+Software&mt=8";

        Application.OpenURL(appStoreUrl);
#endif

        backroundGO.SetActive(false);
        rateUsPanelGO.SetActive(false);
        CatBubble.Instance.CloseCatBubble();
        UIManager.Instance.OnPanelOpen(false);
        Invoke("RequestBanner", 5);
    }

    public void OnClick_NoRate()
    {
        backroundGO.SetActive(false);
        CatBubble.Instance.CloseCatBubble();
        UIManager.Instance.OnPanelOpen(false);
        Invoke("HideRatePanel", 0.5f);
        Invoke("RequestBanner", 5);
    }

    void HideRatePanel()
    {
        rateUsPanelGO.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
            OpenRateUsPanel();
    }
}
