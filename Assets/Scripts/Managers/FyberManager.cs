﻿using UnityEngine;
using System.Collections;
using FyberPlugin;

public class FyberManager : MonoBehaviour
{
    public static FyberManager Instance;

    public string appId = "1246";
    public string securityToken = "7c7dd1c881efa94ae8df62dacf48a414";
    public UnityEngine.UI.Text debugLabel;
    public GameObject rewardPanelGO;
    public UnityEngine.UI.Text rewardLabel;
    [HideInInspector]
    public bool offerWallAvailable;

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
#if UNITY_ANDROID
        StartFyber();
#endif
    }

    /// <summary>
    /// Called on start (android),
    /// either from ParseDatabaseManager after succesfull parsing (iOS)
    /// </summary>
    public void StartFyber()
    {

#if UNITY_IPHONE
        appId = "30577";
        securityToken = "ea470de119264e4833972ae895f3697e";
#endif

        Settings settings = Fyber.With(appId)
        // optional chaining methods
        //.WithUserId(userId)
        //.WithParameters(dictionary)
        .WithSecurityToken(securityToken)
        //.WithManualPrecaching()
        .Start();

        settings.NotifyUserOnReward(true);

        VirtualCurrencyRequester virtualCurrencyRequester = VirtualCurrencyRequester.Create()
        // optional method chaining
        //.AddParameter("key", "value")
        //.AddParameters(dictionary)
        // Overrideing currency Id
        //.ForCurrencyId(currencyId)
        // Changing the GUI notification behaviour for when the user is rewarded
        .NotifyUserOnReward(true)
        // you don't need to add a callback if you are using delegates
        //.WithCallback(vcsCallback)
        ;

        Invoke("RequestOfferWall", 3);
    }

    void OnEnable()
    {
        FyberCallback.NativeError += OnNativeExceptionReceivedFromSDK;
        // Ad availability
        FyberCallback.AdAvailable += OnAdAvailable;
        FyberCallback.AdNotAvailable += OnAdNotAvailable;
        FyberCallback.RequestFail += OnRequestFail;
        FyberCallback.AdStarted += OnAdStarted;
        FyberCallback.AdFinished += OnAdFinished;
        FyberCallback.VirtualCurrencySuccess += OnCurrencyResponse;
        FyberCallback.VirtualCurrencyError += OnCurrencyErrorResponse;
    }

    void OnDisable()
    {
        FyberCallback.NativeError -= OnNativeExceptionReceivedFromSDK;
        // Ad availability
        FyberCallback.AdAvailable -= OnAdAvailable;
        FyberCallback.AdNotAvailable -= OnAdNotAvailable;
        FyberCallback.RequestFail -= OnRequestFail;
        FyberCallback.AdStarted -= OnAdStarted;
        FyberCallback.AdFinished -= OnAdFinished;
        FyberCallback.VirtualCurrencySuccess -= OnCurrencyResponse;
        FyberCallback.VirtualCurrencyError -= OnCurrencyErrorResponse;
    }

    public void OnNativeExceptionReceivedFromSDK(string message)
    {
        //handle exception
        debugLabel.text += message;
    }

    Ad ofwAd;

    private void OnAdAvailable(Ad ad)
    {
        switch (ad.AdFormat)
        {
            case AdFormat.OFFER_WALL:
                ofwAd = ad;
                offerWallAvailable = true;
                break;
        }
    }

    public void RequestOfferWall()
    {
        OfferWallRequester.Create()
    // optional method chaining
    //.AddParameter("key", "value")
    //.AddParameters(dictionary)
    //.WithPlacementId(placementId)
    // configure ofw behaviour:
    .CloseOnRedirect(true)
    // you don't need to add a callback if you are using delegates
    //.WithCallback(requestCallback)
    //requesting the ad
    .Request();
    }


    private void OnAdNotAvailable(AdFormat adFormat)
    {
        switch (adFormat)
        {
            case AdFormat.OFFER_WALL:
                ofwAd = null;
                break;
                // handle other ad formats if needed
        }
    }

    private void OnRequestFail(RequestError error)
    {
        // process error
        UnityEngine.Debug.Log("OnRequestError: " + error.Description);
        debugLabel.text += error.Description;
    }

    public void ShowOfferWall()
    {
        if (ofwAd != null)
        {
            ofwAd.Start();
            ofwAd = null;
        }
    }

    private void OnAdStarted(Ad ad)
    {
        // this is where you mute the sound and toggle buttons if necessary
        SoundManager.Instance.MuteSoundsAndMusic(true);

        switch (ad.AdFormat)
        {
            case AdFormat.OFFER_WALL:
                ofwAd = null;
                break;
                //handle other ad formats if needed
        }
    }

    private void OnAdFinished(AdResult result)
    {
        SoundManager.Instance.MuteSoundsAndMusic(false);

        switch (result.AdFormat)
        {
            case AdFormat.OFFER_WALL:
                UnityEngine.Debug.Log("Ofw closed with result: " + result.Status +
                "and message: " + result.Message);
                debugLabel.text += "Ofw closed with result: " + result.Status +
                "and message: " + result.Message;
                break;
        }
    }

    public void OnCurrencyResponse(VirtualCurrencyResponse response)
    {
        debugLabel.text += "delta of coins " + response.DeltaOfCoins.ToString();

        UnityEngine.Debug.Log("Delta of coins: " + response.DeltaOfCoins.ToString() +
                                ". Transaction ID: " + response.LatestTransactionId +
                                ".\nCurreny ID: " + response.CurrencyId +
                                ". Currency Name: " + response.CurrencyName);

        rewardPanelGO.SetActive(true);
        rewardLabel.text = response.DeltaOfCoins.ToString();
        GameManager.Instance.Gems += (int)response.DeltaOfCoins;
        AnalyticsManager.Instance.GetFyberReward();
    }

    public void OnCurrencyErrorResponse(VirtualCurrencyErrorResponse vcsError)
    {
        debugLabel.text = System.String.Format("Delta of coins request failed.\n" +
                            "Error Type: {0}\nError Code: {1}\nError Message: {2}",
                            vcsError.Type, vcsError.Code, vcsError.Message);

        UnityEngine.Debug.Log(System.String.Format("Delta of coins request failed.\n" +
                            "Error Type: {0}\nError Code: {1}\nError Message: {2}",
                            vcsError.Type, vcsError.Code, vcsError.Message));
    }



    public void RequestVirtualCurrencyWithDelay()
    {
        Invoke("RequestVirtualCurrency", 5);
    }

    void RequestVirtualCurrency()
    {
        VirtualCurrencyRequester.Create()
        // optional method chaining
        //.AddParameter("key", "value")
        //.AddParameters(dictionary)
        // Overrideing currency Id
        //.ForCurrencyId(currencyId)
        // Changing the GUI notification behaviour for when the user is rewarded
        //.NotifyUserOnReward(true)
        // you don't need to add a callback if you are using delegates
        .WithCallback(vcsCallback)
        // requesting vcs
        .Request();
    }

    CustomVirtualCurrencyCallback vcsCallback = new CustomVirtualCurrencyCallback();

    public class CustomVirtualCurrencyCallback : VirtualCurrencyCallback
    {
        public void OnRequestError(RequestError error)
        {
            UnityEngine.Debug.Log("OnRequestError: " + error.Description);
        }

        public void OnError(VirtualCurrencyErrorResponse vcsError)
        {
            UnityEngine.Debug.Log(string.Format("Delta of coins request failed.\n" +
                            "Error Type: {0}\nError Code: {1}\nError Message: {2}",
                            vcsError.Type, vcsError.Code, vcsError.Message));
        }

        public void OnSuccess(VirtualCurrencyResponse response)
        {
            UnityEngine.Debug.Log("virtual currency response success. Delta of coins received: " + response.DeltaOfCoins);

            if (response.DeltaOfCoins > 0)
            {
                FyberManager.Instance.rewardPanelGO.SetActive(true);
                FyberManager.Instance.rewardLabel.text = response.DeltaOfCoins.ToString();
                GameManager.Instance.Gems += (int)response.DeltaOfCoins;
                AnalyticsManager.Instance.GetFyberReward();
            }
        }
    }
}
