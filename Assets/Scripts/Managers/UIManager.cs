﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening; // let's use DOTween plugin to make UI elements half transparancy

/// <summary>
/// Display UI on screen (food, minions, buttons, labels and so on...)
/// </summary>
public class UIManager : MonoBehaviour {

    public static UIManager Instance;

    public Text foodLabel;

    public Text gemsLabel;
    public Image[] minionPricesImg;
    public Text minionPriceLabel;
    public List<ZoneUI> zoneUI;
    public GameObject foodPrincessToEatUI;
    public Transform mainCanvasTr;
    public Transform worldCanvasTr;
    public Text minionsCountLabel;
    public GameObject inGamePanel;
    public GameObject settingsPanel;
    public GameObject developerPanel;
    public GameObject zoneGemPrefab;
    public GameObject minus1Prefab;
    public GameObject[] unlockZonesGO;
    public GameObject restoreGO;
    public GameObject quitPanelGO;
    public LikeOnFacebook likeOnFacebook;
    public GameObject[] allPanelsGO;

    private bool _anyPanelOpened;
    private bool _isBorn;
    
    void Awake()
    {
        Instance = this;
    }

    /// <summary>
    /// Display current food on screen
    /// </summary>
    /// <param name="food"></param>
    public void UpdateFood(int food)
    {
        //foodLabel.text = food.ToString();
        foodLabel.text = Utilities.NiceFormat(food);
    }

    public void UpdateGems(int gems)
    {
        //gemsLabel.text = gems.ToString();
        gemsLabel.text = Utilities.NiceFormat(gems);
    }

    /// <summary>
    /// Display current minion price and make labels half transparent if player haven't enough money (food)
    /// </summary>
    public void UpdateMinionPrice()
    {
        minionPriceLabel.text = Utilities.NiceFormat(Princess.Instance.CurrentMinionPrice());
        if (!_isBorn)
        {
            if (Princess.Instance.MinionAvailable())
            {
                for (int i = 0; i < minionPricesImg.Length; i++)
                {
                    minionPricesImg[i].DOFade(1, 0);
                }
                minionPriceLabel.DOFade(1, 0);
            }
            else
            {
                for (int i = 0; i < minionPricesImg.Length; i++)
                {
                    minionPricesImg[i].DOFade(0.5f, 0);
                }
                minionPriceLabel.DOFade(0.5f, 0);
            }
        }
    } 

    public void FishBubbleFadeOn(float fadeStrength)
    {
        if (fadeStrength == 0.5f) _isBorn = true;
        else _isBorn = false;

        for (int i = 0; i < minionPricesImg.Length; i++)
        {
            minionPricesImg[i].DOFade(fadeStrength, 0);
        }
        minionPriceLabel.DOFade(fadeStrength, 0);

        UpdateMinionPrice();
    }

    /// <summary>
    /// We need to show how much player spent food after click on princess and create new minion
    /// </summary>
    public void DisplayMinusFoodOnPrincess(int minusFood)
    {
        float rndFoodPos = Random.Range(2, 3.5f);
        //GameObject go = Instantiate(foodPrincessToEatUI, new Vector3(Princess.Instance.transform.position.x + rndFoodPos,
        //    Princess.Instance.transform.position.y, Princess.Instance.transform.position.z),
        //    Quaternion.identity) as GameObject;
        GameObject go = Lean.LeanPool.Spawn(foodPrincessToEatUI, new Vector3(Princess.Instance.transform.position.x + rndFoodPos,
            Princess.Instance.transform.position.y, Princess.Instance.transform.position.z),
            Quaternion.identity) as GameObject;

        //go.transform.parent = worldCanvasTr;
        go.transform.SetParent(worldCanvasTr);
        go.transform.localScale = new Vector3(1, 1, 1);
        go.GetComponentInChildren<Text>().text = "-" + Utilities.NiceFormat(minusFood);
        RectTransform rt = go.GetComponent<RectTransform>();
        rt.DOLocalMoveY(rt.localPosition.y + 85, 1);
        //Destroy(go, 1);
        Lean.LeanPool.Despawn(go, 1);
    }

    /// <summary>
    /// Allow us to show or hide buttons for each zone (send minions, add minion).
    /// </summary>
    /// <param name="zone"></param>
    /// <param name="state"></param>
    /// <param name="level"></param>
    /// <param name="foodToUpgrage"></param>
    // Display small food UI, when princess is eating (+10 food label, moving up and dissapearing)
    public void DisplayFoodForPrincessToEat(int food, Vector3 spawnPos)
    {
        GameObject go = Lean.LeanPool.Spawn(foodPrincessToEatUI, new Vector3(spawnPos.x + 2, spawnPos.y + 2, spawnPos.z),
            Quaternion.identity) as GameObject;
        //go.transform.parent = worldCanvasTr;
        go.transform.SetParent(worldCanvasTr);
        go.transform.localScale = new Vector3(0.75f, 0.75f, 0.75f);
        go.GetComponentInChildren<Text>().text = "+" + food.ToString();
        RectTransform rt = go.GetComponent<RectTransform>();
        rt.DOLocalMoveY(rt.localPosition.y + 85, 1);
        GameManager.Instance.Food += food;
        Princess.Instance.AnimateOpeningMount();
        Lean.LeanPool.Despawn(go, 1);
        AchievementsManager.Instance.SendAchievement(AchievementName.FoodMagnat, food, AchievementType.Countable);
    }

    /// <summary>
    /// Update minions count in the right upper corner of screen
    /// </summary>
    /// <param name="count"></param>
    public void UpdateMinionsCount(int count)
    {
        minionsCountLabel.text = count.ToString();
    }

    void Update()
    {
        DisplayNextZone();

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            quitPanelGO.SetActive(!quitPanelGO.activeSelf);
        }
        if(Input.GetKeyDown(KeyCode.S))
        {
            OnPanelOpen(true);
            settingsPanel.SetActive(true);
        }
    }

    public void OnCLick_QuitYes()
    {
        Application.Quit();
    }

    public void OnClick_QuitNo()
    {
        quitPanelGO.SetActive(false);
    }

    void DisplayNextZone()
    {
        if (GameManager.Instance.NextZoneIndex() != 0 && GameManager.Instance.NextZoneIndex() != 7 && !EpicShopManager.Instance.superModeActivated)
        {
            zoneUI[GameManager.Instance.NextZoneIndex()].zoneButtonGO.SetActive(true);
            zoneUI[GameManager.Instance.NextZoneIndex()].zonePriceText.text = Utilities.NiceFormat(GameManager.Instance.PriceToUnlockNextZone());
            unlockZonesGO[GameManager.Instance.NextZoneIndex()].SetActive(true);
        }
    }

    public void HideUnlockZoneButton(int index)
    {
        zoneUI[index].zoneButtonGO.SetActive(false);
    }

    int testPressSettingsCounter = 0;
    public void OnClick_SettingsOpen(bool open)
    {
        settingsPanel.SetActive(open);

        if (open)
            settingsPanel.GetComponent<Animator>().SetTrigger("Appear");
        else
            settingsPanel.GetComponent<Animator>().SetTrigger("Dissapear");

        SoundManager.Instance.PlaySound(Sounds.Panel);
        _anyPanelOpened = open;
    }

    public void OnClick_DeveloperPanelOpen(bool open)
    {
        testPressSettingsCounter++;
        if (testPressSettingsCounter > 10)
        {
            OnPanelOpen(open);
            developerPanel.SetActive(open);
#if UNITY_ANDROID
            if (open)
            {
                restoreGO.SetActive(false);
            }
#endif
            SoundManager.Instance.SetSoundsMuteSettings();
        }
    }

    public bool IsSettingsPanelOpened()
    {
        return settingsPanel.activeSelf;
    }

    public void DisplayGem(Vector3 zonePos, int currentValue)
    {
        GameObject gemGO = Lean.LeanPool.Spawn(zoneGemPrefab, zonePos, Quaternion.identity) as GameObject;
        gemGO.GetComponentInChildren<Text>().text = currentValue.ToString();
        //gemGO.transform.parent = worldCanvasTr;
        gemGO.transform.SetParent(worldCanvasTr);
        gemGO.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        RectTransform rt = gemGO.GetComponent<RectTransform>();
        rt.DOLocalMoveY(rt.localPosition.y + 85, 1);
        Lean.LeanPool.Despawn(gemGO, 1);
    }

    public void DisplayMinus1(Vector3 pos)
    {
        GameObject go = Lean.LeanPool.Spawn(minus1Prefab, pos, Quaternion.identity) as GameObject;
        //go.transform.parent = mainCanvasTr;
        go.transform.SetParent(mainCanvasTr);
        go.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
        RectTransform rt = go.GetComponent<RectTransform>();
        rt.DOLocalMoveY(rt.localPosition.y + 85, 1);
        Lean.LeanPool.Despawn(go, 1);
    }

    public void OnPanelOpen(bool open)
    {
        _anyPanelOpened = open;
    }

    public bool AnyPanelOpened()
    {
        return _anyPanelOpened;
    }

    public void CloseAllPanels()
    {
        for (int i = 0; i < allPanelsGO.Length; i++)
        {
            allPanelsGO[i].SetActive(false);
        }
    }
}
