﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.iOS;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    void Awake()
    {
        Instance = this;

    }

    private int _food = 100;  // 100 by default
    /// <summary>
    /// Let's make food as property - every time when food will be changed,
    /// all text objects in game will be updated automatically.
    /// </summary>
    public int Food   
    {
        get
        {
            return _food;
        }
        set
        {
            _food = value;
            UIManager.Instance.UpdateFood(_food);
            UIManager.Instance.UpdateMinionPrice();
            UpgradesShopUI.Instance.CheckNotEnoughFoodNotification();

            if (_food >= zones[1].foodToUnlockZone)
            {
                if(!enoughMoneyForHouse2UnlockAnimator.GetCurrentAnimatorStateInfo(0).IsName("EnoughMoneyForHouse2"))
                    enoughMoneyForHouse2UnlockAnimator.SetTrigger("Start");
            }
            else
                enoughMoneyForHouse2UnlockAnimator.SetTrigger("Stop");
        }
    }

    [Header("set to 0 before release!")]
    public int testFood;

    private int _gems = 0;
    public int Gems
    {
        get
        {
            return _gems;
        }
        set
        {
            _gems = value;
            UIManager.Instance.UpdateGems(_gems);
            UpgradesShopUI.Instance.CheckNotEnoughGemsNotification();        
        }
    }

    [Header("set to 0 before release!")]
    public int testGems = 0;

    [Header("set to false before release!")]
    public bool skipTutorial;

    [Header("set to false before release!")]
    public bool skipLoading;

    [Space]
    public int startFood;
    public int startGems;

    public Animator enoughMoneyForHouse2UnlockAnimator;

    [Header("must be true before release!")]
    public bool playLogo;

    public bool dailyGiftInsteadOfBonus = true;

    [Space]
    public float tappsZonesSpeed = 1;

    void Start()
    {
        // if iPad or iPhone 4
#if UNITY_IPHONE
        if ((Device.generation.ToString()).IndexOf("iPad") > -1 || Device.generation == DeviceGeneration.iPhone4 || Device.generation == DeviceGeneration.iPhone4S)
        {
            Camera.main.orthographicSize = 5.5f;
        }
#endif

#if UNITY_ANDROID
        if (Camera.main.aspect >= 1.7)
        {
            Debug.Log("16:9");
        }
        else if (Camera.main.aspect >= 1.5)
        {
            Debug.Log("3:2");
        }
        else
        {
            Debug.Log("4:3");
            Camera.main.orthographicSize = 5.7f;
        }
#endif

        Application.targetFrameRate = 40;

        if (testFood != 0)
        Food += testFood;

        if (testGems != 0)
            Gems += testGems;
    }

    public void SetStartFoodAndGems()
    {
        Food = startFood;
        Gems = startGems;
    }

    [Space]
    public List<Zone> zones;

    public int NextZoneIndex()
    {
        for (int i = 0; i < zones.Count; i++)
        {
            if (!zones[i].activated)
                return i;
			
			if(i == 6) return 7;
        }
        return 0;
    }

    public bool EnoughtFoodToUnlockNextZone()
    {
        return Food >= zones[NextZoneIndex()].foodToUnlockZone;
    }

    public int PriceToUnlockNextZone()
    {
        return zones[NextZoneIndex()].foodToUnlockZone;
    }
}