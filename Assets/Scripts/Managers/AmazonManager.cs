﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmazonManager : MonoBehaviour {

    public static AmazonManager Instance;

    public bool amazonEnabled;
    public RateUs rateUs;
    public RescinManager rescinManager;
    public Sprite[] iconSpr;
    public NativeShare nativeShare;
    public ParseDatabaseManager parseDatabaseManager;
    public Purchaser purchaser;
    public UnityEngine.UI.Text gems50Title;

    private void Start()
    {
        Instance = this;
    }

    [ExecuteInEditMode]
    public void EnableAmazon(bool enable)
    {
        amazonEnabled = enable;

        rateUs.displayRateUsPanel = !enable;

        nativeShare.attachLinkToWebsite = !enable;

        parseDatabaseManager.beckendEnabled = !enable;

        if (enable)
        {
            string s = purchaser.kProductIDPack1.Replace("nomnom.", "nomnom.underground.");
            purchaser.kProductIDPack1 = s;
            s = purchaser.kProductIDPack2.Replace("nomnom.", "nomnom.underground.");
            purchaser.kProductIDPack2 = s;
            s = purchaser.kProductIDPack3.Replace("nomnom.", "nomnom.underground.");
            purchaser.kProductIDPack3 = s;
            s = purchaser.kProductIDPack4.Replace("nomnom.", "nomnom.underground.");
            purchaser.kProductIDPack4 = s;
            s = purchaser.kProductIDPack5.Replace("nomnom.", "nomnom.underground.");
            purchaser.kProductIDPack5 = s;
        }
        else
        {
            string s = purchaser.kProductIDPack1.Replace("nomnom.underground.", "nomnom.");
            purchaser.kProductIDPack1 = s;
            s = purchaser.kProductIDPack2.Replace("nomnom.underground.", "nomnom.");
            purchaser.kProductIDPack2 = s;
            s = purchaser.kProductIDPack3.Replace("nomnom.underground.", "nomnom.");
            purchaser.kProductIDPack3 = s;
            s = purchaser.kProductIDPack4.Replace("nomnom.underground.", "nomnom.");
            purchaser.kProductIDPack4 = s;
            s = purchaser.kProductIDPack5.Replace("nomnom.underground.", "nomnom.");
            purchaser.kProductIDPack5 = s;
        }

        Debug.Log("amazon enable: " + amazonEnabled);
    }
}
