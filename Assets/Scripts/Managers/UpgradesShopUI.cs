﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public enum UpgradeType
{
    AddWorker,
    IncHouseLimit,
    FoodPrice,
    WalkingSpeed
}

[System.Serializable]
public class UpgradeLevel
{
    public UpgradeType type;
    public GameObject foodBtnGO;
    public GameObject gemsBtnGO;
}

public class UpgradesShopUI : MonoBehaviour
{

    #region Variables

    public static UpgradesShopUI Instance;

    [HideInInspector]
    public Zone currentZone;
    public Zone defaultZone; // zone 0 - will be default zone

    public GameObject upgradesShopUIPanel; //inGameUIPanel;

    public Text addMinionsFoodLabel, addMinionsGemsLabel;
    public Text foodPriceValueFoodLabel, foodPriceValueGemsLabel;
    public Text increaseHouseLimitFoodLabel, increaseHouseLimitGemsLabel;
    public Text findGemsChanceFoodLabel, findGemsChangeGemsLabel;
    public Text walkingSpeedFoodLabel, walkingSpeedGemsLabel;

    public GameObject findGemsNotMaximumStuff, findGemsMaximumStuff;
    public GameObject[] zoneIsFull;
    public ScrollSnapRect scrollSnapRect;

    public UpgradesBtnsAnimators addWorkerAnims, increaseHousesAnims, foodPriceValuesAnims,
        findGemsAnims, speedAnims;

    // let's hide add worker buttons if minions count > 15, 20 and so on
    public GameObject[] addWorkerButtonsStuff;

    #region increse house button animation variables
    public Animator incHouseBtnAnim; 
    private int bounceIncBtnState = Animator.StringToHash("Base Layer.Bounce");
    private int idleIncBtnState = Animator.StringToHash("Base Layer.incHouseBtnIdle");
    private bool _houseFullFirstTimeHaveEnoughFoodFlag;
    #endregion

    #region Notification Variables

    public GameObject notificationIcon;
    public Text notificationLabel;
    private bool _houseLimitTriedToClickFlag;
    private int _notEnoughFood, _notEnoughGems;

    #endregion

    public GameObject addWorkerVideoBtnGO, incHouseLimitVideoBtnGO, foodPriceVideoBtnGO,
        incFindGemsChanceVideoBtnGO, walkingSpeedVideoBtnGO;

    public List<UpgradeLevel> upgradeLevels;

    public Animator addWorkVideoBtnHeartBeatAnimator;

    public Text minionsCountLabel;

    public GameObject addWorkerVideoBtnGO2;

    public GameObject[] itemsToDisplayOnReach10Minions; // food, luck, speed


    #endregion

    void Awake()
    {
        Instance = this;
    }

    #region Buttons Handlers

    public void OnClick_OpenUpgrades(Zone zone)
    {
        if (!Tutorial.Instance.activated)
        {
            OpenUpgrades(zone);
        }
        else
        {
            if (Tutorial.Instance.stepsCounter == 5)
                OpenUpgrades(zone);
        }
    }

    public void OnClick_OpenUpgradesFromPage()
    {
        if (currentZone == null)
            currentZone = defaultZone;

        OnClick_OpenUpgrades(currentZone);
        Shop.Instance.OnClick_OpenGemsShop(false);
        UIManager.Instance.OnPanelOpen(true);
    }

    public void OnClick_CloseUpgradesPanel()
    {
        UIManager.Instance.OnPanelOpen(false);
        SoundManager.Instance.PlaySound(Sounds.ClosePanel);
        upgradesShopUIPanel.SetActive(false);
        ClearNotificationLabel();
    }

    void OpenUpgrades(Zone zone)
    {
        currentZone = zone;
        upgradesShopUIPanel.SetActive(true);
        UIManager.Instance.OnPanelOpen(true);

        findGemsNotMaximumStuff.SetActive(!currentZone.isFindGemsMaximum);
        findGemsMaximumStuff.SetActive(currentZone.isFindGemsMaximum);

        UpdatePricesDisplay();

        if (Tutorial.Instance.activated && Tutorial.Instance.stepsCounter == 5)
        {
            Tutorial.Instance.HideTutorialStep();
            Tutorial.Instance.ShowTutorialStep();
        }

        SoundManager.Instance.PlaySound(Sounds.Panel);
        scrollSnapRect.BackToFirstPage();
        ExclamationMarks.Instance.OnClick_OpenUpgrades();
        addWorkVideoBtnHeartBeatAnimator.SetTrigger("Start");
        currentZone.UpdateUpgradesLevelLabels();
        SponsoredAppsManager.Instance.SetSponsoredAppImgToTheShop();
        minionsCountLabel.text = currentZone.minionsInZone.ToString() + "/100";
        addWorkerVideoBtnGO2.GetComponent<Animator>().SetTrigger("Bounce");
    }

    #endregion

    public void UpdatePricesDisplay()
    {
        findGemsChanceFoodLabel.text = Utilities.NiceFormat(currentZone.findGemsChancePrice.food);
        findGemsChangeGemsLabel.text = Utilities.NiceFormat(currentZone.findGemsChancePrice.gems);

        if (currentZone.ZoneIsFull())
        {
            zoneIsFull[currentZone.zoneIndex].SetActive(true);
        }
        else
        {
            zoneIsFull[currentZone.zoneIndex].SetActive(false);
        }

        if(currentZone.addWorkerPrice.currLevel >= currentZone.addWorkerPrice.levelMax)
        {
            addWorkerAnims.foodAnimator.gameObject.SetActive(false);
        }
        else
        {
            addWorkerAnims.foodAnimator.gameObject.SetActive(true);
            addWorkerAnims.foodAnimator.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
            addMinionsFoodLabel.text = Utilities.NiceFormat(currentZone.addWorkerPrice.food);
        }

        if (currentZone.increaseHouseLimitPrice.currLevel >= currentZone.increaseHouseLimitPrice.levelMax)
        {
            increaseHousesAnims.foodAnimator.gameObject.SetActive(false);
            increaseHousesAnims.gemsAnimator.gameObject.SetActive(false);
        }
        else
        {
            //if (currentZone.minionsInZone < currentZone.minionsCountLimit)
            //{
                increaseHousesAnims.foodAnimator.gameObject.SetActive(true);
                increaseHousesAnims.gemsAnimator.gameObject.SetActive(true);
                increaseHousesAnims.foodAnimator.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
                increaseHousesAnims.gemsAnimator.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
                increaseHouseLimitFoodLabel.text = Utilities.NiceFormat(currentZone.increaseHouseLimitPrice.food);
                increaseHouseLimitGemsLabel.text = Utilities.NiceFormat(currentZone.increaseHouseLimitPrice.gems);
            //}
        }

        if (currentZone.foodPriceValue.currLevel >= currentZone.foodPriceValue.levelMax)
        {
            foodPriceValuesAnims.foodAnimator.gameObject.SetActive(false);
            foodPriceValuesAnims.gemsAnimator.gameObject.SetActive(false);
        }
        else
        {
            foodPriceValuesAnims.foodAnimator.gameObject.SetActive(true);
            foodPriceValuesAnims.gemsAnimator.gameObject.SetActive(true);
            foodPriceValuesAnims.foodAnimator.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
            foodPriceValuesAnims.gemsAnimator.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
            foodPriceValueFoodLabel.text = Utilities.NiceFormat(currentZone.foodPriceValue.food);
            foodPriceValueGemsLabel.text = Utilities.NiceFormat(currentZone.foodPriceValue.gems);
        }

        if(currentZone.walkingSpeedPrice.currLevel >= currentZone.walkingSpeedPrice.levelMax)
        {
            speedAnims.foodAnimator.gameObject.SetActive(false);
            speedAnims.gemsAnimator.gameObject.SetActive(false);
        }
        else
        {
            speedAnims.foodAnimator.gameObject.SetActive(true);
            speedAnims.gemsAnimator.gameObject.SetActive(true);
            speedAnims.foodAnimator.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
            speedAnims.gemsAnimator.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
            walkingSpeedFoodLabel.text = Utilities.NiceFormat(currentZone.walkingSpeedPrice.food);
            walkingSpeedGemsLabel.text = Utilities.NiceFormat(currentZone.walkingSpeedPrice.gems);
        }

        minionsCountLabel.text = currentZone.minionsInZone.ToString() + "/100";

        if(currentZone.limitesIndex >= 1)
        {
            for (int i = 0; i < itemsToDisplayOnReach10Minions.Length; i++)
            {
                itemsToDisplayOnReach10Minions[i].SetActive(true);
            }
        }
        else
        {
            for (int i = 0; i < itemsToDisplayOnReach10Minions.Length; i++)
            {
                itemsToDisplayOnReach10Minions[i].SetActive(false);
            }
        }

        //if(currentZone.minionsInZone >= currentZone.minionsCountLimit)
        //{
        //    for (int i = 0; i < addWorkerButtonsStuff.Length; i++)
        //    {
        //        addWorkerButtonsStuff[i].SetActive(false);
        //    }
        //}
        //else
        //{
        //    for (int i = 0; i < addWorkerButtonsStuff.Length; i++)
        //    {
        //        addWorkerButtonsStuff[i].SetActive(true);
        //    }
        //}
    }

    #region Upgrades Buttons

    public void OnClick_AddWorker(bool isGems)
    {
        if (currentZone.ZoneIsFull())
        {
            notificationIcon.SetActive(true);
            notificationLabel.text = "House limit reached, please upgrade it!";
            if(!PlayerPrefs.HasKey("_houseFullFirstTimeHaveEnoughFoodFlag"))
            {
                _houseFullFirstTimeHaveEnoughFoodFlag = true;
            }
        }

        if (!isGems)
        {
            if(GameManager.Instance.Food >= (int)currentZone.addWorkerPrice.food)
            {
                //if(!currentZone.ZoneIsFull() && MinionsManager.Instance.TotalMinions() > currentZone.minionsInZone)
                if (!currentZone.ZoneIsFull())
                {
                    addWorkerAnims.foodAnimator.SetTrigger("Pressed");
                    GameManager.Instance.Food -= (int)currentZone.addWorkerPrice.food;
                    currentZone.OnClick_AddMinionToZone();
                    IncUpgradeLevel(upgradeLevels[0]);
                    SoundManager.Instance.PlaySound(Sounds.Upgrade);
                    minionsCountLabel.text = currentZone.minionsInZone.ToString() + "/100";
                }
            }
            else
            {
                addWorkerAnims.foodAnimator.SetTrigger("NotEnough");
                notificationIcon.SetActive(true);
                notificationLabel.text = "Not enough Food";
                _notEnoughFood = (int)currentZone.addWorkerPrice.food;
                SoundManager.Instance.PlaySound(Sounds.NotEnough);
            }
        }
        else
        {
            if (GameManager.Instance.Gems >= (int)currentZone.addWorkerPrice.gems)
            {
                //if (!currentZone.ZoneIsFull() && MinionsManager.Instance.TotalMinions() > currentZone.minionsInZone)
                if (!currentZone.ZoneIsFull())
                {
                    addWorkerAnims.gemsAnimator.SetTrigger("Pressed");
                    GameManager.Instance.Gems -= (int)currentZone.addWorkerPrice.gems;
                    currentZone.OnClick_AddMinionToZone();
                    IncUpgradeLevel(upgradeLevels[0]);
                    SoundManager.Instance.PlaySound(Sounds.Upgrade);
                    minionsCountLabel.text = currentZone.minionsInZone.ToString() + "/100";
                    AnalyticsManager.Instance.SpendGemWorker();
                }
            }
            else
            {
                addWorkerAnims.gemsAnimator.SetTrigger("NotEnoughGems");
                notificationIcon.SetActive(true);
                notificationLabel.text = "Not enough Gems";
                _notEnoughGems = (int)currentZone.addWorkerPrice.gems;
                SoundManager.Instance.PlaySound(Sounds.NotEnough);
            }
        }

        if(Tutorial.Instance.activated && Tutorial.Instance.stepsCounter == 6)
        {
            Tutorial.Instance.HideTutorialStep();
            OnClick_CloseUpgradesPanel();
            Tutorial.Instance.ShowTutorialStep();
        }
    }

    public void OnClick_IncreaseHouseLimit(bool isGems)
    {
        //if (currentZone.state == Zone.State.Ready)
        //{
            if (!isGems)
            {
                if (GameManager.Instance.Food >= (int)currentZone.increaseHouseLimitPrice.food)
                {
                    if (!currentZone.isRefilling)
                    {
                        if (currentZone.ZoneIsFull()) ClearNotificationLabel();
                        increaseHousesAnims.foodAnimator.SetTrigger("Pressed");
                        GameManager.Instance.Food -= (int)currentZone.increaseHouseLimitPrice.food;
                        if (currentZone.state != Zone.State.Ready)
                            currentZone.harvestProgressBar.PlayCallback();

                        LimitHouseApply();
                    }
                }
                else
                {
                    increaseHousesAnims.foodAnimator.SetTrigger("NotEnough");
                    notificationIcon.SetActive(true);
                    notificationLabel.text = "Not enough Food";
                    _notEnoughFood = (int)currentZone.increaseHouseLimitPrice.food;
                    SoundManager.Instance.PlaySound(Sounds.NotEnough);
                }
            }
            else
            {
                if (GameManager.Instance.Gems >= (int)currentZone.increaseHouseLimitPrice.gems)
                {
                    if (!currentZone.isRefilling)
                    {
                        if (currentZone.ZoneIsFull()) ClearNotificationLabel();
                        increaseHousesAnims.gemsAnimator.SetTrigger("Pressed");
                        GameManager.Instance.Gems -= (int)currentZone.increaseHouseLimitPrice.gems;
                        if (currentZone.state != Zone.State.Ready)
                            currentZone.harvestProgressBar.PlayCallback();

                        LimitHouseApply();
                    }
                }
                else
                {
                    increaseHousesAnims.gemsAnimator.SetTrigger("NotEnoughGems");
                    notificationIcon.SetActive(true);
                    notificationLabel.text = "Not enough Gems";
                    _notEnoughGems = (int)currentZone.increaseHouseLimitPrice.gems;
                    SoundManager.Instance.PlaySound(Sounds.NotEnough);
                }
            }
        //}
        //else
        //{
        //    //notificationIcon.SetActive(true);
        //    //notificationLabel.text = "House must be empty for upgrade the house";
        //    //_houseLimitTriedToClickFlag = true;
        //    currentZone.harvestProgressBar.PlayCallback();
        //    LimitHouseApply();
        //}
    }

    void LimitHouseApply()
    {
        currentZone.IncreaseHouseLimitUpgrade();
        SoundManager.Instance.PlaySound(Sounds.Upgrade);
        IncUpgradeLevel(upgradeLevels[1]);
        PlayerPrefs.SetInt("_houseFullFirstTimeHaveEnoughFoodFlag", 1);
        _houseFullFirstTimeHaveEnoughFoodFlag = false;
        incHouseBtnAnim.goToStateIfNotAlreadyThere(idleIncBtnState);
        OnClick_CloseUpgradesPanel();
    }

    public void OnClick_FoodPriceValue(bool isGems)
    {
        if(!isGems)
        {
            if(GameManager.Instance.Food >= (int)currentZone.foodPriceValue.food)
            {
                foodPriceValuesAnims.foodAnimator.SetTrigger("Pressed");
                GameManager.Instance.Food -= (int)currentZone.foodPriceValue.food;
                currentZone.FoodPriceValueUpgrade();
                SoundManager.Instance.PlaySound(Sounds.Upgrade);
                IncUpgradeLevel(upgradeLevels[2]);
            }
            else
            {
                foodPriceValuesAnims.foodAnimator.SetTrigger("NotEnough");
                notificationIcon.SetActive(true);
                notificationLabel.text = "Not enough Food";
                _notEnoughFood = (int)currentZone.foodPriceValue.food;
                SoundManager.Instance.PlaySound(Sounds.NotEnough);
            }
        }
        else
        {
            if(GameManager.Instance.Gems >= (int)currentZone.foodPriceValue.gems)
            {
                foodPriceValuesAnims.gemsAnimator.SetTrigger("Pressed");
                GameManager.Instance.Gems -= (int)currentZone.foodPriceValue.gems;
                currentZone.FoodPriceValueUpgrade();
                SoundManager.Instance.PlaySound(Sounds.Upgrade);
                IncUpgradeLevel(upgradeLevels[2]);
                AnalyticsManager.Instance.SpendGemFoodAmount();
            }
            else
            {
                foodPriceValuesAnims.gemsAnimator.SetTrigger("NotEnoughGems");
                notificationIcon.SetActive(true);
                notificationLabel.text = "Not enough Gems";
                _notEnoughGems = (int)currentZone.foodPriceValue.gems;
                SoundManager.Instance.PlaySound(Sounds.NotEnough);
            }
        }
    }

    public void OnClick_IncreateFindGemsChance(bool isGems)
    {
        if(!isGems && !currentZone.isFindGemsMaximum)
        {
            if(GameManager.Instance.Food >= (int)currentZone.findGemsChancePrice.food)
            {
                findGemsAnims.foodAnimator.SetTrigger("Pressed");
                GameManager.Instance.Food -= (int)currentZone.findGemsChancePrice.food;
                currentZone.IncreaseGemsChanceUpgrade();
                SoundManager.Instance.PlaySound(Sounds.Upgrade);
            }
            else
            {
                findGemsAnims.foodAnimator.SetTrigger("NotEnough");
                notificationIcon.SetActive(true);
                notificationLabel.text = "Not enough Food";
                _notEnoughFood = (int)currentZone.findGemsChancePrice.food;
                SoundManager.Instance.PlaySound(Sounds.NotEnough);
            }
        }
        else if(isGems && !currentZone.isFindGemsMaximum)
        {
            if(GameManager.Instance.Gems >= (int)currentZone.findGemsChancePrice.gems)
            {
                findGemsAnims.gemsAnimator.SetTrigger("Pressed");
                GameManager.Instance.Gems -= (int)currentZone.findGemsChancePrice.gems;
                currentZone.IncreaseGemsChanceUpgrade();
                SoundManager.Instance.PlaySound(Sounds.Upgrade);
                AnalyticsManager.Instance.SpendGemIncreaseFindGemsChance();
            }
            else
            {
                findGemsAnims.gemsAnimator.SetTrigger("NotEnoughGems");
                notificationIcon.SetActive(true);
                notificationLabel.text = "Not enough Gems";
                _notEnoughGems = (int)currentZone.findGemsChancePrice.gems;
                SoundManager.Instance.PlaySound(Sounds.NotEnough);
            }
        }
    }

    public void OnClick_WalkingSpeed(bool isGems)
    {
        if (!isGems)
        {
            if (GameManager.Instance.Food >= (int)currentZone.walkingSpeedPrice.food)
            {
                speedAnims.foodAnimator.SetTrigger("Pressed");
                GameManager.Instance.Food -= (int)currentZone.walkingSpeedPrice.food;
                currentZone.WalkingSpeedUpgrade();
                SoundManager.Instance.PlaySound(Sounds.Upgrade);
                IncUpgradeLevel(upgradeLevels[3]);
            }
            else
            {
                speedAnims.foodAnimator.SetTrigger("NotEnough");
                notificationIcon.SetActive(true);
                notificationLabel.text = "Not enough Food";
                _notEnoughFood = (int)currentZone.walkingSpeedPrice.food;
                SoundManager.Instance.PlaySound(Sounds.NotEnough);
            }
        }
        else
        {
            if(GameManager.Instance.Gems >= (int)currentZone.walkingSpeedPrice.gems)
            {
                speedAnims.gemsAnimator.SetTrigger("Pressed");
                GameManager.Instance.Gems -= (int)currentZone.walkingSpeedPrice.gems;
                currentZone.WalkingSpeedUpgrade();
                SoundManager.Instance.PlaySound(Sounds.Upgrade);
                IncUpgradeLevel(upgradeLevels[3]);
                AnalyticsManager.Instance.SpendGemWalkingSpeed();
            }
            else
            {
                speedAnims.gemsAnimator.SetTrigger("NotEnoughGems");
                notificationIcon.SetActive(true);
                notificationLabel.text = "Not enough Gems";
                _notEnoughGems = (int)currentZone.walkingSpeedPrice.gems;
                SoundManager.Instance.PlaySound(Sounds.NotEnough);
            }
        }
    }

    void IncUpgradeLevel(UpgradeLevel upgradeLevel)
    {
        //upgradeLevel.currLevel[zone]++;
        //if(upgradeLevel.currLevel[zone] >= upgradeLevel.maxLevel[zone])
        //{
        //    upgradeLevel.foodBtnGO.SetActive(false);
        //    upgradeLevel.gemsBtnGO.SetActive(false);
        //}
        if(upgradeLevel.type == UpgradeType.AddWorker)
        {
            if(currentZone.minionsInZone >= 100)
            {
                upgradeLevel.foodBtnGO.SetActive(false);
                upgradeLevel.gemsBtnGO.SetActive(false);
            }
        }
        else if(upgradeLevel.type == UpgradeType.FoodPrice)
        {
            if(currentZone.foodPriceValue.currLevel >= 20)
            {
                upgradeLevel.foodBtnGO.SetActive(false);
                upgradeLevel.gemsBtnGO.SetActive(false);
            }
        }
        else if(upgradeLevel.type == UpgradeType.IncHouseLimit)
        {
            if(currentZone.increaseHouseLimitPrice.currLevel >= currentZone.increaseHouseLimitPrice.levelMax)
            {
                upgradeLevel.foodBtnGO.SetActive(false);
                upgradeLevel.gemsBtnGO.SetActive(false);
            }
        }
        else if(upgradeLevel.type == UpgradeType.WalkingSpeed)
        {
            if(currentZone.walkingSpeedPrice.currLevel >= currentZone.walkingSpeedPrice.levelMax)
            {
                upgradeLevel.foodBtnGO.SetActive(false);
                upgradeLevel.gemsBtnGO.SetActive(false);
            }
        }
    }

    #endregion

    #region Notifications label

    public void CheckHouseLimitTriedToClick()
    {
        if (_houseLimitTriedToClickFlag)
            ClearNotificationLabel();
    }
    public void CheckNotEnoughFoodNotification()
    {
        if (_notEnoughFood != 0)
        {
            if (GameManager.Instance.Food >= _notEnoughFood)
            {
                ClearNotificationLabel();
            }
        }
    }
    public void CheckNotEnoughGemsNotification()
    {
        if (_notEnoughGems != 0)
        {
            if (GameManager.Instance.Food >= _notEnoughFood)
            {
                ClearNotificationLabel();
            }
        }
    }
    void ClearNotificationLabel()
    {
        notificationIcon.SetActive(false);
        notificationLabel.text = "";
        _notEnoughFood = 0;
        _notEnoughGems = 0;
    }

    #endregion

    public void FindGameChanceIsMaximum()
    {
        findGemsNotMaximumStuff.SetActive(false);
        findGemsMaximumStuff.SetActive(true);
    }

    #region Watch video buttons

    public void OnClick_AddWorkerVideo()
    {
        VideoCallbackDelegate del = AddWorkerVideoWatched;
        AdsManager.Instance.WatchCallbackVideo(del, VideoType.ShopUpgrades);
    }

    public void OnClick_IncreaseHouseLimitVideo()
    {
        //UpgradeVideoWatched del = IncreaseHouseLimitVideoWatched;
        //AdsManager.Instance.WatchUpgradeVideo(del);
    }

    public void OnClick_FoodPriceVideo()
    {
        //UpgradeVideoWatched del = FoodPriceVideoWatched;
        //AdsManager.Instance.WatchUpgradeVideo(del);
    }

    public void OnClick_IncreaseFindGemsChanceVideo()
    {
        //UpgradeVideoWatched del = IncreaseFindGemsChanceVideoWatched;
        //AdsManager.Instance.WatchUpgradeVideo(del);
    }

    public void OnClick_WalkingSpeedVideo()
    {
        //UpgradeVideoWatched del = WalkingSpeedVideoWatched;
        //AdsManager.Instance.WatchUpgradeVideo(del);
    }

    #endregion

    #region Video watched methods callbacks

    void AddWorkerVideoWatched()
    {
        AnalyticsManager.Instance.UserWatchVideoAddWorker();
        currentZone.OnClick_AddMinionToZone();
    }

    void IncreaseHouseLimitVideoWatched()
    {
        print("increase limit watched");
        currentZone.IncreaseHouseLimitUpgrade();
        SoundManager.Instance.PlaySound(Sounds.ShopUpgrageBought);
    }

    void FoodPriceVideoWatched()
    {
        print("food price watched");
        currentZone.FoodPriceValueUpgrade();
        SoundManager.Instance.PlaySound(Sounds.ShopUpgrageBought);
    }

    void IncreaseFindGemsChanceVideoWatched()
    {
        print("increase find gems watched");
        currentZone.IncreaseGemsChanceUpgrade();
        SoundManager.Instance.PlaySound(Sounds.ShopUpgrageBought);
    }

    void WalkingSpeedVideoWatched()
    {
        print("walking speed watched");
        currentZone.WalkingSpeedUpgrade();
        SoundManager.Instance.PlaySound(Sounds.ShopUpgrageBought);
    }

#endregion

    void CheckVideoButtonsAvailability()
    {
        //if (!currentZone.ZoneIsFull() && MinionsManager.Instance.TotalMinions() > currentZone.minionsInZone)
        if (!currentZone.ZoneIsFull())
        {
            if (currentZone.minionsInZone < currentZone.minionsCountLimit)
            {
                addWorkerVideoBtnGO.SetActive(true);
            }
        }
        else addWorkerVideoBtnGO.SetActive(false);

        incFindGemsChanceVideoBtnGO.SetActive(!currentZone.isFindGemsMaximum);

        if (!AdsManager.Instance.IsVideoAvailable())
        {
            addWorkerVideoBtnGO.SetActive(false);
            incHouseLimitVideoBtnGO.SetActive(false);
            foodPriceVideoBtnGO.SetActive(false);
            incFindGemsChanceVideoBtnGO.SetActive(false);
            walkingSpeedVideoBtnGO.SetActive(false);
        }
        else
        {
            incHouseLimitVideoBtnGO.SetActive(true);
            foodPriceVideoBtnGO.SetActive(true);
            walkingSpeedVideoBtnGO.SetActive(true);
        }
    }

    void Update()
    {
        if(upgradesShopUIPanel.activeSelf)
        {
            CheckVideoButtonsAvailability();
            AddWorkersGemsAndVideoBtnAvailability();
            if (currentZone.limitesIndex <= 18)
            {
                increaseHousesAnims.foodAnimator.gameObject.SetActive(!currentZone.isUpgrading);
                increaseHousesAnims.gemsAnimator.gameObject.SetActive(!currentZone.isUpgrading);
            }
            else
            {
                increaseHousesAnims.foodAnimator.gameObject.SetActive(false);
                increaseHousesAnims.gemsAnimator.gameObject.SetActive(false);
            }
        }

        if(increaseHousesAnims.foodAnimator.gameObject.activeSelf && _houseFullFirstTimeHaveEnoughFoodFlag)
        {
            if (GameManager.Instance.Food >= (int)currentZone.increaseHouseLimitPrice.food)
            {
                incHouseBtnAnim.goToStateIfNotAlreadyThere(bounceIncBtnState);
            }
        }
    }

    public bool Is10MinionsHouseLimitUnlocked()
    {
        return currentZone.minionsInZone >= 5;
    }

    void AddWorkersGemsAndVideoBtnAvailability()
    {
        if(currentZone.zoneIndex == 0 && currentZone.minionsInZone >= 2)
        {
            if (currentZone != null)
            {
                if (!currentZone.ZoneIsFull())
                {
                    addWorkerVideoBtnGO2.SetActive(true);
                }
                else
                {
                    addWorkerVideoBtnGO2.SetActive(false);
                }
            }
        }
        else if(currentZone.zoneIndex == 0 && currentZone.minionsInZone < 2)
        {
            addWorkerVideoBtnGO2.SetActive(false);
        }
        else
        {
            if (currentZone != null)
            {
                if (!currentZone.ZoneIsFull())
                {
                    addWorkerVideoBtnGO2.SetActive(true);
                }
                else
                {
                    addWorkerVideoBtnGO2.SetActive(false);
                }
            }
        }
    }
}

[System.Serializable]
public class UpgradesBtnsAnimators
{
    public Animator foodAnimator, gemsAnimator;
}