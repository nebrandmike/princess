﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Advertisements;
using ChartboostSDK;

public enum VideoType
{
    Default,
    DoubleDailyBonus,
    ShopUpgrades,
    DoubleCardsReward,
    EpicUpgrade,
    DailyGift
}

public delegate void VideoCallbackDelegate();

public enum VideoAdsNetwork
{
    UnityAds,
    Vungle,
    Chartboost
}

/// <summary>
/// Actually it's only video ads manager. Interstitial and banner you can find in AdsInterstitialManager
/// </summary>
public class AdsManager : MonoBehaviour {

    public static AdsManager Instance;
    public string unityAdszoneId;
    public int rewardQty = 1;
    public GameObject rewardedPanel;
    public UnityEngine.UI.Text rewardLabel;
    public GameObject watchAVideoPanel, inGamePanel;
    ShowOptions options;
    public GameObject rewardedVideoBtnGO;
    public GameObject backgroundGO;
    public Animator yesBtnAnim;
    public AudioSource[] audioSources;
    public FyberManager fyberManager;
    [Space]
    public bool UnityAdsEnabled = true;
    public bool VungleEnabled = true;
    public bool ChartboostEnabled = false;

    private VideoCallbackDelegate _upgradeVideoWatchedDelegate;
    private VideoType _videoType;
    private VideoAdsNetwork _lastVideoAdsNetwork;
    private int _videoAdsCounter;
    private bool _canPlayVideo = true;

    //[Space]
    //public UnityEngine.UI.Text vungleAdsTestLabel;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {   
        if (UnityAdsEnabled)
        {
            options = new ShowOptions();
            options.resultCallback = HandleUnityAdsShowResult;
        }

        if (VungleEnabled)
        {
            Vungle.init("583f24d0c66120751d00030a", "583f24654ffcb7af6b00035e", "Test_Windows");
            Vungle.onAdFinishedEvent += onAdFinishedEvent;
        }

        if (ChartboostEnabled)
        {
            Invoke("CacheChartboostRewardedVideo", 15);
            Chartboost.didCompleteRewardedVideo += didCompleteRewardedVideo;
            Chartboost.didDismissRewardedVideo += didDismissRewardedVideo;
            Chartboost.didCloseRewardedVideo += didCloseRewardedVideo;
            Invoke("CacheChartboostRewardedVideo", 6);
        }
    }

    public void OnClick_RewardedVideo()
    {
        if (_canPlayVideo)
        {
            _canPlayVideo = false;
            Invoke("ResetCanPlayVideoFlag", 1);

            if (!AmazonManager.Instance.amazonEnabled)
            {
                if (!fyberManager.offerWallAvailable)
                {
                    rewardedVideoBtnGO.SetActive(false);
                    watchAVideoPanel.SetActive(true);
                    watchAVideoPanel.GetComponent<Animator>().SetTrigger("Appear");
                    UIManager.Instance.OnPanelOpen(true);
                    backgroundGO.SetActive(true);
                    SoundManager.Instance.PlaySound(Sounds.Panel);
                }
                else
                {
                    AnalyticsManager.Instance.FyberPressed();
                    fyberManager.RequestOfferWall();
                    Invoke("ShowOfferWall", 0.25f);
                }
            }
            else
            {
                rewardedVideoBtnGO.SetActive(false);
                watchAVideoPanel.SetActive(true);
                watchAVideoPanel.GetComponent<Animator>().SetTrigger("Appear");
                UIManager.Instance.OnPanelOpen(true);
                backgroundGO.SetActive(true);
                SoundManager.Instance.PlaySound(Sounds.Panel);
            }
        }
    }

    void ResetCanPlayVideoFlag()
    {
        _canPlayVideo = true;
    }

    void ShowOfferWall()
    {
        fyberManager.ShowOfferWall();
    }

    public void OnClick_YesWatchRewardedVideo()
    {
        //if (IsVideoAvailable())
        //{
        if (_canPlayVideo)
        {
            //_canPlayVideo = false;
            Invoke("ResetCanPlayVideoFlag", 1);
            yesBtnAnim.SetTrigger("Pressed");
            rewardedPanel.GetComponent<Animator>().SetTrigger("DissDelay");
            PlayVideo();
        }
        //}
        //else
        //{
        //    yesBtnAnim.SetTrigger("Wiggle");
        //    SoundManager.Instance.PlaySound(Sounds.NotEnough);
        //}
    }

    public void PlayVideo()
    {
        if (_canPlayVideo)
        {
            _canPlayVideo = false;
            Invoke("ResetCanPlayVideoFlag", 1);
            _videoType = VideoType.Default;
            rewardedVideoBtnGO.SetActive(false);
            backgroundGO.SetActive(false);
            UIManager.Instance.OnPanelOpen(false);
            PlayAvailableVideo();
            SoundManager.Instance.PlaySound(Sounds.Panel);
            OnClick_CloseWatchRewardedVideoPanel();
        }
    }

    void PlayAvailableVideo()
    {
        _videoAdsCounter++;

        if (_videoAdsCounter == 3)
            _videoAdsCounter = 0;

        if (_videoAdsCounter == 0)
        {
            if (UnityAdsEnabled)
                PlayUnityAdsVideo();
            else PlayAvailableVideo();
        }
        else if (_videoAdsCounter == 1)
        {
            if (Vungle.isAdvertAvailable())
            {
                if (VungleEnabled)
                    PlayVungleVideo();
                else
                    PlayAvailableVideo();
            }
            else
            {
                _videoAdsCounter++;
                PlayAvailableVideo();
                return;
            }
        }
        else if (_videoAdsCounter == 2)
        {
            if (ChartboostEnabled)
            {
#if !UNITY_EDITOR
            PlayChartboostRewardedVideo();
#endif
            }
            else
            {
                PlayAvailableVideo();
            }
        }
    }

    public void OnClick_CloseWatchRewardedVideoPanel()
    {
        backgroundGO.SetActive(true);
        SoundManager.Instance.PlaySound(Sounds.ClosePanel);
        backgroundGO.SetActive(false);
        Invoke("HidePanel", 0.5f);
    }

    void HidePanel()
    {
        watchAVideoPanel.SetActive(false);
        rewardedVideoBtnGO.SetActive(true);
        UIManager.Instance.OnPanelOpen(false);
    }

    public bool WatchAVideoPanelOpened()
    {
        return watchAVideoPanel.activeSelf;
    }

    public void PlayDailyBonusVideo()
    {
        _videoType = VideoType.DoubleDailyBonus;
        PlayAvailableVideo();
    }

    void PlayUnityAdsVideo()
    {
        Advertisement.Show(unityAdszoneId, options);
    }

    public void PlayVungleVideo()
    {
        //vungleAdsTestLabel.text = "vungle available:" + Vungle.isAdvertAvailable().ToString();
        if (Vungle.isAdvertAvailable())
        {
            Dictionary<string, object> options = new Dictionary<string, object>();
            options["incentivized"] = true;
            Vungle.playAdWithOptions(options);
            // disable sounds and music
            for (int i = 0; i < audioSources.Length; i++)
            {
                audioSources[i].enabled = false;
            }
            Invoke("EnableSoundsAndMusic", 30);
        }
        //else PlayUnityAdsVideo();
}
    // Unity Ads Callback
    private void HandleUnityAdsShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:

                switch(_videoType)
                {
                    case VideoType.Default:
                        Debug.Log("Video completed. User rewarded " + rewardQty + " credits.");
                        watchAVideoPanel.SetActive(false);
                        backgroundGO.SetActive(true);
                        rewardedPanel.SetActive(true);
                        UIManager.Instance.OnPanelOpen(false);
                        rewardedPanel.GetComponent<Animator>().SetTrigger("Appear");
                        rewardLabel.text = rewardQty.ToString();
                        break;

                    case VideoType.DoubleDailyBonus:
                        DailyBonus.Instance.WatchVideoCallback();
                        break;

                    case VideoType.ShopUpgrades:
                        _upgradeVideoWatchedDelegate();
                        break;

                    case VideoType.DoubleCardsReward:
                        CardsManager.Instance.DoubleRewardVideoWatched();
                        break;
                    case VideoType.EpicUpgrade:
                        _upgradeVideoWatchedDelegate();
                        break;

                    case VideoType.DailyGift:
                        DailyGift.Instance.VideoWatchedCallback();
                        break;
                }
                break;     
            case ShowResult.Skipped:
                Debug.LogWarning("Video was skipped.");
                break;
            case ShowResult.Failed:
                Debug.LogError("Video failed to show.");
                break;
        }
    }
    // Vungle Callback
    void onAdFinishedEvent(AdFinishedEventArgs arg)
    {
        Debug.Log("onAdFinishedEvent. watched: " + arg.TimeWatched + ", length: " + arg.TotalDuration + ", isCompletedView: " + arg.IsCompletedView);
        if (arg.IsCompletedView)
        {
            switch (_videoType)
            {
                case VideoType.Default:
                    Debug.Log("Video completed. User rewarded " + rewardQty + " credits.");
                    watchAVideoPanel.SetActive(false);
                    backgroundGO.SetActive(true);
                    rewardedPanel.SetActive(true);
                    UIManager.Instance.OnPanelOpen(false);
                    rewardedPanel.GetComponent<Animator>().SetTrigger("Appear");
                    rewardLabel.text = rewardQty.ToString();
                    break;

                case VideoType.DoubleDailyBonus:
                    DailyBonus.Instance.WatchVideoCallback();
                    break;

                case VideoType.ShopUpgrades:
                    _upgradeVideoWatchedDelegate();
                    break;

                case VideoType.DoubleCardsReward:
                    CardsManager.Instance.DoubleRewardVideoWatched();
                    break;
                case VideoType.EpicUpgrade:
                    _upgradeVideoWatchedDelegate();
                    break;

                case VideoType.DailyGift:
                    DailyGift.Instance.VideoWatchedCallback();
                    break;
            }
        }
        // enable sounds and music
        for (int i = 0; i < audioSources.Length; i++)
        {
            audioSources[i].enabled = true;
        }
        MusicManager.Instance.GetComponent<AudioSource>().Play();
    }

    void EnableSoundsAndMusic()
    {
        for (int i = 0; i < audioSources.Length; i++)
        {
            audioSources[i].enabled = true;
        }
        if(!MusicManager.Instance.GetComponent<AudioSource>().isPlaying)
            MusicManager.Instance.GetComponent<AudioSource>().Play();
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus)
        {
            Vungle.onPause();
        }
        else
        {
            Vungle.onResume();
        }
    }

    public void OnClick_RewardedPanelOK()
    {
        GameManager.Instance.Gems += rewardQty;
        AchievementsManager.Instance.SendAchievement(AchievementName.GemsMagnat, rewardQty, AchievementType.Countable);
        backgroundGO.SetActive(false);
        SoundManager.Instance.PlaySound(Sounds.ClosePanel);
        Invoke("CloseRewardedPanel", 0.9f);
    }

    void CloseRewardedPanel()
    {
        rewardedPanel.SetActive(false);
        UIManager.Instance.OnPanelOpen(false);
    }

    private bool _canWatchVideo = true;
    public void WatchCallbackVideo(VideoCallbackDelegate del, VideoType videoType)
    {
        if (_canWatchVideo)
        {
            _canWatchVideo = false;
            Invoke("ResetCanWatchVideoFlag", 1);

            _upgradeVideoWatchedDelegate = del;
            _videoType = videoType;

            PlayAvailableVideo();
        }
    }

    void ResetCanWatchVideoFlag()
    {
        _canWatchVideo = true;
    }

    public void WatchDoubleCardRewardVideo()
    {
        if (_canPlayVideo)
        {
            _canPlayVideo = false;
            Invoke("ResetCanPlayVideoFlag", 1);
            _videoType = VideoType.DoubleCardsReward;

            PlayAvailableVideo();
        }
    }

    public bool IsVideoAvailable()
    {
        if (Vungle.isAdvertAvailable() || Advertisement.IsReady())
            return true;

        return false;
    }

#region Chartboost

    public void CacheChartboostRewardedVideo()
    {
        Chartboost.cacheRewardedVideo(CBLocation.MainMenu);
    }

    public void PlayChartboostRewardedVideo()
    {
        //SoundManager.Instance.MuteSoundsAndMusic(true);
        Chartboost.showRewardedVideo(CBLocation.MainMenu);
        Invoke("CacheChartboostRewardedVideo", 2);
    }

    void didCompleteRewardedVideo(CBLocation location, int reward)
    {
        switch (_videoType)
        {
            case VideoType.Default:
                Debug.Log("Video completed. User rewarded " + rewardQty + " credits.");
                watchAVideoPanel.SetActive(false);
                backgroundGO.SetActive(true);
                rewardedPanel.SetActive(true);
                UIManager.Instance.OnPanelOpen(false);
                rewardedPanel.GetComponent<Animator>().SetTrigger("Appear");
                rewardLabel.text = rewardQty.ToString();
                break;

            case VideoType.DoubleDailyBonus:
                DailyBonus.Instance.WatchVideoCallback();
                break;

            case VideoType.ShopUpgrades:
                _upgradeVideoWatchedDelegate();
                break;

            case VideoType.DoubleCardsReward:
                CardsManager.Instance.DoubleRewardVideoWatched();
                break;
            case VideoType.EpicUpgrade:
                _upgradeVideoWatchedDelegate();
                break;

            case VideoType.DailyGift:
                DailyGift.Instance.VideoWatchedCallback();
                break;
        }

        //SoundManager.Instance.MuteSoundsAndMusic(false);
    }

    // Вызывается после того, как вознаграждаемое видео было прекращено.
    void didDismissRewardedVideo(CBLocation location)
    {
        //SoundManager.Instance.MuteSoundsAndMusic(false);
    }

    // Вызывается после того, как вознаграждаемое видео было закрыто.
    void didCloseRewardedVideo(CBLocation location)
    {
        //SoundManager.Instance.MuteSoundsAndMusic(false);
    }


#endregion
}
