﻿using UnityEngine;
using System.Collections;
using admob;
using ChartboostSDK;

public enum AdsNetwork
{
    Admob,
    Chartboost
}

public class AdsInterstitialManager : MonoBehaviour
{
    public static AdsInterstitialManager Instance;

    // admob:
    //banner iOS ca-app-pub-4770476964347943/7480602032
    //interstitial iOS ca-app-pub-4770476964347943/8957335235
    //rewarded video 3 gems iOS  ca-app-pub-4770476964347943/1434068430

    private AdsNetwork lastUsedNetwork = AdsNetwork.Admob;

    private Admob ad;

    public int openUpgradesCounter;
    [Header("delay before interst will be shown")]
    public float interstitialDelaySec = 0.4f;
    [Space]
    public bool admobEnable = true;
    public bool chartboostEnable = false;

    private bool _adsRemoved; // if user make any in app purchase

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        _adsRemoved = PlayerPrefs.GetInt("AdsRemoved") == 0 ? false : true;

        if (admobEnable)
        {
            Invoke("initAdmob", 2);
            Invoke("CacheAdmobInterstitial", 10);
            Invoke("CheckIfHouse1LimitMore5", 12);
        }
        if(chartboostEnable)
            Invoke("CacheChartboostInterstitial", 6);
    }

    void CheckIfHouse1LimitMore5()
    {
        if (GameManager.Instance.zones[0].limitesIndex >= 1 && !_adsRemoved)
            RequestAdmobSmartBanner();
    }

    public void OnOpenUpgradesPanel()
    {
        int showAdsCounter = PlayerPrefs.GetInt("Rated") == 1 ? 5 : 8;

        openUpgradesCounter++;
        if(openUpgradesCounter % showAdsCounter == 0 && !_adsRemoved)
        {
            if (admobEnable && !chartboostEnable)
            {
                ShowAdmobInterstitial();
            }
            else if (admobEnable && chartboostEnable)
            {
                if (lastUsedNetwork == AdsNetwork.Chartboost)
                {
                    ShowAdmobInterstitial();
                    lastUsedNetwork = AdsNetwork.Admob;
                }
                else if (lastUsedNetwork == AdsNetwork.Admob)
                {
                    ShowChartboostInterstitial();
                    lastUsedNetwork = AdsNetwork.Chartboost;
                }
            }
        }
    }

    // user make any in app purchase
    public void RemoveAds()
    {
        _adsRemoved = true;
        PlayerPrefs.SetInt("AdsRemoved", 1);
        RemoveAdmobBanner();
    }

    #region Admob

    public void initAdmob()
    {
        if (!_adsRemoved)
        {
            //  isAdmobInited = true;
            ad = Admob.Instance();
            ad.bannerEventHandler += onBannerEvent;
            ad.interstitialEventHandler += onInterstitialEvent;
            ad.rewardedVideoEventHandler += onRewardedVideoEvent;
            ad.nativeBannerEventHandler += onNativeBannerEvent;

#if UNITY_ANDROID
            ad.initAdmob("ca-app-pub-4770476964347943/2249823634", "ca-app-pub-4770476964347943/6680023239");
#endif
#if UNITY_IPHONE
        ad.initAdmob("ca-app-pub-4770476964347943/7480602032", "ca-app-pub-4770476964347943/8957335235");
#endif
        }
    }

    void onInterstitialEvent(string eventName, string msg)
    {
        if (!_adsRemoved)
        {
            Debug.Log("handler onAdmobEvent---" + eventName + "   " + msg);
            if (eventName == AdmobEvent.onAdLoaded)
            {
                //Admob.Instance().showInterstitial();
            }
            else if (eventName == AdmobEvent.onAdClosed)
            {
                //SoundManager.Instance.MuteSoundsAndMusic(false);
            }
            else if (eventName == AdmobEvent.onAdFailedToLoad)
            {
                //SoundManager.Instance.MuteSoundsAndMusic(false);
            }
            else if (eventName == AdmobEvent.adViewWillDismissScreen)
            {
                //SoundManager.Instance.MuteSoundsAndMusic(false);
            }
            else if (eventName == AdmobEvent.onAdOpened)
            {
                //SoundManager.Instance.MuteSoundsAndMusic(true);
            }
            else if (eventName == AdmobEvent.onRewardedVideoStarted)
            {
                //SoundManager.Instance.MuteSoundsAndMusic(true);
            }
            else if (eventName == AdmobEvent.onAdLeftApplication)
            {
                Time.timeScale = 0;
            }
        }
    }
    void onBannerEvent(string eventName, string msg)
    {
        Debug.Log("handler onAdmobBannerEvent---" + eventName + "   " + msg);
        if (eventName == AdmobEvent.onAdLeftApplication)
            Time.timeScale = 0;
    }
    void onRewardedVideoEvent(string eventName, string msg)
    {
        Debug.Log("handler onRewardedVideoEvent---" + eventName + "   " + msg);
        if (eventName == AdmobEvent.onAdLeftApplication)
            Time.timeScale = 0;
    }
    void onNativeBannerEvent(string eventName, string msg)
    {
        Debug.Log("handler onAdmobNativeBannerEvent---" + eventName + "   " + msg);
        if (eventName == AdmobEvent.onAdLeftApplication)
            Time.timeScale = 0;
    }

    private void OnApplicationPause(bool pause)
    {
        if (!pause)
            Time.timeScale = 1;
    }

    public void RequestAdmobBanner()
    {
        Admob.Instance().showBannerAbsolute(AdSize.Banner, Screen.width/2 - 150, Screen.height);
    }

    public void RequestAdmobSmartBanner()
    {
        Admob.Instance().showBannerRelative(AdSize.SmartBanner, AdPosition.BOTTOM_CENTER, 0);
        Invoke("RequestAdmobSmartBanner", 60);
    }

    void RemoveAdmobBanner()
    {
        Admob.Instance().removeBanner();
    }

    public void CacheAdmobInterstitial()
    {
#if !UNITY_EDITOR
        ad.loadInterstitial();
#endif
    }

    public void ShowAdmobInterstitial()
    {
        Invoke("ShowAdmobInterstitialWithDelay", interstitialDelaySec);
        Invoke("CacheAdmobInterstitial", 2);
    }

    void ShowAdmobInterstitialWithDelay()
    {
        ad.showInterstitial();
    }

#endregion

#region Chartboost

    public void CacheChartboostInterstitial()
    {
        Chartboost.cacheInterstitial(CBLocation.HomeScreen);
    }

    public void ShowChartboostInterstitial()
    {
        Chartboost.showInterstitial(CBLocation.HomeScreen);
        Invoke("CacheChartboostInterstitial", 1);
    }

#endregion
}
