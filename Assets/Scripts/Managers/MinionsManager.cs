﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MinionsManager : MonoBehaviour {

    public static MinionsManager Instance;
    // points to move around princess from one to another
    public Transform[] waypoints;
    // list of all existing minions on the scene
    [HideInInspector]
    public List<Minion> minions;
    public float zoneSpeed = 0.5f;
    public float minionsSpeed = 0.2f;

    void Awake()
    {
        Instance = this;
    }

    /// <summary>
    /// Get random point for move minion around the princess
    /// </summary>
    /// <returns></returns>
    public Transform RandomWaypoint()
    {
        return waypoints[Random.Range(0, waypoints.Length)];
    }

    /// <summary>
    /// After spawn new minion, let's add him to all minions list and update minions count on screen
    /// </summary>
    /// <param name="minion"></param>
    public void AddMinion(Minion minion)
    {
        minions.Add(minion);
        UIManager.Instance.UpdateMinionsCount(minions.Count);
        if(minions.Count % 5 == 0)
        {
            AnalyticsManager.Instance.MinionCount(minions.Count);
        }
    }

    /// <summary>
    /// How many minions doesn't work now (moving around the princess)
    /// </summary>
    /// <returns></returns>
    public int FreeMinions()
    {
        int counter = 0;
        for (int i = 0; i < minions.Count; i++)
        {
            if(minions[i].state == Minion.State.Free || minions[i].state == Minion.State.MoveToNewPoint)
            {
                counter++;
            }
        }
        return counter;
    }
    /// <summary>
    /// How many minions in the scene now?
    /// </summary>
    /// <returns></returns>
    public int TotalMinions()
    {
        return minions.Count;
    }

    /// <summary>
    /// List free minions (free - it's mean they are doesn't work at current moment, but walking around princess)
    /// </summary>
    /// <param name="level">currrent zone level</param>
    /// <returns></returns>
    public List<Minion> FreeMinionsLst(int level)
    {
        List<Minion> freeMinions = new List<Minion>();
        int counter = 0;
        for (int i = 0; i < minions.Count; i++)
        {
            if (minions[i].state == Minion.State.Free || minions[i].state == Minion.State.MoveToNewPoint)
            {
                freeMinions.Add(minions[i]);
                counter++;
                if (counter == level)
                    return freeMinions;
            }
        }
        return freeMinions;
    }
}
