﻿using UnityEngine;
using System.Collections;

public enum Sounds
{
    MinionBorn,
    Upgrade,
    SendMinionToWork,
    PrincessEat,
    Panel,
    ClosePanel,
    HouseUpgraded,
    ShopUpgrageBought,
    RandomGemEarned,
    BuyAll,
    Click,
    CardClicked,
    GoodCard,
    BadCard,
    NotEnough,
    TapOnHouse,
    HouseUpgradeFinished,
    SkipForGem,
    CatUpgradedShake,
    CatUpgradedBloop,
    AchievementUnlocked,
    GiftAvailable,
    GiftBoxTap,
    GiftBoxOpen,
    GiftAppear,
    TutPlus5Gems,
    CantBornMore,
    PigFlyAway,
    HousePlus5Completed
}

public class SoundManager : MonoBehaviour {

    public static SoundManager Instance;

    public AudioClip upgrade, princessEat, panel, shopUpgrageBought, houseUpgraded, randomGemEarned,
        buyAll, click, cardClicked, goodCard, badCard, notEnough, tapOnHouse,
        houseUpgradeFinished, skipForGem, catUpgradedShake, catUpgradedBloop, achievementUnlocked, giftAvailable,
        giftBoxTap, giftBoxOpen, giftAppear, tutPlus5Gems, cantBornMore, pigFlyAway, closePanel, housePlus5Completed;
    [Space]
    public AudioClip[] sendMinionToWork, panels, minionBorn;

    [Space]
    public UnityEngine.UI.Image muteSoundsBtnImg, muteMusicBtnImg;
    public Sprite soundsOn, soundsOff, musicOn, musicOff;
    public AudioSource musicAS;

    private AudioSource _audioSource;
    private bool _soundsOn = true;
    private bool _musicOn = true;

    void Awake()
    {
        Instance = this;
        _audioSource = GetComponent<AudioSource>();
        _soundsOn = PlayerPrefs.GetInt("SoundsOff") == 0 ? true : false;
        _musicOn = PlayerPrefs.GetInt("MusicOff") == 0 ? true : false;
        SetSoundsMuteSettings();
    }

    public void PlaySound(Sounds sound)
    {
        switch(sound)
        {
            case Sounds.MinionBorn:
                _audioSource.PlayOneShot(minionBorn[Random.Range(0, minionBorn.Length)]);
                break;
            case Sounds.Panel:
                _audioSource.PlayOneShot(panel);
                break;
            case Sounds.ClosePanel:
                _audioSource.PlayOneShot(closePanel);
                break;
            case Sounds.PrincessEat:
                if(!_audioSource.isPlaying)
                    _audioSource.PlayOneShot(princessEat);
                break;
            case Sounds.SendMinionToWork:
                _audioSource.PlayOneShot(sendMinionToWork[Random.Range(0, sendMinionToWork.Length)]);
                break;
            case Sounds.Upgrade:
                _audioSource.PlayOneShot(upgrade);
                break;
            case Sounds.HouseUpgraded:
                _audioSource.PlayOneShot(houseUpgraded);
                break;
            case Sounds.ShopUpgrageBought:
                _audioSource.PlayOneShot(shopUpgrageBought);
                break;
            case Sounds.RandomGemEarned:
                _audioSource.PlayOneShot(randomGemEarned);
                break;
            case Sounds.BuyAll:
                _audioSource.PlayOneShot(buyAll);
                break;
            case Sounds.Click:
                _audioSource.PlayOneShot(click);
                break;
            case Sounds.CardClicked:
                _audioSource.PlayOneShot(cardClicked);
                break;
            case Sounds.GoodCard:
                _audioSource.PlayOneShot(goodCard);
                break;
            case Sounds.BadCard:
                _audioSource.PlayOneShot(badCard);
                break;
            case Sounds.NotEnough:
                _audioSource.PlayOneShot(notEnough);
                break;
            case Sounds.TapOnHouse:
                _audioSource.PlayOneShot(tapOnHouse);
                break;
            case Sounds.HouseUpgradeFinished:
                _audioSource.PlayOneShot(houseUpgradeFinished);
                break;
            case Sounds.SkipForGem:
                _audioSource.PlayOneShot(skipForGem);
                break;
            case Sounds.CatUpgradedShake:
                _audioSource.PlayOneShot(catUpgradedShake);
                break;
            case Sounds.CatUpgradedBloop:
                _audioSource.PlayOneShot(catUpgradedBloop);
                break;
            case Sounds.AchievementUnlocked:
                _audioSource.PlayOneShot(achievementUnlocked);
                break;
            case Sounds.GiftAvailable:
                _audioSource.PlayOneShot(giftAvailable);
                break;
            case Sounds.GiftBoxTap:
                _audioSource.PlayOneShot(giftBoxTap);
                break;
            case Sounds.GiftBoxOpen:
                _audioSource.PlayOneShot(giftBoxOpen);
                break;
            case Sounds.GiftAppear:
                _audioSource.PlayOneShot(giftAppear);
                break;
            case Sounds.TutPlus5Gems:
                _audioSource.PlayOneShot(tutPlus5Gems);
                break;
            case Sounds.CantBornMore:
                _audioSource.PlayOneShot(cantBornMore);
                break;
            case Sounds.PigFlyAway:
                _audioSource.PlayOneShot(pigFlyAway);
                break;
            case Sounds.HousePlus5Completed:
                _audioSource.PlayOneShot(housePlus5Completed);
                break;
        }
    }

    public void SetSoundsMuteSettings()
    {
        muteSoundsBtnImg.sprite = _soundsOn ? soundsOn : soundsOff;
        muteMusicBtnImg.sprite = _musicOn ? musicOn : musicOff;

        _audioSource.volume = _soundsOn ? 1 : 0;
        musicAS.volume = _musicOn ? 0.35f : 0;
    }

    public void OnClick_MuteSounds()
    {
        _soundsOn = !_soundsOn;
        if (_soundsOn)
            PlayerPrefs.SetInt("SoundsOff", 0);
        else 
            PlayerPrefs.SetInt("SoundsOff", 1);

        SetSoundsMuteSettings();
        SoundManager.Instance.PlaySound(Sounds.Click);
    }

    public void OnClick_MuteMusic()
    {
        _musicOn = !_musicOn;
        if (_musicOn)
            PlayerPrefs.SetInt("MusicOff", 0);
        else
            PlayerPrefs.SetInt("MusicOff", 1);

        SetSoundsMuteSettings();
        SoundManager.Instance.PlaySound(Sounds.Click);
    }

    public void PlayPigAwaySound()
    {
        PlaySound(Sounds.PigFlyAway);
    }

    public void MuteSoundsAndMusic(bool isOn)
    {
        if(isOn)
        {
            _audioSource.volume = 0;
            musicAS.volume = 0;
        }
        else
        {
            if (_soundsOn)
                _audioSource.volume = 1;
            if (_musicOn)
                musicAS.volume = 0.35f;
        }
    }
}
