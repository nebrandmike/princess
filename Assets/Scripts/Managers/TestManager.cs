﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class TestManager : MonoBehaviour {

    public static TestManager Instance;

    public int addFoodOnClick = 1000;
    public int addGemsOnClick = 100;
    public GameObject hintLabelGO;
    public bool displayHowLongPlaying = true;

    public float howLongPlaying;
    public UnityEngine.UI.Text howLongLabel;
    public Toggle disBornMinionTgl;
    public Text parseDebugLabel;
    public Text fyberDebugLabel;

    void Start()
    {
        Instance = this;
        howLongPlaying = PlayerPrefs.GetFloat("_howLongPlaying");
    }

    public void OnClick_ResetGame()
    {
        print("reset settings");
        PlayerPrefs.DeleteAll();
        SaveHelper.Instance.SetDefaultSaves();
        hintLabelGO.SetActive(true);
    }

    public void OnClick_AddFood()
    {
        print("test food added");
        GameManager.Instance.Food += addFoodOnClick;
    }

    public void OnClick_AddGems()
    {
        print("test gems added");
        GameManager.Instance.Gems += addGemsOnClick;
    }

    void Update()
    {
        howLongPlaying += Time.deltaTime;
        if (howLongLabel.gameObject.activeSelf)
            howLongLabel.text = "how long playing: " + ((int)howLongPlaying).ToString();
    }

    public void Save()
    {
        PlayerPrefs.SetFloat("_howLongPlaying", howLongPlaying);
    }

    public void OnClick_DisableBornMinionTimerChanged()
    {
        Princess.Instance.bornTimerEnabled = !disBornMinionTgl.isOn;
    }

    public void OnClick_HideParseDebugLabel()
    {
        // show
        parseDebugLabel.DOFade(1, 0);
    }

    public void OnClick_ShowFyberDebugLabel()
    {
        fyberDebugLabel.DOFade(1, 0);
    }
}
