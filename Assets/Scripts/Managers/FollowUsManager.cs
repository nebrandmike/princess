﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class FollowUsManager : MonoBehaviour
{
    public string snapchatLink, instLink, youTubeLink, twitterLink, facebookLink, websiteLink;
    public UnityEngine.UI.Image snapchatBtnImg, instBtnImg, youTubeBtnImg, twitterBtnImg, fbBtnImg, websiteBtnImg;
    public int rewardGems = 1;
    public float timeDifferenceForSuccess;
    [Space]
    public GameObject followUs_Panel, followUsSuccess_Panel, followUsFail_Panel;

    private bool snapchatAvailable = true, instAvailable = true, youTubeAvailable = true, twitterAvailable = true,
        fbAvailable = true, websiteAvailable = true;
    private bool snapchatClicked, instClicked, youTubeClicked, twitterClicked, fbClicked, websiteClicked;
    private DateTime clickTime;

    public void OnFollowUsPanelOpen()
    {
        NullifyCLickedFlags();

        snapchatAvailable = PlayerPrefs.GetInt("cnapchatAvailable") == 0 ? true : false;
        instAvailable = PlayerPrefs.GetInt("instAvailable") == 0 ? true : false;
        youTubeAvailable = PlayerPrefs.GetInt("youTubeAvailable") == 0 ? true : false;
        twitterAvailable = PlayerPrefs.GetInt("twitterAvailable") == 0 ? true : false;
        fbAvailable = PlayerPrefs.GetInt("fbAvailable") == 0 ? true : false;
        websiteAvailable = PlayerPrefs.GetInt("websiteAvailable") == 0 ? true : false;

        if (!snapchatAvailable) snapchatBtnImg.DOFade(0.5f, 0);
        if (!instAvailable) instBtnImg.DOFade(0.5f, 0);
        if (!youTubeAvailable) youTubeBtnImg.DOFade(0.5f, 0);
        if (!twitterAvailable) twitterBtnImg.DOFade(0.5f, 0);
        if (!fbAvailable) fbBtnImg.DOFade(0.5f, 0);
        if (!websiteAvailable) websiteBtnImg.DOFade(0.5f, 0);
    }

    public void OnClick_Snapchat()
    {
        Application.OpenURL(snapchatLink);
        if (snapchatAvailable)
        {
            snapchatClicked = true;
            clickTime = DateTime.Now;
        }
    }

    public void OnClick_Inst()
    {
        Application.OpenURL(instLink);
        if (instAvailable)
        {
            instClicked = true;
            clickTime = DateTime.Now;
        }
    }

    public void OnClick_YouTube()
    {
        Application.OpenURL(youTubeLink);
        if (youTubeAvailable)
        {
            youTubeClicked = true;
            clickTime = DateTime.Now;
        }
    }

    public void OnClick_Twitter()
    {
        Application.OpenURL(twitterLink);
        if (twitterAvailable)
        {
            twitterClicked = true;
            clickTime = DateTime.Now;
        }
    }

    public void OnClick_Facebook()
    {
        Application.OpenURL(facebookLink);
        if (fbAvailable)
        {
            fbClicked = true;
            clickTime = DateTime.Now;
        }
    }

    public void OnClick_Website()
    {
        Application.OpenURL(websiteLink);
        if (websiteAvailable)
        {
            websiteClicked = true;
            clickTime = DateTime.Now;
        }
    }

    private void OnApplicationPause(bool pause)
    {
        if ((!pause && snapchatAvailable && snapchatClicked) ||
            (!pause && instAvailable && instClicked) ||
            (!pause && youTubeAvailable && youTubeClicked) ||
            (!pause && twitterAvailable && twitterClicked) ||
            (!pause && fbAvailable && fbClicked) ||
            (!pause && websiteAvailable && websiteClicked))
        {
            TimeSpan diff = DateTime.Now - clickTime;
            print(diff.TotalSeconds);
            if (diff.TotalSeconds >= timeDifferenceForSuccess)
                followUsSuccess_Panel.SetActive(true);
            else
                followUsFail_Panel.SetActive(true);

            UIManager.Instance.OnPanelOpen(true);
        }
    }

    public void OnClick_Nice()
    {
        if (snapchatAvailable && snapchatClicked)
        {
            PlayerPrefs.SetInt("snapchatAvailable", 1);
            AnalyticsManager.Instance.SendEvent(AnalyticsEvent.FollowSnapchat);
        }
        if (instAvailable && instClicked)
        {
            PlayerPrefs.SetInt("instAvailable", 1);
            AnalyticsManager.Instance.SendEvent(AnalyticsEvent.FollowInstagram);
        }
        if (youTubeAvailable && youTubeClicked)
        {
            PlayerPrefs.SetInt("youTubeAvailable", 1);
            AnalyticsManager.Instance.SendEvent(AnalyticsEvent.FollowYouTube);
        }
        if (twitterAvailable && twitterClicked)
        {
            PlayerPrefs.SetInt("twitterAvailable", 1);
            AnalyticsManager.Instance.SendEvent(AnalyticsEvent.FollowTwitter);
        }
        if (fbAvailable && fbClicked)
        {
            PlayerPrefs.SetInt("fbAvailable", 1);
            AnalyticsManager.Instance.SendEvent(AnalyticsEvent.FollowFacebook);
        }
        if (websiteAvailable && websiteClicked)
        {
            PlayerPrefs.SetInt("websiteAvailable", 1);
            AnalyticsManager.Instance.SendEvent(AnalyticsEvent.FollowWebsite);
        }

        GameManager.Instance.Gems += rewardGems;
        followUs_Panel.SetActive(false);
        UIManager.Instance.OnPanelOpen(false);
        OnFollowUsPanelOpen();
    }

    public void OnClick_TryAgain()
    {
        followUs_Panel.SetActive(true);
        UIManager.Instance.OnPanelOpen(true);
        OnFollowUsPanelOpen();
    }

    void NullifyCLickedFlags()
    {
        snapchatClicked = false;
        instClicked = false;
        youTubeClicked = false;
        twitterClicked = false;
        fbClicked = false;
        websiteClicked = false;
    }
}
