﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour {

    public static MusicManager Instance;

    public AudioClip firstPart, loopPart, fastPart;
    [HideInInspector]
    public bool canPlay;

    private AudioSource _audioSource;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (_audioSource.clip.name == firstPart.name && !_audioSource.isPlaying && canPlay)
        {
            canPlay = false;
            PlayLoopPart();
        }
    }

    public void PlayLoopPart()
    {
        _audioSource.Stop();
        _audioSource.clip = loopPart;
        _audioSource.loop = true;
        _audioSource.Play();
    }

    public void PlayFastPart()
    {
        _audioSource.Stop();
        _audioSource.clip = fastPart;
        _audioSource.Play();
    }
}
