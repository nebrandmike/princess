﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Analytics;

public enum AnalyticsEvent
{
    FollowSnapchat,
    FollowInstagram,
    FollowYouTube,
    FollowTwitter,
    FollowFacebook,
    FollowWebsite
}

public class AnalyticsManager : MonoBehaviour {

    public static AnalyticsManager Instance;
    public bool analyticsEnabled = true;
    public UnityEngine.UI.Text analyticsEnabledLabel;

    private int _touchCounter;

    void Awake()
    {
        Instance = this;
    }

    public void SendEvent(AnalyticsEvent e)
    {
        Analytics.CustomEvent(e.ToString());
    }

    void Update()
    {
        if(Input.anyKeyDown)
        {
            _touchCounter++;
        }
    }

    void OnApplicationQuit()
    {
#if UNITY_EDITOR
        SendAnalytics();
#endif
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus)
        {
            SendAnalytics();
        }
    }

    public void AnalyticsOnOff()
    {
        analyticsEnabled = !analyticsEnabled;
        analyticsEnabledLabel.text = "analytics: " + analyticsEnabled.ToString();
    }

    void SendAnalytics()
    {
        if (analyticsEnabled)
        {
            int secondsInGame = (int)TestManager.Instance.howLongPlaying;
            Analytics.CustomEvent("gameOver", new Dictionary<string, object>
        {
            { "touch counter", _touchCounter },
            { "total seconds in game", secondsInGame }
          });
        }
    }

    public void SendUnlockedHouse(int house)
    {
        if (analyticsEnabled)
        {
            string str = "unlock house " + house.ToString();

#if UNITY_IPHONE
            str += "_iOS";
#endif
            Analytics.CustomEvent(str);
        }
    }

    public void UserPressYesRateOnHouseX(int houseIndex)
    {
        if (analyticsEnabled)
        {
            string str = "UserPressYesRateOnHouse " + houseIndex.ToString();
#if UNITY_IPHONE
            str += "_iOS";
#endif
            Analytics.CustomEvent(str);
        }
    }

    public void UserWatchVideoSkipOrDoubleCard()
    {
        if (analyticsEnabled)
        {
            string str = "UserWatchVideoSkipOrDoubleCard";
#if UNITY_IPHONE
            str += "_iOS";
#endif
            Analytics.CustomEvent(str);
        }
    }

    public void UserWatchVideoAddWorker()
    {
        string str = "UserWatchVideoAddWorker";
#if UNITY_IPHONE
            str += "_iOS";
#endif
        if (analyticsEnabled)
        {
            Analytics.CustomEvent(str);
        }
    }

    public void UserStartWatchEpicReduceFishingTime()
    {
        string str = "UserStartWatchEpicReduceFishingTime";
#if UNITY_IPHONE
            str += "_iOS";
#endif
        if (analyticsEnabled)
        {
            Analytics.CustomEvent(str);
        }
    }

    public void UserStartWatchEpicImproveMinionsSpeed()
    {
        string str = "UserStartWatchEpicImproveMinionsSpeed";
#if UNITY_IPHONE
            str += "_iOS";
#endif
        if (analyticsEnabled)
        {
            Analytics.CustomEvent(str);
        }
    }

    public void UserStartWatchEpicFoodBoost()
    {
        string str = "UserStartWatchEpicFoodBoost";
#if UNITY_IPHONE
            str += "_iOS";
#endif
        if (analyticsEnabled)
        {
            Analytics.CustomEvent(str);
        }
    }

    public void UserWatchSecondGift()
    {
        string str = "UserWatchSecondGift";
#if UNITY_IPHONE
            str += "_iOS";
#endif
        if (analyticsEnabled)
        {
            Analytics.CustomEvent(str);
        }
    }

    public void SpendGemWorker()
    {
        string str = "SpendGemWorker";
#if UNITY_IPHONE
            str += "_iOS";
#endif
        if (analyticsEnabled)
        {
            Analytics.CustomEvent(str);
        }
    }

    public void SpendGemFoodAmount()
    {
        string str = "SpendGemFoodAmount";
#if UNITY_IPHONE
            str += "_iOS";
#endif
        if (analyticsEnabled)
        {
            Analytics.CustomEvent(str);
        }
    }

    public void SpendGemIncreaseFindGemsChance()
    {
        string str = "SpendGemIncreaseFindGemsChance";
#if UNITY_IPHONE
            str += "_iOS";
#endif
        if (analyticsEnabled)
        {
            Analytics.CustomEvent(str);
        }
    }

    public void SpendGemWalkingSpeed()
    {
        string str = "SpendGemWalkingSpeed";
#if UNITY_IPHONE
            str += "_iOS";
#endif
        if (analyticsEnabled)
        {
            Analytics.CustomEvent(str);
        }
    }

    public void SpendGemUpgradeHouse()
    {
        string str = "SpendGemUpgradeHouse";
#if UNITY_IPHONE
            str += "_iOS";
#endif
        if (analyticsEnabled)
        {
            Analytics.CustomEvent(str);
        }
    }

    public void SpendGemEpicUpgradeReduceFishingTime()
    {
        string str = "SpendGemEpicUpgradeReduceFishingTime";
#if UNITY_IPHONE
            str += "_iOS";
#endif
        if (analyticsEnabled)
        {
            Analytics.CustomEvent(str);
        }
    }

    public void SpendGemEpicUpgradeImproveMinionsSpeed()
    {
        string str = "SpendGemEpicUpgradeImproveMinionsSpeed";
#if UNITY_IPHONE
            str += "_iOS";
#endif
        if (analyticsEnabled)
        {
            Analytics.CustomEvent(str);
        }
    }

    public void SpendGemEpicUpgradeFoodBoost()
    {
        string str = "SpendGemEpicUpgradeFoodBoost";
#if UNITY_IPHONE
            str += "_iOS";
#endif
        if (analyticsEnabled)
        {
            Analytics.CustomEvent(str);
        }
    }

    public void SpendGemForSkipUpgradeProgressBar()
    {
        string str = "SpendGemForSkipUpgradeProgressBar";
#if UNITY_IPHONE
            str += "_iOS";
#endif
        if (analyticsEnabled)
        {
            Analytics.CustomEvent(str);
        }
    }

    public void SpendGemForSkipHarvestProgressBar()
    {
        string str = "SpendGemForSkipHarvestProgressBar";
#if UNITY_IPHONE
            str += "_iOS";
#endif
        if (analyticsEnabled)
        {
            Analytics.CustomEvent(str);
        }
    }

    public void TradeButtonPressed()
    {
        string str = "TradeButtonPressed";
#if UNITY_IPHONE
            str += "_iOS";
#endif
        if (analyticsEnabled)
        {
            Analytics.CustomEvent(str);
        }
    }

    public void MinionCount(int minionCount)
    {
        if (analyticsEnabled)
        {
            string str = "minion count " + minionCount.ToString();
#if UNITY_IPHONE
            str += "_iOS";
#endif
            Analytics.CustomEvent(str);
        }
    }

    public void UserClickLeftBallon()
    {
        string str = "UserClickLeftBallon";
#if UNITY_IPHONE
            str += "_iOS";
#endif
        if (analyticsEnabled)
        {
            Analytics.CustomEvent(str);
        }
    }

    public void UserClickRightBallon()
    {
        string str = "UserClickRightBallon";
#if UNITY_IPHONE
            str += "_iOS";
#endif
        if (analyticsEnabled)
        {
            Analytics.CustomEvent(str);
        }
    }

    public void TutourStepCompleted(int step)
    {
        string str = "tutour step " + step.ToString();
#if UNITY_IPHONE
            str += "_iOS";
#endif
        if (analyticsEnabled)
        {
            Analytics.CustomEvent(str);
        }
    }

    public void UserStartWatchIdleEpic()
    {
        string str = "UserStartWatchIdleEpic";
#if UNITY_IPHONE
            str += "_iOS";
#endif
        if (analyticsEnabled)
        {
            Analytics.CustomEvent(str);
        }
    }

    public void UserStartWatchSunEpic()
    {
        string str = "UserStartWatchSunEpic";
#if UNITY_IPHONE
            str += "_iOS";
#endif
        if (analyticsEnabled)
        {
            Analytics.CustomEvent(str);
        }
    }

    public void UserSpent5GemsForAutoRun()
    {
        string str = "UserSpent5GemsForAutoRun";
#if UNITY_IPHONE
            str += "_iOS";
#endif
        if (analyticsEnabled)
        {
            Analytics.CustomEvent(str);
        }
    }

    public void UserSpent5GemsForSuperMode()
    {
        string str = "UserSpent5GemsForSuperMode";
#if UNITY_IPHONE
            str += "_iOS";
#endif
        if (analyticsEnabled)
        {
            Analytics.CustomEvent(str);
        }
    }

    public void FacebookLikePressed()
    {
        string str = "FacebookLikePressed";
#if UNITY_IPHONE
            str += "_iOS";
#endif
        if (analyticsEnabled)
        {
            Analytics.CustomEvent(str);
        }
    }

    public void SharePressed()
    {
        string str = "CameraSharePressed";
#if UNITY_IPHONE
            str += "_iOS";
#endif
        if (analyticsEnabled)
        {
            Analytics.CustomEvent(str);
        }
    }

    public void FyberPressed()
    {
        string str = "FyberPressed";
#if UNITY_IPHONE
            str += "_iOS";
#endif
        if (analyticsEnabled)
        {
            Analytics.CustomEvent(str);
        }
    }

    public void GetFyberReward()
    {
        string str = "GetFyberReward";
#if UNITY_IPHONE
            str += "_iOS";
#endif
        if (analyticsEnabled)
        {
            Analytics.CustomEvent(str);
        }
    }
}
