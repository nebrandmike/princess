﻿using UnityEngine;
using System.Collections;

public class EpicShop : MonoBehaviour {

    public GameObject upgradesPanel, epicShopPanel, gemsShopPanel;
    public ScrollSnapRect scrollSnapRect;
    public GameObject backgoundGO;

    public void OnClick_OpenEpicShopPanel()
    {
        upgradesPanel.SetActive(false);
        gemsShopPanel.SetActive(false);
        epicShopPanel.SetActive(true);
        //scrollSnapRect.BackToFirstPage();
        UIManager.Instance.OnPanelOpen(true);
        backgoundGO.SetActive(true);
        SoundManager.Instance.PlaySound(Sounds.Panel);
    }

    public void OnClick_CloseEpicPanel()
    {
        if (epicShopPanel.activeSelf)
        {
            epicShopPanel.SetActive(false); 
        }
    }

    public void OnClick_OpenBackground()
    {
        backgoundGO.SetActive(true);
    }

    public void OnClick_CloseBackgroud()
    {
        backgoundGO.SetActive(false);
        UIManager.Instance.OnPanelOpen(false);
    }
}
