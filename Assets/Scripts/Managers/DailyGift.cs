﻿using UnityEngine;
using System.Collections;
using System;

public class DailyGift : MonoBehaviour {

    public static DailyGift Instance;

    public GameObject dailyGiftPanelGO, boxGO, giftGO;
    public Animator dailyGiftPanelAnim, giftAppearAnim;
    public Animator[] tapOnBoxAnim;
    public int tapsOnGift = 5;
    public GameObject hintLabelGO;
    public UnityEngine.UI.Text rewardLabel;
    public int minRewardGems = 1, maxRewardGems = 5;
    public GameObject videoBtnGO;
    public Transform coverTr, coverStartPosTr;
    public Animator giftAvailableAnim;

    private int _tapCounter;
    private bool _giftOpened;
    private int _giftCounter;
    private int _currBoxIndex;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        if (!Tutorial.Instance.activated)
        {
            if (GameManager.Instance.dailyGiftInsteadOfBonus)
            {
                int today = DateTime.Today.Day;
                if (PlayerPrefs.HasKey("day_gift"))
                {
                    DateTime dayBefore = Convert.ToDateTime(PlayerPrefs.GetString("day_gift"));
                    TimeSpan diff = DateTime.Today - dayBefore;
                    if (dayBefore.Date.Day != DateTime.Now.Day)
                    {
                        giftAvailableAnim.SetTrigger("forward");
                        SoundManager.Instance.PlaySound(Sounds.GiftAvailable);
                    }
                }
                else
                {
                    giftAvailableAnim.SetTrigger("forward");
                    SoundManager.Instance.PlaySound(Sounds.GiftAvailable);
                }
            }
        }
    }

    public void OnClick_GiftAvailable()
    {
        giftAvailableAnim.SetTrigger("back");
        Invoke("DisplayGift", 0.5f);
        SoundManager.Instance.PlaySound(Sounds.Click);
    }

    void DisplayGift()
    {
        _giftCounter++;
        dailyGiftPanelGO.SetActive(true);
        dailyGiftPanelAnim.SetTrigger("Appear");
        UIManager.Instance.OnPanelOpen(true);
        PlayerPrefs.SetString("day_gift", DateTime.Now.ToShortDateString());
        SoundManager.Instance.PlaySound(Sounds.Panel);
    }

    public void OnClick_CloseGiftPanel()
    {
        dailyGiftPanelAnim.SetTrigger("Dissapear");
        UIManager.Instance.OnPanelOpen(false);
        Invoke("HideGiftPanel", 0.5f);
        SoundManager.Instance.PlaySound(Sounds.Click);
    }

    void HideGiftPanel()
    {
        dailyGiftPanelGO.SetActive(false);
    }

    public void OnClick_TapOnGift(int boxNumber)
    {
        _currBoxIndex = boxNumber;

        //if (_tapCounter >= tapsOnGift && !_giftOpened)
        //{
            _giftOpened = true;
            _tapCounter = 0;
            StartCoroutine(OpenTheBox(boxNumber));
            SoundManager.Instance.PlaySound(Sounds.GiftBoxTap);
        //}
        //else if (_tapCounter < tapsOnGift && !_giftOpened)
        //{
        //    _tapCounter++;
        //    coverTr.position = coverStartPosTr.position;
        //    tapOnBoxAnim[boxNumber].SetTrigger("Start");
        //    SoundManager.Instance.PlaySound(Sounds.GiftBoxTap);
        //}
    }

    bool coroutineStarted = false;
    IEnumerator OpenTheBox(int boxNumber)
    {
        if (!coroutineStarted)
        {
            coroutineStarted = true;
            yield return new WaitForSeconds(0.2f);
            hintLabelGO.SetActive(false);
            tapOnBoxAnim[boxNumber].SetTrigger("CoverUp");
            SoundManager.Instance.PlaySound(Sounds.GiftBoxOpen);
            yield return new WaitForSeconds(0.5f);
            boxGO.SetActive(false);
            giftGO.SetActive(true);

            if (_giftCounter < 2)
                videoBtnGO.SetActive(true);
            else
                videoBtnGO.SetActive(false);

            giftAppearAnim.SetTrigger("Start");
            SoundManager.Instance.PlaySound(Sounds.GiftAppear);
            GiveReward();
            coroutineStarted = false;
        }
    }

    void GiveReward()
    {
        int reward = UnityEngine.Random.Range(minRewardGems, maxRewardGems);
        rewardLabel.text = reward.ToString();
        GameManager.Instance.Gems += reward;
    }

    bool dailyGiftVideoBtnClicked = false;
    public void OnClick_VideoBtn()
    {
        if (!dailyGiftVideoBtnClicked)
        {
            dailyGiftVideoBtnClicked = true;
            AdsManager.Instance.WatchCallbackVideo(VideoWatchedCallback, VideoType.DailyGift);
        }
    }

    public void VideoWatchedCallback()
    {
        AnalyticsManager.Instance.UserWatchSecondGift();
        giftGO.SetActive(false);
        boxGO.SetActive(true);
        tapOnBoxAnim[_currBoxIndex].SetTrigger("StartPos");
        hintLabelGO.SetActive(true);
        _giftOpened = false;
        _giftCounter++;
    }
}
