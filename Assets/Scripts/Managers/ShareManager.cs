﻿using UnityEngine;
using System.Collections;

public class ShareManager : MonoBehaviour
{
    [Header("Interrupt user in XXX sec from start")]
    public float secFromStart = 600;
    [Space]
    public NativeShare nativeShare;
    public GameObject shareProposalPanelGO;
    public GameObject backroundGO;
    public GameObject rewardPanelGO;

    void Start()
    {
        if(PlayerPrefs.GetInt("SharedBefore") == 0)
        {
            Invoke("InterruptUser", secFromStart);
        }
    }

    public void InterruptUser()
    {
        UIManager.Instance.CloseAllPanels();
        shareProposalPanelGO.SetActive(true);
        backroundGO.SetActive(true);
        SoundManager.Instance.PlaySound(Sounds.Panel);
    }

    private float shareTime;

    public void OnClick_Yes()
    {
        shareProposalPanelGO.SetActive(false);
        backroundGO.SetActive(false);
        nativeShare.ShareScreenshotWithText();
        SoundManager.Instance.PlaySound(Sounds.Click);
        AnalyticsManager.Instance.SharePressed();
        UIManager.Instance.OnPanelOpen(false);
        shareTime = Time.timeSinceLevelLoad;
    }

    public void OnClick_No()
    {
        shareProposalPanelGO.SetActive(false);
        backroundGO.SetActive(false);
        SoundManager.Instance.PlaySound(Sounds.Click);
        UIManager.Instance.OnPanelOpen(false);
    }

    public void GiveReward()
    {
        rewardPanelGO.SetActive(true);
        backroundGO.SetActive(true);
        SoundManager.Instance.PlaySound(Sounds.Panel);
    }

    public void OnClick_GetReward()
    {
        GameManager.Instance.Gems += 10;
        AchievementsManager.Instance.SendAchievement(AchievementName.GemsMagnat, 10, AchievementType.Countable);
        PlayerPrefs.SetInt("SharedBefore", 1);
        backroundGO.SetActive(false);
        Invoke("HideRewardPanel", 0.4f);
        SoundManager.Instance.PlaySound(Sounds.Click);
        UIManager.Instance.OnPanelOpen(false);
    }

    void HideRewardPanel()
    {
        rewardPanelGO.SetActive(false);
    }

    private void OnApplicationPause(bool pause)
    {
        if(!pause)
        {
            if(shareTime != 0)
            {
                if(Time.timeSinceLevelLoad - shareTime >= 15) // 15 seconds for share
                {   
                    GiveReward();
                }
                shareTime = 0;
            }
        }
    }
}
