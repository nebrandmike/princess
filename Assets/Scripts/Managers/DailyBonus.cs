﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class DailyBonus : MonoBehaviour {

    public static DailyBonus Instance;
    public int[] reward;
    public Text[] daysText;
    public GameObject dailyBonusPanel, videoWatchedPanel;
    public Text videoWatchedRewardLabel;
    public GameObject backgroundGO;
    public Animator doublePrizeBtnAnim;
    public Animator dailyBonPanelAnim;
    public GameObject notificationLabelGO;
    private int _currentReward;
    private int _dayInSequence;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        if (!Tutorial.Instance.activated)
        {
            if (!GameManager.Instance.dailyGiftInsteadOfBonus)
            {
                int today = DateTime.Today.Day;
                if (PlayerPrefs.HasKey("day"))
                {
                    DateTime dayBefore = Convert.ToDateTime(PlayerPrefs.GetString("day"));
                    TimeSpan diff = DateTime.Today - dayBefore;
                    if (dayBefore.Date.Day != DateTime.Now.Day)
                    {
                        dailyBonusPanel.SetActive(true);

                        if (diff.Hours > 24)
                        {
                            SetFirstDay();
                        }
                        else
                        {
                            ContinueSequence();
                        }
                    }
                }
                else SetFirstDay();
            }
        }
    }

    void SetFirstDay()
    {
        backgroundGO.SetActive(true);
        dailyBonusPanel.SetActive(true);
        dailyBonPanelAnim.SetTrigger("Appear");
        UIManager.Instance.OnPanelOpen(true);
        PlayerPrefs.SetString("day", DateTime.Now.ToShortDateString());
        _dayInSequence = 1;
        PlayerPrefs.SetInt("dayInSequence", 1);
        daysText[0].text = "today";
        _currentReward = reward[0];
    }

    void ContinueSequence()
    {
        _dayInSequence = PlayerPrefs.GetInt("dayInSequence");
        _dayInSequence++;
        _currentReward = reward[_dayInSequence - 1];
        daysText[_dayInSequence - 1].text = "today";
        if (_dayInSequence >= 5) _dayInSequence = 1;
        PlayerPrefs.SetString("day", DateTime.Now.ToShortDateString());
        PlayerPrefs.SetInt("dayInSequence", _dayInSequence);
    }

    public void OnClick_DoubleReward()
    {
        if (AdsManager.Instance.IsVideoAvailable())
        {
            AdsManager.Instance.PlayDailyBonusVideo();
            SoundManager.Instance.PlaySound(Sounds.Panel);
            doublePrizeBtnAnim.SetTrigger("Pressed");
            dailyBonPanelAnim.SetTrigger("Dissapear");
        }
        else
        {
            doublePrizeBtnAnim.SetTrigger("NotAvailable");
        }
    }

    void Update()
    {
        if(dailyBonusPanel.activeSelf)
        {
            notificationLabelGO.SetActive(!AdsManager.Instance.IsVideoAvailable());
        }
    }

    public void WatchVideoCallback()
    {
        switch (_dayInSequence)
        {
            case 1: _currentReward = 2; break;
            case 2: _currentReward = 8; break;
            case 3: _currentReward = 15; break;
            case 4: _currentReward = 30; break;
            case 5: _currentReward = 38; break;
        }
        dailyBonusPanel.SetActive(false);
        videoWatchedPanel.SetActive(true);
        UIManager.Instance.OnPanelOpen(true);
        videoWatchedPanel.GetComponent<Animator>().SetTrigger("Appear");
        videoWatchedRewardLabel.text = _currentReward.ToString();
        GameManager.Instance.Gems += _currentReward;
        AchievementsManager.Instance.SendAchievement(AchievementName.GemsMagnat, _currentReward, AchievementType.Countable);
    }

    public void OnClick_Claim()
    {
        backgroundGO.SetActive(false);
        //inGamePanel.SetActive(true);
        GameManager.Instance.Gems += _currentReward;
        SoundManager.Instance.PlaySound(Sounds.Panel);
        Invoke("HidePanels", 0.9f);
    }

    public void OnClick_VideoWatchedClaim()
    {
        SoundManager.Instance.PlaySound(Sounds.Click);
        backgroundGO.SetActive(false);
        Invoke("HidePanels", 0.9f);
    }

    void HidePanels()
    {
        dailyBonusPanel.SetActive(false);
        videoWatchedPanel.SetActive(false);
        UIManager.Instance.OnPanelOpen(false);
    }
}
