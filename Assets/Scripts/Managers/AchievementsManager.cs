﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;
using System;

public enum AchievementName
{
    UnlockHouse,    // unlock houses №2,3,4,5,6,7
    FoodMagnat,     // collected 1k, 10k, 100k, 500k, 1m food
    GemsMagnat,     // collected 10 gems, 100 gems, 1k, 2k, 3k gems
    BornMinions,    // 10, 25, 50, 100, 200
    TapOnHouse,     // 100, 1k, 10k, 100k, 500k
    PlayStraight,   // Play1hour straight (timer)
    Send100minonsToASingleHouse,
    DrawEachCard1Time, 
    LuckCard,       // Draw 3 good cards in row; 
    FinalArchivment // Unlock all others archivements
}

public enum AchievementType
{
    Countable,    // achievements has levels (food, gems, born minions)
    NotCountable, // unlock shops
    Disposable    // achievements can be unlocked only once (FinalArchivment, PlayStraight)
}

[System.Serializable]
public class Achievement
{
    public AchievementName name;
    public AchievementType type;
    public int currLevel; // save
    public int currValue; // save
    public int[] unlockValues;
    public GameObject[] stars;
    public bool displayProgress = true;
    public int[] gemsReward;
    public Text progressLabel;
    public Text gemsRewardLabel;
    public Image rewardBtnImg;
    public Transform rewardGemTr;
    public Transform gemsInitialPos;
    public GameObject achievementGO;
}

public class AchievementsManager : MonoBehaviour {

    public static AchievementsManager Instance;

    public List<Achievement> achievements;
    [Space]
    public Animator achievementUnlockedAnim;
    public GameObject achievementsPanelGO;
    public GameObject settingsPanelGO;
    public Color rewardAvailableBtnColor;
    public Color rewardUnavailableBtnColor;
    public bool[] unlockedAchievements; // save
    public bool[] achievementsFinished; // save
    public Transform gemPosTr;
    
    private TimeSpan _playStraightDuration = new TimeSpan(1, 0, 0);
    private DateTime _timeToPlayStraightUnlocking;

    private List<string> _drawEachCardLst = new List<string>();
    private int _luckCardsCounter;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        if(!achievementsFinished[5])
            _timeToPlayStraightUnlocking = DateTime.Now + _playStraightDuration;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            AchievementUnlocked();

        if (Input.GetKeyDown(KeyCode.Z))
            print(achievements[1].unlockValues[achievements[1].currLevel]);

        if(Input.GetKeyDown(KeyCode.Q))
        {
            GameManager.Instance.Food += 1000;
            SendAchievement(AchievementName.FoodMagnat, 1000, AchievementType.Countable);
        }

        if(Input.GetKeyDown(KeyCode.A))
        {
            SendAchievement(AchievementName.DrawEachCard1Time, 1, AchievementType.Disposable);
        }

        if (Input.GetKeyDown(KeyCode.P))
            PlusLuckCards();

        if (!achievementsFinished[5])
        {
            TimeSpan t = _timeToPlayStraightUnlocking - DateTime.Now;
            if (t.TotalSeconds < 1)
            {
                achievementsFinished[5] = true;
                SendAchievement(AchievementName.PlayStraight, 1, AchievementType.Disposable);
            }
        }
    }

    void AchievementUnlocked()
    {
        if(!achievementsPanelGO.activeSelf)
            achievementUnlockedAnim.SetTrigger("forward");

        Invoke("PlaySound", 0.4f);
        UpdateAchievementsUI();
    }

    void PlaySound()
    {
        SoundManager.Instance.PlaySound(Sounds.AchievementUnlocked);
    }

    public void OnClick_AchievementUnlocked()
    {
        achievementUnlockedAnim.SetTrigger("back");
        SoundManager.Instance.PlaySound(Sounds.Click);
        Invoke("OpenAchievementsPanel", 0.5f);
    }

    void OpenAchievementsPanel()
    {
        achievementsPanelGO.SetActive(true);
        settingsPanelGO.SetActive(false);
        achievementsPanelGO.GetComponent<Animator>().SetTrigger("Appear");
        OnClick_OpenAchievements();
    }

    public void OnClick_OpenAchievements()
    {
        UIManager.Instance.OnPanelOpen(true);
        achievementsPanelGO.SetActive(true);
        settingsPanelGO.SetActive(false);
        achievementsPanelGO.GetComponent<Animator>().SetTrigger("Appear");
        UpdateAchievementsUI();
        SoundManager.Instance.PlaySound(Sounds.Panel);
    }

    void UpdateAchievementsUI()
    {
        if (achievementsPanelGO.activeSelf)
        {
            for (int i = 0; i < achievements.Count; i++)
            {
                if (!unlockedAchievements[i])
                {
                    achievements[i].gemsRewardLabel.text = achievements[i].gemsReward[achievements[i].currLevel].ToString();
                }

                if (achievements[i].displayProgress && achievements[i].type == AchievementType.Countable)
                {
                    if (achievements[i].currLevel < achievements[i].unlockValues.Length)
                    {
                        achievements[i].progressLabel.text = Utilities.NiceFormat(achievements[i].currValue) +
                                                             "/" +
                                                             Utilities.NiceFormat(
                                                                 achievements[i].unlockValues[achievements[i].currLevel]);
                    }
                }
                else if(achievements[i].displayProgress && achievements[i].type == AchievementType.NotCountable)
                {
                    achievements[i].progressLabel.text = Utilities.NiceFormat(achievements[i].currLevel) +
                        //"/" + Utilities.NiceFormat(achievements[i].unlockValues[achievements[i].currLevel]);
                        "/" + achievements[i].unlockValues[achievements[i].currLevel];
                }

                if (unlockedAchievements[i])
                {
                    achievements[i].rewardBtnImg.color = rewardAvailableBtnColor;
                    achievements[i].rewardBtnImg.GetComponent<Animator>().SetTrigger("Activated");
                }

                for (int j = 0; j < achievements[i].currLevel; j++)
                {

                    if (achievements[i].currLevel > 0 && achievements[i].type != AchievementType.Disposable)
                    {
                        if(j <= achievements[i].stars.Length)
                            achievements[i].stars[j].SetActive(true);
                    }
                }
            }
        }
    }

    public void OnClick_CloseAchievements()
    {
        UIManager.Instance.OnPanelOpen(false);
        achievementsPanelGO.GetComponent<Animator>().SetTrigger("Dissapear");
        SoundManager.Instance.PlaySound(Sounds.ClosePanel);
        Invoke("HideAchievementsPanel", 0.5f);
    }

    void HideAchievementsPanel()
    {
        achievementsPanelGO.SetActive(false);
    }

    public void SendAchievement(AchievementName name, int value, AchievementType type)
    {
        for (int i = 0; i < achievements.Count; i++)
        {
            switch(type)
            {
                case AchievementType.NotCountable:
                    if (achievements[i].name == name)
                    {
                        achievements[i].currLevel++;
                        unlockedAchievements[i] = true;
                        AchievementUnlocked();
                    }
                    break;

                case AchievementType.Countable:
                    if (achievements[i].name == name)
                    {
                        achievements[i].currValue += value;
                        if (achievements[i].currLevel < achievements[i].unlockValues.Length)
                        {
                            if (achievements[i].currValue >= achievements[i].unlockValues[achievements[i].currLevel])
                            {
                                achievements[i].currLevel++;
                                unlockedAchievements[i] = true;
                                AchievementUnlocked();
                            }
                        }
                    }
                    break;

                case AchievementType.Disposable:
                    if (achievements[i].name == name)
                    {
                        unlockedAchievements[i] = true;
                        achievementsFinished[i] = true;
                        AchievementUnlocked();
                    }
                    break;
            }
        }

        CheckFinalAchievement();
    }

    public void OnClick_Reward(int achievementIndex)
    {
        if(unlockedAchievements[achievementIndex])
        {
            achievements[achievementIndex].rewardBtnImg.GetComponent<Animator>().SetTrigger("Clicked");
            achievements[achievementIndex].rewardBtnImg.color = rewardUnavailableBtnColor;
            unlockedAchievements[achievementIndex] = false;

            if (achievements[achievementIndex].type != AchievementType.Disposable)
            {
                GameManager.Instance.Gems += achievements[achievementIndex].gemsReward[achievements[achievementIndex].currLevel - 1];
                SendAchievement(AchievementName.GemsMagnat, achievements[achievementIndex].gemsReward[achievements[achievementIndex].currLevel - 1], AchievementType.Countable);
            }
            else
            {
                GameManager.Instance.Gems += achievements[achievementIndex].gemsReward[0];
                SendAchievement(AchievementName.GemsMagnat, achievements[achievementIndex].gemsReward[0], AchievementType.Countable);
            }

            if (achievements[achievementIndex].type != AchievementType.Disposable)
            {
                achievements[achievementIndex].gemsRewardLabel.text = achievements[achievementIndex].gemsReward[achievements[achievementIndex].currLevel].ToString();
                achievements[achievementIndex].rewardGemTr.DOMove(gemPosTr.position, 0.3f).OnComplete(() =>
                achievements[achievementIndex].rewardGemTr.DOMove(achievements[achievementIndex].gemsInitialPos.position, 0).OnComplete(() =>
                achievements[achievementIndex].rewardGemTr.DOScale(0, 0).OnComplete(() =>
                achievements[achievementIndex].rewardGemTr.DOScale(1, 0.3f)))
                );
            }
            else
            {
                achievements[achievementIndex].rewardGemTr.DOMove(gemPosTr.position, 0.3f);
                achievementsFinished[achievementIndex] = true;
                StartCoroutine(HideAchievement(achievementIndex));
            }

            SoundManager.Instance.PlaySound(Sounds.Click);
        }
    }

    IEnumerator HideAchievement(int achievementIndex)
    {
        yield return new WaitForSeconds(0.5f);
        achievements[achievementIndex].rewardGemTr.gameObject.SetActive(false);
        achievements[achievementIndex].achievementGO.transform.DOScale(0, 0.3f).OnComplete(() =>
                achievements[achievementIndex].achievementGO.SetActive(false));
    }

    void CheckAchievementsFinished()
    {
        for (int i = 0; i < achievementsFinished.Length; i++)
        {
            if(achievementsFinished[i])
            {
                achievements[i].achievementGO.SetActive(false);
            }
        }
    }

    public void AddCardToLst(string cardName)
    {
        if (!achievementsFinished[7])
        {
            bool hasThisCard = false;
            for (int i = 0; i < _drawEachCardLst.Count; i++)
            {
                if (cardName == _drawEachCardLst[i])
                    hasThisCard = true;
            }
            if (!hasThisCard)
            {
                _drawEachCardLst.Add(cardName);
                if (_drawEachCardLst.Count >= (CardsManager.Instance.goodCards.Count + CardsManager.Instance.badCards.Count))
                    SendAchievement(AchievementName.DrawEachCard1Time, 1, AchievementType.Disposable);
            }
        }
    }

    public void PlusLuckCards()
    {
        if (!achievementsFinished[8])
        {
            _luckCardsCounter++;
            if(_luckCardsCounter == 3)
            {
                SendAchievement(AchievementName.LuckCard, 1, AchievementType.Disposable);
            }
        }
    }

    public void ReserLuckCardsCounter()
    {
        _luckCardsCounter = 0;
    }

    void CheckFinalAchievement()
    {
        if (achievementsFinished[9])
        {
            int counter = 0;
            for (int i = 0; i < achievementsFinished.Length; i++)
            {
                if (achievementsFinished[i])
                    counter++;
            }

            if (counter >= achievements.Count)
                SendAchievement(AchievementName.FinalArchivment, 1, AchievementType.Disposable);
        }
    }
}
