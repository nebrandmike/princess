﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

[System.Serializable]
public class EpicUpgrade
{
    public GameObject timerGO;
    public EpicUpgradeType type;
    public Animator videoAnim;
    public Animator gemsAnim;
    public int gemsPrice;
    public GameObject iconGO;
    public Image iconImg;
}

public enum EpicUpgradeType
{
    None,
    ReduceFishingTime,
    ImproveMinionsSpeed,
    FoodBoost,
    AutoRun,
    SuperMode
}

public class EpicShopManager : MonoBehaviour
{
    public static EpicShopManager Instance;

    public List<EpicUpgrade> upgrades;

    public GameObject[] timerGO;
    public Text[] timerLabel;
    public GameObject idleVideoBtnGO, idleGemsBtnGO;
    public EpicShop epicShop;

    public bool superModeActivated;
    public GameObject superModeLabelOnScreen;
    public GameObject superModeVideoBtnGO, superModeGemsBtnGO;
    public GameObject[] superModeUIElementsToHide;
    public SuperModeTutorial superModeTutorial;

    private List<GameObject> _activeElementsToHide = new List<GameObject>();

    private TimeSpan _upgradesDuration = new TimeSpan(2, 0, 0);
    private DateTime[] _timeToEndUpgrade = { new DateTime(), new DateTime(), new DateTime() };
    private DateTime[] _epicTimeStamp = { new DateTime(), new DateTime(), new DateTime() };
    private bool[] _epicActivated = { false, false, false };

    private List<EpicUpgradeType> _epicType = new List<EpicUpgradeType>();
    private double speed;
    private double[] currentAmount = { 0, 0, 0 };

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        // UNCOMMENT IF YOU WANT BACK EPIC UPGRADES!!!

        //_epicType.Add(EpicUpgradeType.ReduceFishingTime);
        //_epicType.Add(EpicUpgradeType.ImproveMinionsSpeed);
        //_epicType.Add(EpicUpgradeType.FoodBoost);

        //SetSpeed();

        //if (PlayerPrefs.GetString("_epicTimeStamp[0]") != "")
        //{
        //    long temp = Convert.ToInt64(PlayerPrefs.GetString("_epicTimeStamp[0]"));
        //    _epicTimeStamp[0] = DateTime.FromBinary(temp);
        //    long temp2 = Convert.ToInt64(PlayerPrefs.GetString("_timeToEndUpgrade[0]"));
        //    _timeToEndUpgrade[0] = DateTime.FromBinary(temp2);
        //    string type = PlayerPrefs.GetString("EpicType[0]");
        //    _epicActivated[0] = true;
        //    currentAmount[0] = PlayerPrefs.GetFloat("currentAmount[0]");
        //    TimeSpan t = _timeToEndUpgrade[0] - DateTime.Now;
        //    ReduceFishingTimeApply(t.TotalSeconds > 0, t);
        //}

        //if (PlayerPrefs.GetString("_epicTimeStamp[1]") != "")
        //{
        //    long temp = Convert.ToInt64(PlayerPrefs.GetString("_epicTimeStamp[1]"));
        //    _epicTimeStamp[1] = DateTime.FromBinary(temp);
        //    long temp2 = Convert.ToInt64(PlayerPrefs.GetString("_timeToEndUpgrade[1]"));
        //    _timeToEndUpgrade[1] = DateTime.FromBinary(temp2);
        //    string type = PlayerPrefs.GetString("EpicType[1]");
        //    _epicActivated[1] = true;
        //    currentAmount[1] = PlayerPrefs.GetFloat("currentAmount[1]");
        //    TimeSpan t = _timeToEndUpgrade[1] - DateTime.Now;
        //    ImproveMinionsSpeedApply(t.TotalSeconds > 0, t);
        //}

        //if (PlayerPrefs.GetString("_epicTimeStamp[2]") != "")
        //{
        //    long temp = Convert.ToInt64(PlayerPrefs.GetString("_epicTimeStamp[2]"));
        //    _epicTimeStamp[2] = DateTime.FromBinary(temp);
        //    long temp2 = Convert.ToInt64(PlayerPrefs.GetString("_timeToEndUpgrade[2]"));
        //    _timeToEndUpgrade[2] = DateTime.FromBinary(temp2);
        //    string type = PlayerPrefs.GetString("EpicType[2]");
        //    _epicActivated[2] = true;
        //    currentAmount[2] = PlayerPrefs.GetFloat("currentAmount[2]");
        //    TimeSpan t = _timeToEndUpgrade[2] - DateTime.Now;
        //    FoodBoostApply(t.TotalSeconds > 0, t);
        //}
    }

    void FixedUpdate()
    {
        // UNCOMMENT IF YOU WANT BACK EPIC UPGRADES!!!

        //CheckIfActivated(0);
        //CheckIfActivated(1);
        //CheckIfActivated(2);

        if (epicShop.epicShopPanel.activeSelf)
        {
            if (MinionBot.Instance.state == MinionBot.State.Unactive)
            {
                idleVideoBtnGO.SetActive(true);
                idleGemsBtnGO.SetActive(true);
            }
            else
            {
                idleVideoBtnGO.SetActive(false);
                idleGemsBtnGO.SetActive(false);
            }

            if (superModeActivated)
            {
                superModeVideoBtnGO.SetActive(false);
                superModeGemsBtnGO.SetActive(false);
            }
            else
            {
                if (!superModeTutorial.gameplayInterrupted)
                {
                    superModeVideoBtnGO.SetActive(true);
                    superModeGemsBtnGO.SetActive(true);
                }
            }
        }
    }

    void CheckIfActivated(int index)
    {
        if (_epicActivated[index])
        {
            timerGO[index].SetActive(true);
            upgrades[index].videoAnim.gameObject.SetActive(false);
            upgrades[index].gemsAnim.gameObject.SetActive(false);
            TimeSpan t = DateTime.Now - _epicTimeStamp[index];
            TimeSpan t2 = _timeToEndUpgrade[index] - DateTime.Now;

            //bool notif

            if (t2.TotalSeconds > 0)
            {
                timerLabel[index].text = ToReadableString(t2);
                upgrades[index].iconGO.SetActive(true);
            }
            else
            {
                EpicUpgradeEnded(index);
                timerLabel[index].text = "";
            }
        }
        else
        {
            upgrades[index].iconGO.SetActive(false);
            upgrades[index].videoAnim.gameObject.SetActive(true);
            upgrades[index].gemsAnim.gameObject.SetActive(true);
        }
    }

    public string ToReadableString(TimeSpan span)
    {
        string formatted = string.Format("{0}{1}{2}",
            span.Duration().Hours > 0 ? string.Format("{0:0}{1}:", span.Hours, span.Hours == 1 ? String.Empty : "") : string.Empty,
            span.Duration().Minutes > 0 ? string.Format("{0:0}{1}:", span.Minutes, span.Minutes == 1 ? String.Empty : "") : string.Empty,
            span.Duration().Seconds > 0 ? string.Format("{0:0}{1}", span.Seconds, span.Seconds == 1 ? String.Empty : "") : string.Empty);

        if (formatted.EndsWith(",")) formatted = formatted.Substring(0, formatted.Length - 2);

        if (string.IsNullOrEmpty(formatted)) formatted = "0";

        return formatted;
    }

    public void OnClick_EpicUpgradeVideo(int upgradeIndex)
    {
        //if (AdsManager.Instance.IsVideoAvailable() && !_epicActivated[upgradeIndex])
        if (AdsManager.Instance.IsVideoAvailable())
        {
            switch (upgradeIndex)
            {
                case 0:
                    VideoCallbackDelegate del = ReduceFishingTime;
                    AnalyticsManager.Instance.UserStartWatchEpicReduceFishingTime();
                    AdsManager.Instance.WatchCallbackVideo(del, VideoType.EpicUpgrade);
                    break;
                case 1:
                    AnalyticsManager.Instance.UserStartWatchEpicImproveMinionsSpeed();
                    VideoCallbackDelegate del2 = ImproveMinionsSpeed;
                    AdsManager.Instance.WatchCallbackVideo(del2, VideoType.EpicUpgrade);
                    break;
                case 2:
                    AnalyticsManager.Instance.UserStartWatchEpicFoodBoost();
                    VideoCallbackDelegate del3 = FoodBoost;
                    AdsManager.Instance.WatchCallbackVideo(del3, VideoType.EpicUpgrade);
                    break;
                case 3:
                    AnalyticsManager.Instance.UserStartWatchIdleEpic();
                    VideoCallbackDelegate del4 = Idle;
                    AdsManager.Instance.WatchCallbackVideo(del4, VideoType.EpicUpgrade);
                    break;
                case 4:
                    AnalyticsManager.Instance.UserStartWatchSunEpic();
                    VideoCallbackDelegate del5 = SuperModeActivated;
                    AdsManager.Instance.WatchCallbackVideo(del5, VideoType.EpicUpgrade);
                    break;
            }
        }
        else
        {
            upgrades[upgradeIndex].videoAnim.SetTrigger("NotEnough");
            SoundManager.Instance.PlaySound(Sounds.NotEnough);
        }
    }

    public void OnClick_BuyUpgrade(int upgradeIndex)
    {
        //if (GameManager.Instance.Gems >= upgrades[upgradeIndex].gemsPrice && !_epicActivated[upgradeIndex])
        //{
        if (GameManager.Instance.Gems >= upgrades[upgradeIndex].gemsPrice)
        {
            GameManager.Instance.Gems -= upgrades[upgradeIndex].gemsPrice;
            SoundManager.Instance.PlaySound(Sounds.Upgrade);
            upgrades[upgradeIndex].gemsAnim.SetTrigger("Pressed");
            //timerGO[upgradeIndex].SetActive(true);
            switch (upgradeIndex)
            {
                case 0:
                    AnalyticsManager.Instance.SpendGemEpicUpgradeReduceFishingTime();
                    ReduceFishingTime();
                    break;
                case 1:
                    AnalyticsManager.Instance.SpendGemEpicUpgradeImproveMinionsSpeed();
                    ImproveMinionsSpeed();
                    break;
                case 2:
                    AnalyticsManager.Instance.SpendGemEpicUpgradeFoodBoost();
                    FoodBoost();
                    break;
                case 3:
                    AnalyticsManager.Instance.UserSpent5GemsForAutoRun();
                    Idle();
                    break;
                case 4:
                    AnalyticsManager.Instance.UserSpent5GemsForSuperMode();
                    SuperModeActivated();
                    break;
            }
        }
        else
        {
            upgrades[upgradeIndex].gemsAnim.SetTrigger("NotEnoughGems");
            SoundManager.Instance.PlaySound(Sounds.NotEnough);
        }
    }

    void SetSpeed()
    {
        TimeSpan t = _timeToEndUpgrade[0] - DateTime.Now;
        speed = 100 / t.TotalSeconds;
    }

    #region ReduceFishingTime
    public void ReduceFishingTime()
    {
        print("ReduceFishingTime");
        StartTimer(0);
        _epicType[0] = EpicUpgradeType.ReduceFishingTime;
        PlayerPrefs.SetString("EpicType", _epicType.ToString());
        ReduceFishingTimeApply(true, _upgradesDuration);
    }

    void ReduceFishingTimeApply(bool started, TimeSpan durationSec)
    {
        SetSpeed();
        for (int i = 0; i < GameManager.Instance.zones.Count; i++)
        {
            if (started)
                GameManager.Instance.zones[i].ReduceFishingTimeStarted();
            else
                GameManager.Instance.zones[i].ReduceFishingTimeEnded();
        }

        if (started)
        {
            if (PlayerPrefs.GetInt("ReduceFishingTimeApply") == 0)
            {
                PlayerPrefs.SetInt("ReduceFishingTimeApply", 1);
                //CatNotificationManager.Instance.SendNotification(durationSec.TotalSeconds, NotificationType.ReduceFishingTime);
            }
        }
        else if(!started) // if finished
        {
            PlayerPrefs.SetInt("ReduceFishingTimeApply", 0);
        }
    }
    #endregion

    #region ImproveMinionsSpeed
    public void ImproveMinionsSpeed()
    {
        StartTimer(1);
        _epicType[1] = EpicUpgradeType.ImproveMinionsSpeed;
        PlayerPrefs.SetString("EpicType", _epicType.ToString());
        ImproveMinionsSpeedApply(true, _upgradesDuration);
    }

    public bool improveMinionsSpeedActivated = false;
    void ImproveMinionsSpeedApply(bool started, TimeSpan durationSec)
    {
        SetSpeed();
        improveMinionsSpeedActivated = started;
        if (started)
        {
            for (int i = 0; i < MinionsManager.Instance.minions.Count; i++)
            {
                MinionsManager.Instance.minions[i].ImproveSpeedStarted();
            }
        }
        else
        {
            for (int i = 0; i < MinionsManager.Instance.minions.Count; i++)
            {
                MinionsManager.Instance.minions[i].ImproveSpeedEnded();
            }
        }
        if (started)
        {
            if (PlayerPrefs.GetInt("ImproveMinionsSpeedApply") == 0)
            {
                PlayerPrefs.SetInt("ImproveMinionsSpeedApply", 1);
                //CatNotificationManager.Instance.SendNotification(durationSec.TotalSeconds, NotificationType.ImproveMinionsSpeed);
            }
        }
        else if(!started) // if finished
        {
            PlayerPrefs.SetInt("ImproveMinionsSpeedApply", 0);
        }
    }

    #endregion

    #region Food Boost 25%

    public void FoodBoost()
    {
        StartTimer(2);
        _epicType[2] = EpicUpgradeType.FoodBoost;
        PlayerPrefs.SetString("EpicType", _epicType.ToString());
        FoodBoostApply(true, _upgradesDuration);
    }

    void FoodBoostApply(bool started, TimeSpan durationSec)
    {
        SetSpeed();
        for (int i = 0; i < GameManager.Instance.zones.Count; i++)
        {
            if (started)
                GameManager.Instance.zones[i].FoodBoostStarted();
            else
                GameManager.Instance.zones[i].FoodBoostEnded();
        }

        if (started)
        {
            if (PlayerPrefs.GetInt("FoodBoostApply") == 0)
            {
                PlayerPrefs.SetInt("FoodBoostApply", 1);
                //CatNotificationManager.Instance.SendNotification(durationSec.TotalSeconds, NotificationType.FoodBoost);
            }
        }
        else if (!started) // if finished
        {
            PlayerPrefs.SetInt("FoodBoostApply", 0);
        }
    }

    #endregion

    #region Idle

    public void Idle()
    {
        MinionBot.Instance.Activate(600);
    }

    #endregion

    #region Super Mode

    public void SuperModeActivated()
    {
        superModeActivated = true;
        superModeLabelOnScreen.SetActive(true);
        for (int i = 0; i < superModeUIElementsToHide.Length; i++)
        {
            if (superModeUIElementsToHide[i].activeSelf)
            {
                _activeElementsToHide.Add(superModeUIElementsToHide[i]);
            }
        }
        for (int i = 0; i < _activeElementsToHide.Count; i++)
        {
            _activeElementsToHide[i].SetActive(false);
        }
        MusicManager.Instance.PlayFastPart();
        PlayerPrefs.SetInt("EpicShopInterrupted", 1);
        Invoke("SuperModeDeactivated", 30);
    }

    void SuperModeDeactivated()
    {
        superModeLabelOnScreen.SetActive(false);
        superModeActivated = false;
        for (int i = 0; i < _activeElementsToHide.Count; i++)
        {
            _activeElementsToHide[i].SetActive(true);
        }
        MusicManager.Instance.PlayLoopPart();
        superModeTutorial.gameplayInterrupted = false;
    }

    #endregion

    void StartTimer(int upgradeIndex)
    {
        _epicTimeStamp[upgradeIndex] = DateTime.Now;
        _timeToEndUpgrade[upgradeIndex] = DateTime.Now + _upgradesDuration;
        string nameToSave = "_epicTimeStamp[" + upgradeIndex.ToString() + "]"; 
        PlayerPrefs.SetString(nameToSave, _epicTimeStamp[upgradeIndex].ToBinary().ToString());
        string nameToSave2 = "_timeToEndUpgrade[" + upgradeIndex.ToString() + "]";
        PlayerPrefs.SetString(nameToSave2, _timeToEndUpgrade[upgradeIndex].ToBinary().ToString());
        _epicActivated[upgradeIndex] = true;
    }

    void EpicUpgradeEnded(int upgradeIndex)
    {
        _epicActivated[upgradeIndex] = false;
        timerGO[upgradeIndex].SetActive(false);
        upgrades[upgradeIndex].videoAnim.gameObject.SetActive(true);
        upgrades[upgradeIndex].gemsAnim.gameObject.SetActive(true);
        TimeSpan t = new TimeSpan(0, 0, 0);
        switch (_epicType[upgradeIndex])
        {
            case EpicUpgradeType.ReduceFishingTime:
                ReduceFishingTimeApply(false, t);
                break;
            case EpicUpgradeType.ImproveMinionsSpeed:
                ImproveMinionsSpeedApply(false, t);
                break;
            case EpicUpgradeType.FoodBoost:
                FoodBoostApply(false, t);
                break;
        }
    }
}
