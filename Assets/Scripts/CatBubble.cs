﻿using UnityEngine;
using System.Collections;

public class CatBubble : MonoBehaviour {

    public static CatBubble Instance;

    public GameObject catBubbleGO;
    public UnityEngine.UI.Text catBubbleLabel;

    void Awake()
    {
        Instance = this;
    }

    public void DisplayCatBubble(string label, bool open)
    {
        print("display cat bubble");
        catBubbleLabel.text = label;
        if(open)
            catBubbleGO.GetComponent<Animator>().SetTrigger("Appear");
        else
            catBubbleGO.GetComponent<Animator>().SetTrigger("Dissapear");
    }

    public void CloseCatBubble()
    {
        catBubbleGO.GetComponent<Animator>().SetTrigger("Dissapear");
    }
}
