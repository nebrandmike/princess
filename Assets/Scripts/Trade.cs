﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Trade : MonoBehaviour {

    public static Trade Instance;

    public GameObject tradeBtnGO;
    //public GameObject haventEnoughMoneyLabelGO;
    public GameObject tradePanelGO, tradeCompletedGO;

    public Text minionsLabel;
    public Text foodLabel;
    public Text youreGetFoodLabel;
    public Text totalFoodLabel;

    private int _foodAmountForUser = 100;

    void Awake()
    {
        Instance = this;
    }

    public void OnGemsShopOpen()
    {
        tradePanelGO.SetActive(true);
        tradeCompletedGO.SetActive(false);

        if (GameManager.Instance.NextZoneIndex() < 3)
            _foodAmountForUser = 100;
        else if (GameManager.Instance.NextZoneIndex() >= 3 && GameManager.Instance.NextZoneIndex() < 6)
            _foodAmountForUser = 150;

        minionsLabel.text = MinionsManager.Instance.TotalMinions().ToString();
        foodLabel.text = _foodAmountForUser.ToString();
        totalFoodLabel.text = (MinionsManager.Instance.TotalMinions() * _foodAmountForUser).ToString();
        
        if (GameManager.Instance.Gems >= 5)
        {
            tradeBtnGO.SetActive(true);
            //tradeBtnGO.GetComponent<Animator>().SetTrigger("Pressed");
            //haventEnoughMoneyLabelGO.SetActive(false);
        }
        else
        {
            
            //haventEnoughMoneyLabelGO.SetActive(true);
            //tradeBtnGO.SetActive(false);
        }
    }

    public void OnClick_Trade()
    {
        if (GameManager.Instance.Gems >= 5)
        {
            GameManager.Instance.Gems -= 5;
            GameManager.Instance.Food += MinionsManager.Instance.TotalMinions() * _foodAmountForUser;
            tradePanelGO.SetActive(false);
            tradeCompletedGO.SetActive(true);
            youreGetFoodLabel.text = (MinionsManager.Instance.TotalMinions() * _foodAmountForUser).ToString();
            SoundManager.Instance.PlaySound(Sounds.BuyAll);
            AnalyticsManager.Instance.TradeButtonPressed();
        }
        else
        {
            tradeBtnGO.GetComponent<Animator>().SetTrigger("NotEnough");
        }
    }

    public void OnClick_OKTradeCompleted()
    {
        OnGemsShopOpen();
        SoundManager.Instance.PlaySound(Sounds.Click);
    }
}
