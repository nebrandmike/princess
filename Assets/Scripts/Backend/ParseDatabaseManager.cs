﻿using UnityEngine;
using System.Collections;
using Parse;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Globalization;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class ParseDatabaseManager : MonoBehaviour
{

    const string glyphsChar = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"; //
    const string glyphsNum = "123456789"; //

    //List<string> strSponsoredAppLinkLst = new List<string>();
    //List<string> strSponsoredPhotoLinkLst = new List<string>();

    public List<string> strOurAppLinkLst = new List<string>();
    public List<string> strOurPhotoLinkLst = new List<string>();

    //public string[] strSponsoredAppLinksArray = { "", "", "", "", "", "", "", "", "", "" };
    //public string[] strSponsoredAppPhotoLinksArray = { "", "", "", "", "", "", "", "", "", "" };

    //public string[] strOurAppLinksArray = { "", "", "", "", "", "", "", "", "", "" };
    //public string[] strOurAppPhotoLinksArray = { "", "", "", "", "", "", "", "", "", "" };
    [Space]
    public UnityEngine.UI.Text backendTestLabel;

    public FyberManager fyberManager;
	
	public bool beckendEnabled;

    private bool _promoCodeAvailable;
    private bool _codeIsInvalid;
    private string _priceTier;

    // Use this for initialization
    //codes http://parseplatform.github.io/docs/unity/guide/#queries
    void Start()
    {
        if (FourDaysLeft() && beckendEnabled)
        {
            //Link to backend https://parse-dashboard.back4app.com/apps/7f56b372-6af9-4b9c-83a1-3ad59bf94278/browser/AdButtonInfo
            //Custom parse server on back4app.com server
            ParseClient.Initialize(new ParseClient.Configuration
            {
                ApplicationId = "Yb0Y56z23LIsz6hx5d99v7u8XqG1b5v7dkD4V9fi",
                Server = "https://parseapi.back4app.com/",
                WindowsKey = "CRCHcmZophOMusNv4Eb2FgN3k6z2g7PXl8WQXjq6"
            });

            getParseAdButtonInfos();
        }

#if UNITY_IPHONE
        GetParseFyberAvilabilityOniOS();
#endif
    }

    #region Promo Codes

    public void GeneratePromoCodeClicked()
    {
        ParseClient.Initialize(new ParseClient.Configuration
        {
            ApplicationId = "Yb0Y56z23LIsz6hx5d99v7u8XqG1b5v7dkD4V9fi",
            Server = "https://parseapi.back4app.com/",
            WindowsKey = "CRCHcmZophOMusNv4Eb2FgN3k6z2g7PXl8WQXjq6"
        });

        generateNewUser();
        //addCodeToDatabase();
        addCodeToDatabaseWithUserReference();
        //Debug.Log("codeValid:" + isCodeValid("X2N8L8W8R6"));
        //redeemCode("X2N8L8W8R6");
    }

    //helper method to create a random string with 12chars, A1B2C3..
    string generateRandomCode()
    {
        int minCharAmount = 6;
        string myCode = "";
        for (int i = 0; i < minCharAmount; i++)
        {
            myCode += glyphsChar[UnityEngine.Random.Range(0, glyphsChar.Length)];
            myCode += glyphsNum[UnityEngine.Random.Range(0, glyphsNum.Length)];
        }

        return myCode;
    }

    /*     void generateNewUser()
        {
            if (ParseUser.CurrentUser == null)
            {
                // do stuff with the user
                var user = new ParseUser()
                {
                    Username = generateRandomCode(),
                    //Username = "q1w2e3r4t5",
                    Password = generateRandomCode(),
                    //Password = "q1w2e3r4t5",
                    Email = ""
                };


                // other fields can be set just like with ParseObject
                //user["phone"] = "415-392-0202";

                user.SignUpAsync();

                Debug.Log("setting up new user");

            }
            else
            {
                // show the signup or login screen
                Debug.Log("user already signup");

            }
        } */

    void generateNewUser()
    {


        //ParseUser.LogOut();
        
        if (ParseUser.CurrentUser == null)
        {
            //_username = generateRandomCode();
            // do stuff with the user
            var user = new ParseUser()
            {
                Username = generateRandomCode(),
                Password = generateRandomCode(),
                Email = generateRandomCode() + "@test.com"
            };

            

            // other fields can be set just like with ParseObject
            //user["phone"] = "415-392-0202";

            Task signUpTask = user.SignUpAsync().ContinueWith(t =>
            {
                if (t.IsCanceled)
                {
                    Debug.Log("*Se ha cancelado el registro");

                }
                else if (t.IsFaulted)
                {

                    Debug.Log("*It is possible that the username already exists, please choose another name");
                }
                else
                {


                    Debug.Log("success???");
                }

            });

            Debug.Log("setting up new user");

        }
        else
        {
            // show the signup or login screen
            Debug.Log("user already signup");

        }

        PlayerPrefs.SetString("Username", ParseUser.CurrentUser.ObjectId);
    }

    public void redeemCode(string codeToRedeem)
    {
        print("redeemCode " + codeToRedeem);

        var query = ParseObject.GetQuery("InAppCodes")
            .WhereEqualTo("strCode", codeToRedeem);
        query = query.Limit(1);

        query.FindAsync().ContinueWith(t =>
        {
            IEnumerable<ParseObject> results = t.Result;

            foreach (var result in results)
            {
                var gameScore = (ParseObject)result;

                // This does not require a network access.
                gameScore["boolAvailable"] = false;
                gameScore["strNote"] = "Code is invalid now";
                _priceTier = gameScore["priceTier"].ToString();
                print("price tier " + _priceTier);
                gameScore.SaveAsync();

                Debug.Log("Code is invalid now:" + codeToRedeem);
                _promoCodeAvailable = true;
            }
        });
    }

    public void Init()
    {
        ParseClient.Initialize(new ParseClient.Configuration
        {
            ApplicationId = "Yb0Y56z23LIsz6hx5d99v7u8XqG1b5v7dkD4V9fi",
            Server = "https://parseapi.back4app.com/",
            WindowsKey = "CRCHcmZophOMusNv4Eb2FgN3k6z2g7PXl8WQXjq6"
        });
    }

    //check if the code is valid
    public bool isCodeValid(string codeToRedeem)
    {
        print("isCodeValid " + codeToRedeem);

        var query = ParseObject.GetQuery("InAppCodes")
            .WhereEqualTo("strCode", codeToRedeem);
        query = query.Limit(1);

        query.FindAsync().ContinueWith(t =>
        {
            IEnumerable<ParseObject> results = t.Result;

            foreach (var result in results)
            {
                // This does not require a network access.
                string strCode = result.Get<string>("strCode");
                bool codeAvailable = result.Get<bool>("boolAvailable");
                Debug.Log(" this code: " + strCode + " isCodeValid: " + codeAvailable);
                //string username = result.Get<string>("user");
                //print("username = " + username);
                //print("username = " + username + " ParseUser.CurrentUser = " + ParseUser.CurrentUser.ToString());
                //if (username == ParseUser.CurrentUser.ToString())
                //{
                //    _codeIsInvalid = true;
                //    return false;
                //}

                if (codeAvailable)
                {
                    redeemCode(codeToRedeem);
                    return true;
                }
                else
                {
                    _codeIsInvalid = true;
                    return false;
                }
            }

            return false;

        });

        return false;
    }

    //check if the code is valid
    public bool isCodeValidWithUser(string codeToRedeem)
    {

        if (ParseUser.CurrentUser == null)
        {
            Debug.Log("no currentuser");
            return false;

        }

        if (PlayerPrefs.GetString("Username") != ParseUser.CurrentUser.ObjectId)
        {

            //print("ParseUser.CurrentUser.ObjectId = " + ParseUser.CurrentUser.ObjectId);

            var query = ParseObject.GetQuery("InAppCodes")
             .WhereEqualTo("strCode", codeToRedeem);
            query.WhereNotEqualTo("user", ParseUser.CurrentUser.ObjectId);
            query = query.Limit(1);

            query.FindAsync().ContinueWith(t =>
            {

                IEnumerable<ParseObject> results = t.Result;


                foreach (var result in results)
                {
                    // This does not require a network access.
                    Debug.Log("isCodeValidWithUser: success query");
                    string strCode = result.Get<string>("strCode");
                    bool codeAvailable = result.Get<bool>("boolAvailable");
                    Debug.Log(" this code: " + strCode + " isCodeValid: " + codeAvailable);


                    if (codeAvailable)
                    {
                        redeemCode(codeToRedeem);
                        return true;
                    }
                    else
                    {
                        _codeIsInvalid = true;
                        return false;
                    }

                }
                //_codeIsInvalid = true;
                return false;

            });

            //_codeIsInvalid = true;
            return false;

        }
        else
        {
            print("the same user");
            _codeIsInvalid = true;
        }

        return false;
    }

    public string getParseUsername()
    {
        if (ParseUser.CurrentUser != null)
        {

            Debug.Log("username:" + ParseUser.CurrentUser.Username);

            return ParseUser.CurrentUser.Username;
        }
        else
        {
            return "noName";
        }
    }

    //create a random code and add it to the database
    public void addCodeToDatabase()
    {
        ParseObject codeObject = new ParseObject("InAppCodes");

        codeObject["strNote"] = "Company generated Code";
        string strCode = generateRandomCode();
        codeObject["strCode"] = strCode;
        codeObject["boolAvailable"] = true;
        codeObject["priceTier"] = 5; //valid for price 4.99$

        codeObject.SaveAsync();

        PromoCodesManager.Instance.DisplayGeneratedPromoCode(strCode);

        Debug.Log("save " + codeObject["strCode"] +
            "in backend");

    }

    public void addCodeToDatabaseWithUserReference()
    {
        ParseObject codeObject = new ParseObject("InAppCodes");

        codeObject["strNote"] = "User generated Code";
        string strCode = generateRandomCode();
        codeObject["strCode"] = strCode;
        codeObject["boolAvailable"] = true;
        codeObject["priceTier"] = 1; //valid for price 0.99$

        codeObject["user"] = ParseUser.CurrentUser; //refference to the user

        codeObject.SaveAsync();

        PromoCodesManager.Instance.DisplayGeneratedPromoCode(strCode);

        Debug.Log("addCodeToDatabaseWithUserReference " + codeObject["strCode"] +
         "in backend");


    }

    void Update()
    {
        if(_promoCodeAvailable)
        {
            PromoCodesManager.Instance.SetStateText(true, _priceTier);
            _promoCodeAvailable = false;
        }

        if(_codeIsInvalid)
        {
            PromoCodesManager.Instance.SetStateText(false, "");
            _codeIsInvalid = false;
        }

        if(fyberStarted)
        {
            fyberStarted = false;
            fyberManager.StartFyber();
        }
    }

#endregion

    #region Sponsored Apps

    bool FourDaysLeft()
    {
        if (PlayerPrefs.HasKey("sponsored_apps_day"))
        {
            DateTime lastLoadDataTime = Convert.ToDateTime(PlayerPrefs.GetString("sponsored_apps_day"));
            TimeSpan diff = DateTime.Today - lastLoadDataTime;

            if (diff.TotalDays >= 4)
            {
                PlayerPrefs.SetString("sponsored_apps_day", DateTime.Now.ToShortDateString());
                return true;
            }
        }
        else
        {
            PlayerPrefs.SetString("sponsored_apps_day", DateTime.Now.ToShortDateString());
            return true;
        }
        return false;
    }

    /// <summary>
    /// Gets the parse ad button infos. 
    /// </summary> //on tag we can define OUR or SPONSORED apps
    public void getParseAdButtonInfos()
    {
        string countryCodeOnDevice;
        if (COUTRY_CODES.TryGetValue(Application.systemLanguage, out countryCodeOnDevice))
        {
        }
        else
        {
            countryCodeOnDevice = "ALL";
        }


        Debug.Log(Application.systemLanguage.ToString() + " shortCode " + countryCodeOnDevice);

#if UNITY_IPHONE
  var query = ParseObject.GetQuery("AdButtonInfo").WhereContains("strLand", countryCodeOnDevice).WhereEqualTo("bitActive", true).WhereContains("strPlatform", "iOS").WhereNotEqualTo("EXCLUDE", Application.bundleIdentifier);
#elif UNITY_ANDROID

        var query = ParseObject.GetQuery("AdButtonInfo").WhereContains("strLand", countryCodeOnDevice).WhereEqualTo("bitActive", true).WhereContains("strPlatform", "Android").WhereNotEqualTo("EXCLUDE", Application.bundleIdentifier);
#else
		var query = ParseObject.GetQuery("AdButtonInfo").WhereContains("strLand", countryCodeOnDevice).WhereContains("bitActive", true).WhereContains("strTAG", searchTAG).WhereContains ("strPlatform", "Desktop"); 	 
#endif

        query.FindAsync().ContinueWith(t =>
        {

            IEnumerable<ParseObject> results = t.Result;

            foreach (var result in results)
            {
                string strAppLink = result.Get<string>("strAppLink");
                string strPhotoLink = result.Get<string>("strPhotoLink");
                int id = result.Get<int>("ID");
                string searchTag = result.Get<string>("strTAG");
                if (searchTag == "Sponsored")
                {
                    //strSponsoredAppLinkLst.Add(strAppLink);
                    //strSponsoredPhotoLinkLst.Add(strPhotoLink);
                }
                else if (searchTag == "OUR")
                {
                    strOurAppLinkLst.Add(strAppLink);
                    strOurPhotoLinkLst.Add(strPhotoLink);
                }

                Debug.Log("ID " + id + " link " + strAppLink + " strPhotoLink: " + strPhotoLink);

            }
        });
        Invoke("SaveSponsoredAppsToFile", 4);
    }

    #region Helper Method
    /// <summary>
    /// Helper to define shortcuts for the SystemLanguages
    /// </summary>
    private static readonly Dictionary<SystemLanguage, string> COUTRY_CODES = new Dictionary<SystemLanguage, string>()
    {
        { SystemLanguage.Afrikaans, "ZA" },
        { SystemLanguage.Arabic    , "SA" },
        { SystemLanguage.Basque    , "US" },
        { SystemLanguage.Belarusian    , "BY" },
        { SystemLanguage.Bulgarian    , "BJ" },
        { SystemLanguage.Catalan    , "ES" },
        { SystemLanguage.Chinese    , "CN" },
        { SystemLanguage.Czech    , "HK" },
        { SystemLanguage.Danish    , "DK" },
        { SystemLanguage.Dutch    , "BE" },
        { SystemLanguage.English    , "US" },
        { SystemLanguage.Estonian    , "EE" },
        { SystemLanguage.Faroese    , "FU" },
        { SystemLanguage.Finnish    , "FI" },
        { SystemLanguage.French    , "FR" },
        { SystemLanguage.German    , "DE" },
        { SystemLanguage.Greek    , "JR" },
        { SystemLanguage.Hebrew    , "IL" },
        { SystemLanguage.Icelandic    , "IS" },
        { SystemLanguage.Indonesian    , "ID" },
        { SystemLanguage.Italian    , "IT" },
        { SystemLanguage.Japanese    , "JP" },
        { SystemLanguage.Korean    , "KR" },
        { SystemLanguage.Latvian    , "LV" },
        { SystemLanguage.Lithuanian    , "LT" },
        { SystemLanguage.Norwegian    , "NO" },
        { SystemLanguage.Polish    , "PL" },
        { SystemLanguage.Portuguese    , "PT" },
        { SystemLanguage.Romanian    , "RO" },
        { SystemLanguage.Russian    , "RU" },
        { SystemLanguage.SerboCroatian    , "SP" },
        { SystemLanguage.Slovak    , "SK" },
        { SystemLanguage.Slovenian    , "SI" },
        { SystemLanguage.Spanish    , "ES" },
        { SystemLanguage.Swedish    , "SE" },
        { SystemLanguage.Thai    , "TH" },
        { SystemLanguage.Turkish    , "TR" },
        { SystemLanguage.Ukrainian    , "UA" },
        { SystemLanguage.Vietnamese    , "VN" },
        { SystemLanguage.ChineseSimplified    , "CN" },
        { SystemLanguage.ChineseTraditional    , "CN" },
        { SystemLanguage.Unknown    , "ALL" },
        { SystemLanguage.Hungarian    , "HU" },
    };





    //dont work! saved for later
    void callCloudCode()
    {

        Debug.Log("start cloud code");


        ParseCloud.CallFunctionAsync<string>("generateReferralCode", new Dictionary<string, object>()).ContinueWith(t =>

            Debug.Log("callCloudCode" + t.Result)

        );

    }

#endregion

    bool isSaving = false;
    void SaveSponsoredAppsToFile()
    {
        if (!isSaving)
        {
            isSaving = true;
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(Application.persistentDataPath + "/sponsoredapps.dat");

            SponsoredApps data = new SponsoredApps();

            //for (int i = 0; i < data.strSponsoredAppLinksArray.Length; i++)
            //{
            //    data.strSponsoredAppLinksArray[i] = "";
            //}
            //for (int i = 0; i < data.strSponsoredAppPhotoLinksArray.Length; i++)
            //{
            //    data.strSponsoredAppPhotoLinksArray[i] = "";
            //}
            //for (int i = 0; i < data.strOurAppLinksArray.Length; i++)
            //{
            //    data.strOurAppLinksArray[i] = "";
            //}
            //for (int i = 0; i < data.strOurAppPhotoLinksArray.Length; i++)
            //{
            //    data.strOurAppPhotoLinksArray[i] = "";
            //}

            //for (int i = 0; i < strSponsoredAppLinkLst.Count; i++)
            //{
            //    data.strSponsoredAppLinksArray[i] = strSponsoredAppLinkLst[i];
            //    data.strSponsoredAppPhotoLinksArray[i] = strSponsoredPhotoLinkLst[i];
            //}
            //for (int i = 0; i < strOurAppLinkLst.Count; i++)
            //{
            //    data.strOurAppLinksArray[i] = strOurAppLinkLst[i];
            //    data.strOurAppPhotoLinksArray[i] = strOurPhotoLinkLst[i];
            //}

            //backendTestLabel.text += " SAVED " + strSponsoredAppLinkLst.Count.ToString() + " SA TO FILE";
            //backendTestLabel.text += " SAVE " + strOurAppLinkLst.Count.ToString() + " OA TO FILE";

            for (int i = 0; i < strOurAppLinkLst.Count; i++)
            {
                data.AddElementToLst(strOurAppLinkLst[i], strOurPhotoLinkLst[i]);
            }

            bf.Serialize(file, data);
            file.Close();
        }
    }

    public void LoadSponsoredAppsFromFile()
    {
        if (File.Exists(Application.persistentDataPath + "/sponsoredapps.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/sponsoredapps.dat", FileMode.Open);
            SponsoredApps data = (SponsoredApps)bf.Deserialize(file);
            file.Close();

            //for (int i = 0; i < data.strSponsoredAppLinksArray.Length; i++)
            //{
            //    strSponsoredAppLinksArray[i] = data.strSponsoredAppLinksArray[i];
            //}
            //for (int i = 0; i < data.strSponsoredAppPhotoLinksArray.Length; i++)
            //{
            //    strSponsoredAppPhotoLinksArray[i] = data.strSponsoredAppPhotoLinksArray[i];
            //}
            //for (int i = 0; i < data.strOurAppLinksArray.Length; i++)
            //{
            //    strOurAppLinksArray[i] = data.strOurAppLinksArray[i];
            //}
            //for (int i = 0; i < data.strOurAppPhotoLinksArray.Length; i++)
            //{
            //    strOurAppPhotoLinksArray[i] = data.strOurAppPhotoLinksArray[i];
            //}

            for (int i = 0; i < data.linksToGamesLst.Count; i++)
            {
                strOurAppLinkLst.Add(data.GetLinkToGame(i));
                strOurPhotoLinkLst.Add(data.GetLinkToPhoto(i));
            }
        }
        else
        {
            print("/sponsoredapps.dat doesn't exist!");
            backendTestLabel.text += "sponsoredapps.dat doesn't exist!";
            getParseAdButtonInfos();
        }
    }

    #endregion

    #region Fyber

    bool fyberStarted = false;

    void GetParseFyberAvilabilityOniOS()
    {
        var query = ParseObject.GetQuery("Config");

        query.FindAsync().ContinueWith(t =>
        {
            IEnumerable<ParseObject> results = t.Result;

            foreach (var result in results)
            {
                string appName = result.Get<string>("APPNAME");
                bool bitShowOfferwall = result.Get<bool>("bitShowOfferwall");
                if(bitShowOfferwall && appName == "CAT NOM NOM")
                {
                    fyberStarted = true;
                }
            }
        });
    }

#endregion
}

[System.Serializable]
public class SponsoredApps
{
    public string[] strSponsoredAppLinksArray = { "", "", "", "", "", "", "", "", "", "" };
    public string[] strSponsoredAppPhotoLinksArray = { "", "", "", "", "", "", "", "", "", "" };

    public string[] strOurAppLinksArray = { "", "", "", "", "", "", "", "", "", "" };
    public string[] strOurAppPhotoLinksArray = { "", "", "", "", "", "", "", "", "", "" };

    public List<string> linksToGamesLst = new List<string>();
    public List<string> linksToPhotoLst = new List<string>();

    public void AddElementToLst(string linkToGame, string linkToPhoto)
    {
        //Debug.Log("element " + linkToGame + " added");
        linksToGamesLst.Add(linkToGame);
        linksToPhotoLst.Add(linkToPhoto);
    }

    public string GetLinkToGame(int index)
    {
        //Debug.Log("get game element " + linksToGamesLst[index]);

        if (index > linksToGamesLst.Count)
        {
            if (index <= strOurAppLinksArray.Length)
                return strOurAppLinksArray[index];
        }
        else
        {
            return linksToGamesLst[index];
        }

        return null;
    }

    public string GetLinkToPhoto(int index)
    {
        //Debug.Log("get photo element " + linksToPhotoLst[index]);

        if (index > linksToPhotoLst.Count)
        {
            if (index <= strOurAppPhotoLinksArray.Length)
                return linksToPhotoLst[index];
        }
        else
        {
            return linksToPhotoLst[index];
        }

        return null;
    }
}