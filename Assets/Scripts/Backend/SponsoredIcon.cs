﻿using UnityEngine;
using System.Collections;

public class SponsoredIcon : MonoBehaviour {

    public UnityEngine.UI.Image iconImg;

    public void OnClick_OpenSponsoredApp()
    {
        SponsoredAppsManager.Instance.OnCLick_OpenOurApp();
    }
}
