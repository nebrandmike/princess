﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SponsoredAppsManager : MonoBehaviour {

    public static SponsoredAppsManager Instance;

    public Animator iconSponsoredAppsAnimator, iconOurAppsAnimator;
    public UnityEngine.UI.Image iconSponsoredImg, iconOurImg;
    public float iconDurationSec = 20; // each 20 seconds icon will be moving from left to right
    public ParseDatabaseManager parseDatabaseManager;
    public ScrollSnapRect shopScrollSnapRect;
    public GameObject sponsoredAppsItemGO;

    public List<string> _strSponsoredAppLinkLst = new List<string>();
    public List<string> _strSponsoredPhotoLinkLst = new List<string>();
    private int _currSponsoredAppIndex;
    private Sprite _iconSponsoredSpr;
    private bool _sponsoredAppsLoaded;

    public List<string> _strOurAppLinkLst = new List<string>();
    public List<string> _strOurPhotoLinkLst = new List<string>();
    private int _currOurAppIndex;
    private Sprite _iconOurSpr;
    private bool _ourAppsLoaded;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
		if(parseDatabaseManager.beckendEnabled)
			StartCoroutine(checkInternetConnection());
    }

    void Load()
    {
        print("load");
        parseDatabaseManager.LoadSponsoredAppsFromFile();
        Invoke("LoadSponsoredAppsFromFile", 7);
    }

    void LoadSponsoredAppsFromFile()
    {
        print("load sponsored apps from file");

        //for (int i = 0; i < parseDatabaseManager.strSponsoredAppLinksArray.Length; i++)
        //{
        //    if (parseDatabaseManager.strSponsoredAppLinksArray[i] != "")
        //    {
        //        _strSponsoredAppLinkLst.Add(parseDatabaseManager.strSponsoredAppLinksArray[i]);
        //        _strSponsoredPhotoLinkLst.Add(parseDatabaseManager.strSponsoredAppPhotoLinksArray[i]);
        //    }
        //}

        parseDatabaseManager.backendTestLabel.text += " \n loaded " + _strSponsoredAppLinkLst.Count.ToString() + " sponsored apps";


        if (_strSponsoredAppLinkLst.Count == 0)
        {
           // Invoke("Load", 2f);
            //Invoke("LoadSponsoredAppsFromFile", 6);
            Invoke("LoadOurAppsFromFile", 8);
            InvokeRepeating("DisplayIconOnGameScreen", 10, iconDurationSec);
        }
    }

    void LoadOurAppsFromFile()
    {
        print("load our apps from file");

        //for (int i = 0; i < parseDatabaseManager.strOurAppLinksArray.Length; i++)
        //{
        //    if (parseDatabaseManager.strOurAppLinksArray[i] != "")
        //    {
        //        _strOurAppLinkLst.Add(parseDatabaseManager.strOurAppLinksArray[i]);
        //        _strOurPhotoLinkLst.Add(parseDatabaseManager.strOurAppPhotoLinksArray[i]);
        //    }
        //}

        for (int i = 0; i < parseDatabaseManager.strOurAppLinkLst.Count; i++)
        {
            if (parseDatabaseManager.strOurAppLinkLst[i] != "")
            {
                _strOurAppLinkLst.Add(parseDatabaseManager.strOurAppLinkLst[i]);
                _strOurPhotoLinkLst.Add(parseDatabaseManager.strOurPhotoLinkLst[i]);
            }
        }

        if (_strOurPhotoLinkLst.Count > 0)
        {
            //if (!_iconDisplaying && ShopMinions.Instance.lastMinionIndex >= 5)
            //{
            //    _iconDisplaying = true;
                InvokeRepeating("DisplayIconOnGameScreen", 5, iconDurationSec);
            // }
        }
        else
            Invoke("LoadOurAppsFromFile", 5);

        //parseDatabaseManager.backendTestLabel.text += " \n loaded " + _strOurAppLinkLst.Count.ToString() + " our apps";
    }

    void DisplayIconOnGameScreen()
    {
        print("DisplayIconOnGameScreen");
        if (_strSponsoredAppLinkLst.Count > 0)
        {
            if (!Tutorial.Instance.activated)
            {
                iconSponsoredAppsAnimator.gameObject.SetActive(true);
                _currSponsoredAppIndex = UnityEngine.Random.Range(0, _strSponsoredAppLinkLst.Count);
                StartCoroutine(DownloadSponsoredImgFromURL(_strSponsoredPhotoLinkLst[_currSponsoredAppIndex]));
                iconSponsoredAppsAnimator.SetTrigger("Move");
                _sponsoredAppsLoaded = true;
            }
        }
        //else LoadSponsoredAppsFromFile();

        if (_strOurAppLinkLst.Count > 0)
        {
            if (!Tutorial.Instance.activated)
            {
                iconOurAppsAnimator.gameObject.SetActive(true);
                _currOurAppIndex = UnityEngine.Random.Range(0, _strOurAppLinkLst.Count);
                StartCoroutine(DownloadOurImgFromURL(_strOurPhotoLinkLst[_currOurAppIndex]));
                iconOurAppsAnimator.SetTrigger("Move");
                _ourAppsLoaded = true;
            }
        }
        //else LoadOurAppsFromFile();
    }

    IEnumerator DownloadSponsoredImgFromURL(string url)
    {
        Texture2D texture = new Texture2D(1, 1);
        WWW www = new WWW(url);
        yield return www;
        www.LoadImageIntoTexture(texture);

        _iconSponsoredSpr = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
        iconSponsoredImg.sprite = _iconSponsoredSpr;
    }

    IEnumerator DownloadOurImgFromURL(string url)
    {
        Texture2D texture = new Texture2D(1, 1);
        WWW www = new WWW(url);
        yield return www;
        www.LoadImageIntoTexture(texture);

        _iconOurSpr = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
        iconOurImg.sprite = _iconOurSpr;
    }

    public void OnCLick_OpenSponsoredApp()
    {
        if (_strSponsoredAppLinkLst[_currSponsoredAppIndex] != "")
        {
            Application.OpenURL(_strSponsoredAppLinkLst[_currSponsoredAppIndex]);
            iconSponsoredAppsAnimator.gameObject.SetActive(false);
            AnalyticsManager.Instance.UserClickLeftBallon();
        }
    }

    public void OnCLick_OpenOurApp()
    {
        if (_strOurAppLinkLst[_currOurAppIndex] != "")
        {
            Application.OpenURL(_strOurAppLinkLst[_currOurAppIndex]);
            iconOurAppsAnimator.gameObject.SetActive(false);
            AnalyticsManager.Instance.UserClickRightBallon();
        }
    }

    public void SetSponsoredAppImgToTheShop()
    {
        if (UpgradesShopUI.Instance.Is10MinionsHouseLimitUnlocked() && _ourAppsLoaded)
        {
            sponsoredAppsItemGO.SetActive(true);
            sponsoredAppsItemGO.GetComponent<SponsoredIcon>().iconImg.sprite = _iconOurSpr;
            shopScrollSnapRect.startingPage = 1;
        }
        else
        {
            shopScrollSnapRect.startingPage = 2;
        }
    }

    bool isAppsLoaded = false;
    IEnumerator checkInternetConnection()
    {
        print("cic");
        WWW www = new WWW("http://google.com");
        yield return www;
        if (!isAppsLoaded)
        {
            isAppsLoaded = true;
            if (www.isDone && www.bytesDownloaded > 0)
            {
                parseDatabaseManager.backendTestLabel.text += " internet connection: true";
                Invoke("Load", 10f);
                //Invoke("LoadSponsoredAppsFromFile", 15);
                //Invoke("LoadOurAppsFromFile", 15);
                //InvokeRepeating("DisplayIconOnGameScreen", 18, iconDurationSec);
            }
            if (www.isDone && www.bytesDownloaded == 0)
            {
                parseDatabaseManager.backendTestLabel.text += " internet connection: false";
            }
        }
    }
}
