﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PromoCodesManager : MonoBehaviour
{
    public static PromoCodesManager Instance;

    public ParseDatabaseManager parseDatabaseManager;
    public GameObject part1GO, part2GO, part3GO;
    public Text generatedCodeLabel;
    public NativeShare nativeShare;
    public Text inputPromoCodeLabel;
    public Text statusLabel;
    public Text rewardLabel;

    private string _cachedPromoCode;

    void Awake()
    {
        Instance = this;
    }

    public void OnClick_OpenPromoCodePanel()
    {
        part1GO.SetActive(true);
        part2GO.SetActive(false);
        part3GO.SetActive(false);
    }

    public void OnClick_GeneratePromoCode()
    {
        part1GO.SetActive(false);
        part2GO.SetActive(true);
        part3GO.SetActive(false);

        parseDatabaseManager.GeneratePromoCodeClicked();
    }

    public void DisplayGeneratedPromoCode(string promocode)
    {
        _cachedPromoCode = promocode;
        generatedCodeLabel.text = _cachedPromoCode;
    }

    public void OnClick_SendPromoCodeToFriend()
    {
        nativeShare.ShareScreenshotWithText(_cachedPromoCode);
    }

    public void OnClick_PromoCode_OK()
    {
        print("OnClick_PromoCode_OK");
        parseDatabaseManager.Init();

        part3GO.SetActive(true);
        part1GO.SetActive(false);
        part2GO.SetActive(false);

        //parseDatabaseManager.isCodeValid(inputPromoCodeLabel.text);
        parseDatabaseManager.isCodeValidWithUser(inputPromoCodeLabel.text);
    }

    bool processingDone = true;
    public void SetStateText(bool success, string priceTier)
    {
        print("SetStateText" + success);
        statusLabel.text = success ? "Congratilations! This code valid!" : "Code invalid";
        if(success)
        {
            rewardLabel.gameObject.SetActive(true);
            rewardLabel.text = priceTier;
            GameManager.Instance.Gems += System.Convert.ToInt32(priceTier);
        }
        else
        {
            rewardLabel.gameObject.SetActive(false);
        }
    }  
}
