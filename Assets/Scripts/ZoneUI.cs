﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// System.Serialazable allow us create nice lists in inspector. It's useful :-)
/// Each zone has several objects, and all them stored in one list in script UIManager
/// </summary>
[System.Serializable]
public class ZoneUI
{
    public GameObject zoneButtonGO;
    public Image[] zoneBtnImages;
    public Text zonePriceText;
    public GameObject sendMinionsBtnGO, addMinionBtnGO;
    public Image[] sendMinionsImg;
    public Text sendMinionText;
    public Image addMinionBtnImg, addMinionFoodImg;  
    public Text addMinionText;
}
