﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using DG.Tweening;

public enum CardFunctions
{
    // Good
    DoubleFoodValue,
    PlusGems5,
    ReduceHouseTime50,
    CurrFood25Extra,
    // Bad
    CurrFoodMinus25,
    DoubleHouseTimings,
    DoubleSlowMinions,
    ReduceFoodValueMinus50
}

[System.Serializable]
public class Card
{
    public enum Type { Good, Bad }
    public Type type;
    public CardFunctions function;
    public string name;
    public string description;
    public int duration = 60;
    public Sprite sprite;
    public bool doubleGemsAvailable;
    public int doubleGemsPrice = 1; // gems for double reward
    public GameObject resultPanelIconGO;
}

public class CardsManager : MonoBehaviour
{
    public static CardsManager Instance;

    public List<Card> goodCards, badCards;

    [Space]
    public int timerMin;
    public int timerSec;
    [Space]
    public Text timerLabel;
    public Image[] cardsIconImg;
    public Animator cardsIconAnim;
    public GameObject gamePanelGO;
    public GameObject backgroundGO;
    public Image[] cardsImg;
    public GameObject cardsResultPanelGO;
    public Text nameLabel, descriptionLabel, durationLabel, doubleGemsRewardValueLabel;
    public GameObject doubleGemsUIStuff;
    public Sprite backCardsSpr;
    public Text doubleLabel;
    public GameObject cardsDurationIconGO;
    public GameObject cardsIndicatorGO;
    public Image[] cardsDurationImg;
    public Animator gemsBtnAnimator;
    public Animator videoAnimator, gemsVideoAnimator;
    public GameObject videoBtnGO, gemsBtnGO;

    private static TimeSpan interval = new TimeSpan(0, 15, 0); // 15 minutes by default
    private DateTime neededTimeToCardsAvailable;
    private DateTime gamePlayedTimeStamp;
    private bool _gameAvailable;
    private List<Card> _randomCards = new List<Card>();
    private bool _canTapCard;
    private int _currCardIndex;
    private bool userCanBeInterrapted = true;

    void Awake()
    {
        Instance = this;
        interval = new TimeSpan(0, timerMin, timerSec);
    }

    void Start()
    {
        if (PlayerPrefs.HasKey("SavedTime"))
        {
            long temp = Convert.ToInt64(PlayerPrefs.GetString("gamePlayedTimeStamp"));
            gamePlayedTimeStamp = DateTime.FromBinary(temp);
            long temp2 = Convert.ToInt64(PlayerPrefs.GetString("neededTimeToCardsAvailable"));
            neededTimeToCardsAvailable = DateTime.FromBinary(temp2);
        }
        else
        {
            PlayerPrefs.SetInt("SavedTime", 1);
            gamePlayedTimeStamp = DateTime.Now;
            neededTimeToCardsAvailable = DateTime.Now + interval;
            SaveTimer();
        }

        userCanBeInterrapted = PlayerPrefs.GetInt("userCanBeInterrapted") == 0 ? true : false;
    }

    float timer = 0;
    void Update()
    {
        if (!_gameAvailable)
        {
            TimeSpan t = DateTime.Now - gamePlayedTimeStamp;
            TimeSpan t2 = neededTimeToCardsAvailable - DateTime.Now;
            if (t2.TotalSeconds > 0)
            {
                timerLabel.text = ToReadableString(t2);
            }
            else
            {
                GameAvailable();
                timerLabel.text = "";
            }
        }

        if(userCanBeInterrapted && !Tutorial.Instance.activated)
        {
            timer += Time.deltaTime;
            if(timer > 300) // 300 seconds it's 5 minutes
            {
                userCanBeInterrapted = false;
                PlayerPrefs.SetInt("userCanBeInterrapted", 1);
                UIManager.Instance.CloseAllPanels();
                OnClick_OpenCardsGamePanel();
            }
        }

#if UNITY_EDITOR
        // testing cards functions
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            _canTapCard = true;
            _randomCards = new List<Card>();
            _randomCards.Add(goodCards[0]);
            OnClick_Card(0);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            _canTapCard = true;
            _randomCards = new List<Card>();
            _randomCards.Add(goodCards[1]);
            OnClick_Card(0);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            _canTapCard = true;
            _randomCards = new List<Card>();
            _randomCards.Add(goodCards[2]);
            OnClick_Card(0);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            _canTapCard = true;
            _randomCards = new List<Card>();
            _randomCards.Add(goodCards[3]);
            OnClick_Card(0);
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            _canTapCard = true;
            _randomCards = new List<Card>();
            _randomCards.Add(badCards[0]);
            OnClick_Card(0);
        }
        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            _canTapCard = true;
            _randomCards = new List<Card>();
            _randomCards.Add(badCards[1]);
            OnClick_Card(0);
        }
        if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            //_canTapCard = true;
            //_randomCards = new List<Card>();
            //_randomCards.Add(badCards[2]);
            //OnClick_Card(0);
            DoubleSlowMinions(false);
        }
        if (Input.GetKeyDown(KeyCode.Alpha8))
        {
            _canTapCard = true;
            _randomCards = new List<Card>();
            _randomCards.Add(badCards[3]);
            OnClick_Card(0);
        }
#endif
    }

    public string ToReadableString(TimeSpan span)
    {
        string formatted = string.Format("{0}{1}{2}",
            span.Duration().Hours > 0 ? string.Format("{0:0}{1}:", span.Hours, span.Hours == 1 ? String.Empty : "") : string.Empty,
            span.Duration().Minutes > 0 ? string.Format("{0:0}{1}:", span.Minutes, span.Minutes == 1 ? String.Empty : "") : string.Empty,
            span.Duration().Seconds > 0 ? string.Format("{0:0}{1}", span.Seconds, span.Seconds == 1 ? String.Empty : "") : string.Empty);

        if (formatted.EndsWith(",")) formatted = formatted.Substring(0, formatted.Length - 2);

        if (string.IsNullOrEmpty(formatted)) formatted = "0";

        return formatted;
    }

    void GameAvailable()
    {
        _gameAvailable = true;

        for (int i = 0; i < cardsIconImg.Length; i++)
        {
            cardsIconImg[i].DOFade(1, 0);
        }

        cardsIconAnim.SetTrigger("Available");
    }

    public void OnClick_OpenCardsGamePanel()
    {
        if(userCanBeInterrapted)
        {
            userCanBeInterrapted = false;
            PlayerPrefs.SetInt("userCanBeInterrapted", 1);
        }

        gamePanelGO.SetActive(true);
        gamePanelGO.GetComponent<Animator>().SetTrigger("Appear");
        backgroundGO.SetActive(true);
        UIManager.Instance.OnPanelOpen(true);
        for (int i = 0; i < cardsImg.Length; i++)
        {
            cardsImg[i].sprite = backCardsSpr;
        }
        SetRndCards();
    }

    public void OnClick_CloseCardsGamePanel()
    {
        backgroundGO.SetActive(false);
        UIManager.Instance.OnPanelOpen(false);
    }

    public void OnClick_PlayGame()
    {
        if (_gameAvailable)
        {
            _gameAvailable = false;
            for (int i = 0; i < cardsIconImg.Length; i++)
            {
                cardsIconImg[i].DOFade(0.5f, 0);
            }
            gamePanelGO.SetActive(true);
            doubleGemsUIStuff.SetActive(true);
            for (int i = 0; i < cardsImg.Length; i++)
            {
                cardsImg[i].sprite = backCardsSpr;
            }
            SoundManager.Instance.PlaySound(Sounds.Panel);
            _canTapCard = true;
            cardsIconAnim.SetTrigger("NotAvailable");
            gamePlayedTimeStamp = DateTime.Now;
            neededTimeToCardsAvailable = DateTime.Now + interval;
            PlayerPrefs.SetString("gamePlayedTimeStamp", gamePlayedTimeStamp.ToBinary().ToString());
            PlayerPrefs.SetString("neededTimeToCardsAvailable", neededTimeToCardsAvailable.ToBinary().ToString());
        }
    }

    void OnApplicationExit()
    {
        print("on exit");
        SaveTimer();
    }

    void SaveTimer()
    {
        PlayerPrefs.SetString("gamePlayedTimeStamp", gamePlayedTimeStamp.ToBinary().ToString());
        PlayerPrefs.SetString("neededTimeToCardsAvailable", neededTimeToCardsAvailable.ToBinary().ToString());
    }

    void SetRndCards()
    {
        Utilities.Shuffle(goodCards);
        Utilities.Shuffle(badCards);
        _randomCards = new List<Card>();
        for (int i = 0; i < 2; i++)
        {
            _randomCards.Add(goodCards[i]);
        }
        for (int i = 0; i < 1; i++)
        {
            _randomCards.Add(badCards[i]);
        }
        Utilities.Shuffle(_randomCards);
        for (int i = 0; i < _randomCards.Count; i++)
        {
            AchievementsManager.Instance.AddCardToLst(_randomCards[i].name);
        }
    }

    public void OnClick_Card(int index)
    {
        OnClick_PlayGame();
        if (_canTapCard)
        {
            _canTapCard = false;
            _currCardIndex = index;
            StartCoroutine(CardClicked(index));
        }
    }

    IEnumerator CardClicked(int index)
    {
        yield return new WaitForSeconds(0.25f);
        int secondCardInd = 0, thirdCardInd = 0;
        switch (index)
        {
            case 0: secondCardInd = 1; thirdCardInd = 2; break;
            case 1: secondCardInd = 0; thirdCardInd = 2; break;
            case 2: secondCardInd = 0; thirdCardInd = 1; break;
        }

        SoundManager.Instance.PlaySound(Sounds.CardClicked);
        cardsImg[index].gameObject.GetComponent<Animator>().SetTrigger("Flip");
        yield return new WaitForSeconds(0.2f);
        cardsImg[index].sprite = _randomCards[index].sprite;
        yield return new WaitForSeconds(0.5f);
        cardsImg[index].gameObject.GetComponent<Animator>().SetTrigger("Click");

        if (_randomCards[index].type == Card.Type.Good)
            SoundManager.Instance.PlaySound(Sounds.GoodCard);
        else
            SoundManager.Instance.PlaySound(Sounds.BadCard);

        yield return new WaitForSeconds(0.8f);

        cardsImg[secondCardInd].gameObject.GetComponent<Animator>().SetTrigger("Flip");
        cardsImg[thirdCardInd].gameObject.GetComponent<Animator>().SetTrigger("Flip");
        yield return new WaitForSeconds(0.2f);
        cardsImg[secondCardInd].sprite = _randomCards[secondCardInd].sprite;
        cardsImg[thirdCardInd].sprite = _randomCards[thirdCardInd].sprite;
        yield return new WaitForSeconds(1.2f);

        gamePanelGO.SetActive(false);
        cardsResultPanelGO.SetActive(true);
        

        if (_randomCards[_currCardIndex].function == CardFunctions.PlusGems5) // if plus 1 gem activated
        {
            videoBtnGO.SetActive(false);
            gemsBtnGO.SetActive(false);
            gemsVideoAnimator.gameObject.SetActive(true);
            gemsVideoAnimator.SetTrigger("Start");
        }
        else
        {
            videoBtnGO.SetActive(true);
            gemsBtnGO.SetActive(true);
        }

        DisableCardItems();
        SoundManager.Instance.PlaySound(Sounds.Panel);
        cardsResultPanelGO.GetComponent<Animator>().SetTrigger("Appear");

        nameLabel.text = _randomCards[_currCardIndex].name;
        descriptionLabel.text = _randomCards[_currCardIndex].description;
        _randomCards[_currCardIndex].resultPanelIconGO.SetActive(true);

        if (_randomCards[_currCardIndex].type == Card.Type.Good)
        {
            doubleLabel.text = "Double it now!";
            CatBubble.Instance.DisplayCatBubble("Let's double this Awesome Effect!", true);
        }
        else
        {
            doubleLabel.text = "Skip!";
            CatBubble.Instance.DisplayCatBubble("lets skip this Bad Effect!", true);
        }

        if (_randomCards[index].duration > 0)
            durationLabel.text = _randomCards[index].duration.ToString() + " seconds";
        else
            durationLabel.text = "";

        if (_randomCards[index].doubleGemsAvailable)
            doubleGemsRewardValueLabel.text = _randomCards[index].doubleGemsPrice.ToString();
        else
        {
            doubleGemsUIStuff.SetActive(false);
        }

        if (_randomCards[index].type == Card.Type.Good)
            AchievementsManager.Instance.PlusLuckCards();
        else
            AchievementsManager.Instance.ReserLuckCardsCounter();

        yield return new WaitForSeconds(0.5f);
        videoBtnGO.GetComponent<Animator>().SetTrigger("Start");
    }

    public void OnClick_CloseResultPanel()
    {
        _randomCards[_currCardIndex].resultPanelIconGO.SetActive(false);
        DisableCardItems();
        CheckCardFunction(false);
        cardsResultPanelGO.GetComponent<Animator>().SetTrigger("Dissapear");
        backgroundGO.SetActive(false);
        UIManager.Instance.OnPanelOpen(false);
        SoundManager.Instance.PlaySound(Sounds.ClosePanel);
        Invoke("HideResultPanel", 0.5f);
    }

    void HideResultPanel()
    {
        cardsResultPanelGO.SetActive(false);
    }

    public void OnClick_WatchDoubleRewardVideo()
    {
        if (AdsManager.Instance.IsVideoAvailable())
        {
            AdsManager.Instance.WatchDoubleCardRewardVideo();
            backgroundGO.SetActive(false);
            cardsResultPanelGO.SetActive(false);
            UIManager.Instance.OnPanelOpen(false);
            videoAnimator.SetTrigger("Pressed");
            cardsResultPanelGO.GetComponent<Animator>().SetTrigger("Dissapear");
            SoundManager.Instance.PlaySound(Sounds.Click);
            CatBubble.Instance.CloseCatBubble();
        }
        else
        {
            videoAnimator.SetTrigger("Wiggle2");
            SoundManager.Instance.PlaySound(Sounds.NotEnough);
        }
    }

    public void DoubleRewardVideoWatched()
    {
        AnalyticsManager.Instance.UserWatchVideoSkipOrDoubleCard();
        CheckCardFunction(true);
    }

    void CheckCardFunction(bool isDouble)
    {
        switch (_randomCards[_currCardIndex].function)
        {
            case CardFunctions.DoubleFoodValue:
                DisplayCardsDuration();
                DoubleFoodValue(isDouble);
                break;
            case CardFunctions.PlusGems5:
                DisplayCardsDuration();
                PlusGems1(isDouble);
                break;
            case CardFunctions.ReduceHouseTime50:
                DisplayCardsDuration();
                ReduceHouseTime50(isDouble);
                break;
            case CardFunctions.CurrFood25Extra:
                DisplayCardsDuration();
                Plus25CurrFoodAsExtraFood(isDouble);
                break;
            case CardFunctions.CurrFoodMinus25:
                CurrFoodMinus25(isDouble);
                break;
            case CardFunctions.DoubleHouseTimings:
                if (!isDouble) DisplayCardsDuration();
                DoubleHouseTimings(isDouble);
                break;
            case CardFunctions.DoubleSlowMinions:
                if (!isDouble) DisplayCardsDuration();
                DoubleSlowMinions(isDouble);
                break;
            case CardFunctions.ReduceFoodValueMinus50:
                if (!isDouble) DisplayCardsDuration();
                ReduceFoodValueMinus50(isDouble);
                break;
        }  
    }

    void DisplayCardsDuration()
    {
        cardsDurationIconGO.SetActive(true);
        cardsIndicatorGO.SetActive(false);
        cardsDurationImg[0].sprite = _randomCards[_currCardIndex].sprite;
        cardsDurationImg[1].sprite = _randomCards[_currCardIndex].sprite;
        cardsDurationImg[0].DOFillAmount(0, _randomCards[_currCardIndex].duration);
        Invoke("ResetCardDurationIcon", _randomCards[_currCardIndex].duration);
    }

    void ResetCardDurationIcon()
    {
        cardsDurationImg[0].DOFillAmount(1, 0);
        cardsDurationIconGO.SetActive(false);
        cardsIndicatorGO.SetActive(true);
    }

    public void OnClick_DoubleGemsReward()
    {
        if (GameManager.Instance.Gems >= _randomCards[_currCardIndex].doubleGemsPrice)
        {
            GameManager.Instance.Gems -= _randomCards[_currCardIndex].doubleGemsPrice;
            CheckCardFunction(true);
            gemsBtnAnimator.SetTrigger("Pressed");
            SoundManager.Instance.PlaySound(Sounds.Click);
            cardsResultPanelGO.GetComponent<Animator>().SetTrigger("Dissapear");
            backgroundGO.SetActive(false);
            UIManager.Instance.OnPanelOpen(false);
            CatBubble.Instance.CloseCatBubble();
        }
        else
        {
            gemsBtnAnimator.SetTrigger("NotEnough");
            SoundManager.Instance.PlaySound(Sounds.NotEnough);
        }
    }

    #region Good Card Functions

    void DoubleFoodValue(bool isDouble)
    {
        print("double food value" + isDouble);
        for (int i = 0; i < GameManager.Instance.zones.Count; i++)
        {
            GameManager.Instance.zones[i].CardResultDoubleFoodValue(isDouble, _randomCards[_currCardIndex].duration);
        }
    }

    void PlusGems1(bool isDouble)
    {
        print("+ 1 gem" + isDouble);
        if (isDouble)
        {
            GameManager.Instance.Gems += 4;
            AchievementsManager.Instance.SendAchievement(AchievementName.GemsMagnat, 4, AchievementType.Countable);
        }
        else
        {
            GameManager.Instance.Gems += 2;
            AchievementsManager.Instance.SendAchievement(AchievementName.GemsMagnat, 2, AchievementType.Countable);
        }
    }

    void ReduceHouseTime50(bool isDouble)
    {
        print("reduce house time 50%" + isDouble);
        for (int i = 0; i < GameManager.Instance.zones.Count; i++)
        {
            GameManager.Instance.zones[i].CardResultReduceHouseTime50(isDouble, _randomCards[_currCardIndex].duration);
        }
    }

    void Plus25CurrFoodAsExtraFood(bool isDouble)
    {
        print("plus 25 current food as extra food" + isDouble);
        for (int i = 0; i < GameManager.Instance.zones.Count; i++)
        {
            GameManager.Instance.zones[i].CardResultPlus25CurrFoodAsExtraFood(isDouble, _randomCards[_currCardIndex].duration);
        }
    }

    #endregion

    #region Bad Cards Functions

    void CurrFoodMinus25(bool isDouble)
    {
        print("current food minus 25%" + isDouble);
        if (!isDouble)
        {
            GameManager.Instance.Food -= GameManager.Instance.Food / 4;
        }
    }

    void DoubleHouseTimings(bool isDouble)
    {
        print("double house timings" + isDouble);
        if (!isDouble)
        {
            for (int i = 0; i < GameManager.Instance.zones.Count; i++)
            {
                GameManager.Instance.zones[i].CardResultDoubleHouseTimings(_randomCards[_currCardIndex].duration);
            }
        }
    }

    [HideInInspector]
    public bool doubleSlowMinionsActivated;
    void DoubleSlowMinions(bool isDouble)
    {
        print("double slow minions" + isDouble);
        if (!isDouble)
        {
            doubleSlowMinionsActivated = true;
            for (int i = 0; i < MinionsManager.Instance.minions.Count; i++)
            {
                MinionsManager.Instance.minions[i].CardResultSlowMinionsSpeedStarted();
            }
            Invoke("DisableDoubleSlowMinions", _randomCards[_currCardIndex].duration);
        }
    }

    void DisableDoubleSlowMinions()
    {
        doubleSlowMinionsActivated = false;
        for (int i = 0; i < MinionsManager.Instance.minions.Count; i++)
        {
            MinionsManager.Instance.minions[i].CardResultSlowMinionsSpeedEnded();
        }
    }

    void ReduceFoodValueMinus50(bool isDouble)
    {
        print("reduce food value minus 50%" + isDouble);
        if (!isDouble)
        {
            for (int i = 0; i < GameManager.Instance.zones.Count; i++)
            {
                GameManager.Instance.zones[i].CardResultReduceFoodValueMinus50(_randomCards[_currCardIndex].duration);
            }
        }
    }

    #endregion

    void DisableCardItems()
    {
        for (int i = 0; i < goodCards.Count; i++)
        {
            goodCards[i].resultPanelIconGO.SetActive(false);
        }
        for (int i = 0; i < badCards.Count; i++)
        {
            badCards[i].resultPanelIconGO.SetActive(false);
        }
    }
}