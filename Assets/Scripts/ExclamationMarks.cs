﻿using UnityEngine;
using System.Collections;

public class ExclamationMarks : MonoBehaviour
{
    public static ExclamationMarks Instance;

    public GameObject[] upgradesExcMarksGO, epicExcMarksGO, shopExcMarksGO;

    private bool _upgradesShowWasOpened, _epicWasOpened, _shopWasOpened;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        if (PlayerPrefs.GetInt("UpgradesWasOpened") == 1)
            _upgradesShowWasOpened = true;

        if (PlayerPrefs.GetInt("EpicWasOpened") == 1)
            _epicWasOpened = true;

        if (PlayerPrefs.GetInt("ShopWasOpened") == 1)
            _shopWasOpened = true;
    }

    public void OnClick_OpenUpgrades()
    {
        if(!_upgradesShowWasOpened)
        {
            PlayerPrefs.SetInt("UpgradesWasOpened", 1);
        }

        _upgradesShowWasOpened = true;

        CheckWasOpened();
    }

    public void OnClick_OpenEpic()
    {
        if (!_epicWasOpened)
        {
            PlayerPrefs.SetInt("EpicWasOpened", 1);
        }

        _epicWasOpened = true;

        CheckWasOpened();
    }

    public void OnClick_OpenShop()
    {
        if (!_shopWasOpened)
        {
            PlayerPrefs.SetInt("ShopWasOpened", 1);
        }

        _shopWasOpened = true;

        CheckWasOpened();
    }

    void CheckWasOpened()
    {
        CheckUpgrades();
        CheckEpic();
        CheckShop();
    }

    void CheckUpgrades()
    {
        if(_upgradesShowWasOpened)
        {
            for (int i = 0; i < upgradesExcMarksGO.Length; i++)
            {
                upgradesExcMarksGO[i].SetActive(false);
            }
        }
    }

    void CheckEpic()
    {
        if(_epicWasOpened)
        {
            for (int i = 0; i < epicExcMarksGO.Length; i++)
            {
                epicExcMarksGO[i].SetActive(false);
            }
        }
    }

    void CheckShop()
    {
        if (_shopWasOpened)
        {
            for (int i = 0; i < shopExcMarksGO.Length; i++)
            {
                shopExcMarksGO[i].SetActive(false);
            }
        }
    }
}
