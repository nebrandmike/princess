﻿using UnityEngine;
using System.Collections;

public class Shop : MonoBehaviour {

    public static Shop Instance;
    public GameObject gemsShopUIGO, upgradeShopUI;
    public LikeOnFacebook likeOnFacebook;
    public GameObject restoreBtnGO;
    //public ScrollSnapRect scrollSnapRect;

    void Awake()
    {
        Instance = this;
    }

    public void OnClick_OpenGemsShop(bool open)
    {
        UIManager.Instance.OnPanelOpen(open);
        if (open)
        {
            UpgradesShopUI.Instance.OnClick_CloseUpgradesPanel();
#if UNITY_ANDROID
            restoreBtnGO.SetActive(false);
#endif
        }

        if(open)
            upgradeShopUI.SetActive(false);

        if (open)
        {
            gemsShopUIGO.SetActive(true);
            likeOnFacebook.DisplayBtnInShop();
        }
        else
        {
            gemsShopUIGO.SetActive(false);
        }
        if(open)
            SoundManager.Instance.PlaySound(Sounds.Panel);
        else
            SoundManager.Instance.PlaySound(Sounds.ClosePanel);

        Trade.Instance.OnGemsShopOpen();
    }

    void HideGemsShopUIGO()
    {
        gemsShopUIGO.SetActive(false);
        UIManager.Instance.OnPanelOpen(false);
    }

    public bool IsAnyShopOpened()
    {
        return (gemsShopUIGO.activeSelf == true);
    }
}
