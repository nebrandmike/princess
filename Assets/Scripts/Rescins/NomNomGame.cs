﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NomNomGame {

    public NomNomType type;
    [Header("Build Settings")]
    public string productName;
    public string bundleID;
    public string versionAndroid;
    public int bundleVersionCodeAndroid;
    public string versionIOS;
    public string bundleVersionCodeIOS;
    public Sprite icon;

    [Header("Chartboost")]
    public string appID;
    public string appSignature;

    [Header("Music")]
    public AudioClip musicFirstLoop;
    public AudioClip musicLoopPart;

    [Header("Minions")]
    public GameObject[] minionsPrefabs;

    [Header("Character")]
    public Sprite characterSpr;
    public RuntimeAnimatorController charAnimatorController;

    [Header("Muscellations")]
    public string shareLinkToStore;
    public string appleIdForRating;
    public Sprite foodSpr;
    public Sprite queenFoodSpr;
    public Sprite blueBtnFoodSpr;
    public Sprite greenBtnFoodSpr;
    public string tutLabel = "tap on cat to born minion";
    public Sprite backroundSpr;
    public string googlePlayPublicKey;

    [Header("Houses")]
    public Sprite[] houses1Spr;
    
}
