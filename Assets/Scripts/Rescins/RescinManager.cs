﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ChartboostSDK;

public enum NomNomType
{
    Cat,
    Dog,
    Cow
}

public class RescinManager : MonoBehaviour {

    public List<NomNomGame> nomNomGamesList;

    [Space]
    public Image characterBubbleImg;
    public NativeShare nativeShare;
    public RateUs rateUs;
    public SpriteRenderer characterSR;
    public Animator charAnimator;
    public Princess princess;
    public SpriteRenderer unlockHouse1SR;
    public SpriteRenderer[] houses1SR;
    public Image princessFoodImg;
    public Image[] blueFoodImages, greenFoodImages;
    public Image tradeFoodImg1, tradeFoodImg2;
    public Purchaser purchaser;
    public Image foodUIImg;
    public SpriteRenderer fishSR, queenFishSR;
    public Image foodUpgradeImg;
    public Text tutLabel;
    public SpriteRenderer backroundSR;

    [Space]
    public NomNomType currentNomNomGame;

    public void ApplyCurrentRescin()
    {
        characterBubbleImg.sprite = nomNomGamesList[(int)currentNomNomGame].characterSpr;
        nativeShare.linkToWebsite = nomNomGamesList[(int)currentNomNomGame].shareLinkToStore;
        rateUs.myAppleIDForRating = nomNomGamesList[(int)currentNomNomGame].appleIdForRating;
        charAnimator.runtimeAnimatorController = nomNomGamesList[(int)currentNomNomGame].charAnimatorController;
        characterSR.sprite = nomNomGamesList[(int)currentNomNomGame].characterSpr;
        for (int i = 0; i < princess.minionPrefabs.Length; i++)
            princess.minionPrefabs[i] = nomNomGamesList[(int)currentNomNomGame].minionsPrefabs[i];

        unlockHouse1SR.sprite = nomNomGamesList[(int)currentNomNomGame].houses1Spr[0];
        for (int i = 0; i < houses1SR.Length; i++)
        {
            houses1SR[i].sprite = nomNomGamesList[(int)currentNomNomGame].houses1Spr[i];
        }

        princessFoodImg.sprite = nomNomGamesList[(int)currentNomNomGame].foodSpr;

        for (int i = 0; i < blueFoodImages.Length; i++)
        {
            blueFoodImages[i].sprite = nomNomGamesList[(int)currentNomNomGame].blueBtnFoodSpr;
        }

        for (int i = 0; i < greenFoodImages.Length; i++)
        {
            greenFoodImages[i].sprite = nomNomGamesList[(int)currentNomNomGame].greenBtnFoodSpr;
        }

        tradeFoodImg1.sprite = nomNomGamesList[(int)currentNomNomGame].foodSpr;
        tradeFoodImg2.sprite = nomNomGamesList[(int)currentNomNomGame].foodSpr;

        purchaser.kProductIDPack1 = nomNomGamesList[(int)currentNomNomGame].bundleID + ".coins.50";
        purchaser.kProductIDPack2 = nomNomGamesList[(int)currentNomNomGame].bundleID + ".130";
        purchaser.kProductIDPack3 = nomNomGamesList[(int)currentNomNomGame].bundleID + ".280";
        purchaser.kProductIDPack4 = nomNomGamesList[(int)currentNomNomGame].bundleID + ".600";
        purchaser.kProductIDPack5 = nomNomGamesList[(int)currentNomNomGame].bundleID + ".1400";

        foodUIImg.sprite = nomNomGamesList[(int)currentNomNomGame].foodSpr;
        fishSR.sprite = nomNomGamesList[(int)currentNomNomGame].foodSpr;
        queenFishSR.sprite = nomNomGamesList[(int)currentNomNomGame].queenFoodSpr;

        foodUpgradeImg.sprite = nomNomGamesList[(int)currentNomNomGame].foodSpr;

        tutLabel.text = nomNomGamesList[(int)currentNomNomGame].tutLabel;

        backroundSR.sprite = nomNomGamesList[(int)currentNomNomGame].backroundSpr;

        purchaser.googlePlayPublicKey = nomNomGamesList[(int)currentNomNomGame].googlePlayPublicKey;

        CBSettings.setAppId(nomNomGamesList[(int)currentNomNomGame].appID, nomNomGamesList[(int)currentNomNomGame].appSignature);

        Debug.Log("rescin switched to " + nomNomGamesList[(int)currentNomNomGame].type.ToString());
        Debug.Log("Press: Enable OR Disable in amazon manager before release!!!");
    }
}
