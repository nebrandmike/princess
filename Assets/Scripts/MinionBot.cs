﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MinionBot : MonoBehaviour
{
    public static MinionBot Instance;
    public EpicShop epicShop;
    public Button addMinionBtn;
    public Button increaseHouseLimitBtn;
    public GameObject activatedLabelOnScreenGO;

    public enum State
    {
        Unactive,
        Waiting,
        SendingMinionsToZone
    }
    public State state;

    void Awake()
    {
        Instance = this;
    }

    public void Activate(float duration)
    {
        print("minions bot activated");
        state = State.Waiting;
        //InvokeRepeating("TryToBuyMinion", 20, 20);
        //InvokeRepeating("TryToAddMinionToZone", 15, 15);
        activatedLabelOnScreenGO.SetActive(true);
        Invoke("StopBot", duration);
    }

    void StopBot()
    {
        activatedLabelOnScreenGO.SetActive(false);
        state = State.Unactive;
    }

    void Update()
    {
        if (state == State.Waiting) SendMinionsToLastUnlockedZone();
    }

    void SendMinionsToLastUnlockedZone()
    {
        if (MinionsManager.Instance.FreeMinions() > 0)
        {
            if (GameManager.Instance.zones[GameManager.Instance.NextZoneIndex() - 1].state == Zone.State.Ready)
            {
                GameManager.Instance.zones[GameManager.Instance.NextZoneIndex() - 1].OnClick_SendMinionsToWork();
                state = State.SendingMinionsToZone;
                Invoke("ResetState", 2f);
                return;
            }
            else if ((GameManager.Instance.NextZoneIndex() - 2) >= 0)
            {
                if(GameManager.Instance.zones[GameManager.Instance.NextZoneIndex() - 2].state == Zone.State.Ready) 
                {
                    GameManager.Instance.zones[GameManager.Instance.NextZoneIndex() - 2].OnClick_SendMinionsToWork();
                    state = State.SendingMinionsToZone;
                    Invoke("ResetState", 2f);
                    return;
                }
                else if ((GameManager.Instance.NextZoneIndex() - 3) >= 0)
                {
                    if (GameManager.Instance.zones[GameManager.Instance.NextZoneIndex() - 3].state == Zone.State.Ready)
                    {
                        GameManager.Instance.zones[GameManager.Instance.NextZoneIndex() - 3].OnClick_SendMinionsToWork();
                        state = State.SendingMinionsToZone;
                        Invoke("ResetState", 2f);
                        return;
                    }
                    else if ((GameManager.Instance.NextZoneIndex() - 4) >= 0)
                    {
                        if (GameManager.Instance.zones[GameManager.Instance.NextZoneIndex() - 4].state == Zone.State.Ready)
                        {
                            GameManager.Instance.zones[GameManager.Instance.NextZoneIndex() - 4].OnClick_SendMinionsToWork();
                            state = State.SendingMinionsToZone;
                            Invoke("ResetState", 2f);
                            return;
                        }
                        else if ((GameManager.Instance.NextZoneIndex() - 5) >= 0)
                        {
                            if (GameManager.Instance.zones[GameManager.Instance.NextZoneIndex() - 5].state == Zone.State.Ready)
                            {
                                GameManager.Instance.zones[GameManager.Instance.NextZoneIndex() - 5].OnClick_SendMinionsToWork();
                                state = State.SendingMinionsToZone;
                                Invoke("ResetState", 2f);
                                return;
                            }
                            else if ((GameManager.Instance.NextZoneIndex() - 6) >= 0)
                            {
                                if (GameManager.Instance.zones[GameManager.Instance.NextZoneIndex() - 6].state == Zone.State.Ready)
                                {
                                    GameManager.Instance.zones[GameManager.Instance.NextZoneIndex() - 6].OnClick_SendMinionsToWork();
                                    state = State.SendingMinionsToZone;
                                    Invoke("ResetState", 2f);
                                    return;
                                }
                                else if ((GameManager.Instance.NextZoneIndex() - 7) >= 0)
                                {
                                    if (GameManager.Instance.zones[GameManager.Instance.NextZoneIndex() - 7].state == Zone.State.Ready)
                                    {
                                        GameManager.Instance.zones[GameManager.Instance.NextZoneIndex() - 7].OnClick_SendMinionsToWork();
                                        state = State.SendingMinionsToZone;
                                        Invoke("ResetState", 2f);
                                        return;
                                    }
                                }
                            }
                        }
                    }  
                } 
            }  
        }
    }

    void ResetState()
    {
        if(state != State.Unactive)
            state = State.Waiting;
    }

    void TryToBuyMinion()
    {
        print("try to buy minion");
        Princess.Instance.TryToBuyMinion();
    }

    void TryToAddMinionToZone()
    {
        print("try to add minion to zone");
        StartCoroutine(TryToAddMinionToZoneCoroutine());
    }

    IEnumerator TryToAddMinionToZoneCoroutine()
    {
        UpgradesShopUI.Instance.OnClick_OpenUpgrades(GameManager.Instance.
            zones[GameManager.Instance.NextZoneIndex() - 1]);
        yield return new WaitForSeconds(0.5f);
        if (!GameManager.Instance.zones[GameManager.Instance.NextZoneIndex() - 1].ZoneIsFull())
            addMinionBtn.onClick.Invoke();
        else
            GameManager.Instance.zones[GameManager.Instance.NextZoneIndex() - 1].IncreaseHouseLimitUpgrade();

        yield return new WaitForSeconds(0.75f);
        UpgradesShopUI.Instance.OnClick_CloseUpgradesPanel();
        epicShop.OnClick_CloseBackgroud();
    }
}
