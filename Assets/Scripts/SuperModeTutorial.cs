﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperModeTutorial : MonoBehaviour {

    public GameObject epicShopPanelGO;
    public GameObject idleModeItemGO;
    public GameObject trySuperModeBtnGO;
    public GameObject superModeVideoBtnGO;
    public GameObject superModeGemsBtnGO;
    public Animator superModeBtnAnimator;
    public bool gameplayInterrupted;
    public GameObject blockScreenGO;

    void Start()
    {
        // UNCOMMENT FOR BACK INTERRUPT USER FOR SUPER MODE
        //if(PlayerPrefs.GetInt("EpicShopInterrupted") == 0)
        //{
        //    Invoke("InterruptGameplay", 1200); // 20 minutes
        //}
    }

    void InterruptGameplay()
    {
        if (!Tutorial.Instance.activated)
        {
            gameplayInterrupted = true;
            UIManager.Instance.CloseAllPanels();
            PlayerPrefs.SetInt("EpicShopInterrupted", 1);
            epicShopPanelGO.SetActive(true);
            idleModeItemGO.SetActive(false);
            trySuperModeBtnGO.SetActive(true);
            superModeVideoBtnGO.SetActive(false);
            superModeGemsBtnGO.SetActive(false);
            blockScreenGO.SetActive(true);
            superModeBtnAnimator.SetTrigger("startBounce");
        }
        else
        {
            Invoke("InterruptGameplay", 200);
        }
    }

    bool freeSuperModeClicked;
    public void OnClick_FreeSuperMode()
    {
        blockScreenGO.SetActive(false);
        if (gameplayInterrupted && !freeSuperModeClicked)
        {
            freeSuperModeClicked = true;
            superModeBtnAnimator.SetTrigger("stopBounce");
            idleModeItemGO.SetActive(true);
            trySuperModeBtnGO.SetActive(false);
            superModeVideoBtnGO.SetActive(true);
            superModeGemsBtnGO.SetActive(true);
            epicShopPanelGO.SetActive(false);
            EpicShopManager.Instance.SuperModeActivated();
        }
    }

    void Update()
    {
        if (gameplayInterrupted)
        {
            superModeVideoBtnGO.SetActive(false);
            superModeGemsBtnGO.SetActive(false);
        }
    }
}
