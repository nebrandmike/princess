﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class Zone : MonoBehaviour
{
    #region Variables
    public bool activated;                // Does this zone active already?
    public int foodToUnlockZone = 50;
    public int zoneIndex;                 // Each zone has unique index (0,1,2,3,4)
    public enum State
    {
        Ready, // if zone ready to sending minions, there will display buttons "send minions" and "add minion"
        Busy   // otherwise, these buttons will be hidden
    }
    //[HideInInspector]
    public State state;

    public int currPrice = 40; // save

    //[HideInInspector]
    public int minionsInZone = 1;  // save              // how many minions can to work in this zone?
    public Transform[] minionDestinationPoints;         // poins in the scene, after reaching this minions will go back to princess
    public float harverstTime = 1;                      // duration for harvest (minion will be hidden in woods)
    public Sprite foodSpr, upgradedFoodSpr;             // sprited for food (an apple, a cake, a fish and so on)
    public float foodValue = 10;                        // how much food minions can bring to princess from this zone
    public float minionsSpeedWithHarvest = 0.05f;       // minions speed to princess direction (moving back from zone)
    public GameObject[] upgradedZoneSpritesGO;          // an array with upgraded houses (level1, level2 and so on)
    [HideInInspector]
    public int upgradeIndex;             // save        // index of house upgrades (needs for display other houses)
    private List<Minion> _minions = new List<Minion>(); // minion, working in this zone in current time
    private int _availabeMinions;                       // if we haven't enough minions, we need to hide "add minion to zone" button from player           
    private bool _upgradeFoodActivated;                 // if activated, minions can get upgradedFoodSpr instead of foodSpr

    #region Upgrades Shop (Zone) variables
    [Header("Upgrades Shop Prices")]
    public ShopUpgradePrice addWorkerPrice; // save x4
    public ShopUpgradePrice foodPriceValue; // save x4
    public ShopUpgradePrice increaseHouseLimitPrice; // save x4
    public ShopUpgradePrice findGemsChancePrice; // save x4
    public ShopUpgradePrice walkingSpeedPrice; // save x4

    public GameObject minionsCountPanelGO;
    public UnityEngine.UI.Text minionsCountLabel;
    public SpriteRenderer[] houseSprites;

    #endregion

    public GameObject foodPrefab, upgradedFoodPrefab;
    public Transform spawnFoodTr;

    public int[] limites;
    //[HideInInspector]
    public int limitesIndex; // save

    public float findGemsChanceMin = 1, findGemsChanceMax = 1;
    public Transform spawnGemsPos;
    private float _findGemsChance = 1;
    [HideInInspector]
    public bool isFindGemsMaximum; // save
    [HideInInspector]
    public int currGemsReward = 1; // save

    public GameObject progressBarGO;
    public UnityEngine.UI.Image progressBarHarvest, progressBarRefill;

    public float refillTimer = 2;
    public UnityEngine.UI.Text progressBarLabel;
    public string progressBarLabelText;
    public Animator houseAnim;
    public bool transpWhenMinionsInside = true;
    public float transparencyStrength = 0.5f;
    public bool disableHousesUpgradesSprites = true;
    public ProgressBar harvestProgressBar;
    public int minionsCountLimit = 15;
    private List<Minion> _currMinions = new List<Minion>();

    #region house limit upgrade variables
    [Space]
    public float houseLimitUpgradeTime = 10; // 10 sec. by defoult
    public bool isUpgrading;

    #endregion

    public GameObject sendToZoneBtnGO;
    public EpicShop epicShop;

    #endregion

    void Start()
    {
        _findGemsChance = findGemsChanceMin;
        CheckActivated();
        UpgradeHouseApply();
        Invoke("CalculateHarvestTime", 1);
        Invoke("SetGemsPricesForIncreaseHouseLimit", 0.5f);
    }

    void CheckActivated()
    {
        if (PlayerPrefs.GetInt("Tutorial") == 1)
        {
            string saveName = "ZoneActivated" + zoneIndex.ToString();
            if (PlayerPrefs.GetInt(saveName) == 1 && !GameManager.Instance.skipLoading)
            {
                UIManager.Instance.HideUnlockZoneButton(zoneIndex);
                activated = true;
                minionsCountPanelGO.SetActive(true);
                UIManager.Instance.unlockZonesGO[zoneIndex].SetActive(false);
                upgradedZoneSpritesGO[0].SetActive(true);
                string saveName2 = "limitesIndex" + zoneIndex.ToString();
                limitesIndex = PlayerPrefs.GetInt(saveName2);
                sendToZoneBtnGO.SetActive(true);
            }
        }
    }

    public void OnClick_Activate()
    {
        if (GameManager.Instance.Food >= foodToUnlockZone && !activated)
        {
            activated = true;
            GameManager.Instance.Food -= foodToUnlockZone;
            UIManager.Instance.HideUnlockZoneButton(zoneIndex);
            upgradedZoneSpritesGO[0].SetActive(true);
            SoundManager.Instance.PlaySound(Sounds.Upgrade);
            minionsCountPanelGO.SetActive(true);
            UIManager.Instance.unlockZonesGO[zoneIndex].SetActive(false);
            if (Tutorial.Instance.activated && Tutorial.Instance.stepsCounter == 0)
            {
                Tutorial.Instance.HideTutorialStep();
                Tutorial.Instance.ShowTutorialStep();
            }
            string saveName = "ZoneActivated" + zoneIndex.ToString();
            PlayerPrefs.SetInt(saveName, 1);
            sendToZoneBtnGO.SetActive(true);
            if (zoneIndex != 0)
                AchievementsManager.Instance.SendAchievement(AchievementName.UnlockHouse, 0, AchievementType.NotCountable);
            AnalyticsManager.Instance.SendUnlockedHouse(zoneIndex);
        }
    }

    void Update()
    {
        if (activated)
        {
            minionsCountLabel.text = minionsInZone.ToString() + "/" + limites[limitesIndex].ToString();
        }
    }

    /// <summary>
    /// On Click - it's mean this function will be called from button on screen
    /// </summary>
    public void OnClick_SendMinionsToWork()
    {
        if (MinionsManager.Instance.FreeMinions() > 0 && state == State.Ready && activated && !isUpgrading)
        {
            state = State.Busy;
            houseAnim.SetTrigger("OnClick");
            _refillTimerFlag = false;
            _minions.Clear();
            _minions = MinionsManager.Instance.FreeMinionsLst(minionsInZone);
            if(transpWhenMinionsInside)
                houseSprites[upgradeIndex].DOFade(transparencyStrength, 0);

            for (int i = 0; i < _minions.Count; i++)
            {
                _minions[i].MoveToZone(minionDestinationPoints[UnityEngine.Random.Range(0, minionDestinationPoints.Length)], MinionsManager.Instance.zoneSpeed, this);
                _currMinions.Add(_minions[i]);
            }
            if (_currMinions.Count >= 100)
                AchievementsManager.Instance.SendAchievement(AchievementName.Send100minonsToASingleHouse, 1, AchievementType.Disposable);

            _availabeMinions = 0;
            SoundManager.Instance.PlaySound(Sounds.SendMinionToWork);

            if(Tutorial.Instance.activated && Tutorial.Instance.stepsCounter == 2)
            {
                Tutorial.Instance.HideTutorialStep();
            }
            if (Tutorial.Instance.activated && Tutorial.Instance.stepsCounter == 4)
            {
                Tutorial.Instance.HideTutorialStep();
            }
            if (Tutorial.Instance.activated && Tutorial.Instance.stepsCounter == 7)
            {
                Tutorial.Instance.HideTutorialStep();
            }
        }
        else if (state == State.Busy && activated && !_refillTimerFlag && !_harvestTimeOver)
        {
            harvestProgressBar.TapOnHouse();
            houseAnim.SetTrigger("OnClick");
        }
    }

    public void DestinationReached(Minion minion)
    {
        if (!harvestProgressBar.activated && !_cardResultHarvestTimeActivated && !cardResultDoubleHouseTimingsActivated)
        {
            DisplayHarvestProgressBar((int)harverstTime);
        }
        else if (!harvestProgressBar.activated && _cardResultHarvestTimeActivated && !cardResultDoubleHouseTimingsActivated)
        {
            print("_cardResultHarvestTimeActivated true");
            print("_cardResultHarvestTime = " + _cardResultHarvestTime);
            DisplayHarvestProgressBar((int)_cardResultHarvestTime);
        }
        else if (!harvestProgressBar.activated && !_cardResultHarvestTimeActivated && cardResultDoubleHouseTimingsActivated)
        {
            DisplayHarvestProgressBar((int)_cardResultDoubleHouseHarvestTime);
        }

        if(Tutorial.Instance.activated && Tutorial.Instance.stepsCounter == 3)
        {
            Tutorial.Instance.TapOnHouseHint(true);
        }
    }

    void DisplayHarvestProgressBar(int timeToEnd)
    {
        harvestProgressBar.Activate(HarvestTimeOver, timeToEnd, progressBarLabelText, PBType.Harvest);
        _harvestTimeOver = false;
        progressBarLabel.text = progressBarLabelText;
    }

    private bool _harvestTimerActivated = false;

    public void HarvestTimeOver()
    {
        if (Tutorial.Instance.activated && Tutorial.Instance.stepsCounter == 3)
        {
            Tutorial.Instance.TapOnHouseHint(false);
        }

        for (int i = 0; i < _currMinions.Count; i++)
        {
            StartCoroutine(HarvestTimeOver(_currMinions[i]));
        }
    }

    // called if minion don't reached house, but user press limit +5 button
    public void SendMinionsToPrincess()
    {
        for (int i = 0; i < _currMinions.Count; i++)
        {
            StartCoroutine(HarvestTimeOver(_currMinions[i]));
        }
    }

    bool skipHarvestForGemActivated = false;
    public void EnableUpgradedFood()
    {
        skipHarvestForGemActivated = true;
        Invoke("DisableUpgradedFood", 2);
    }

    void DisableUpgradedFood()
    {
        skipHarvestForGemActivated = false;
    }

    private bool _harvestTimeOver = false;
    IEnumerator HarvestTimeOver(Minion minion)
    {
        _harvestTimeOver = true;
        CheckGemsChance();

        bool upgradedFood = false;
        if (!_cardResultPlus25ExtraActivated)
        {
            if (!_upgradeFoodActivated)
                upgradedFood = false;
            else
            {
                if (UnityEngine.Random.Range(0, 2) == 0)
                    upgradedFood = true;
            }
        }
        else
        {
            _cardResultPlusCounter++;
            if (_cardResultPlusCounter % _cardResultPlus25ExtraCounter == 0)
                upgradedFood = true;
        }

        if (skipHarvestForGemActivated) upgradedFood = true;
        if (EpicShopManager.Instance.superModeActivated) upgradedFood = true;

        GameObject food = null;
        if (!upgradedFood)
            food = Lean.LeanPool.Spawn(foodPrefab, spawnFoodTr.position, Quaternion.identity) as GameObject;
        else
            food = Lean.LeanPool.Spawn(upgradedFoodPrefab, spawnFoodTr.position, Quaternion.identity) as GameObject;

        food.transform.DOJump(minion.harvestSR.transform.position, 0.75f, 1, 0.75f);
        food.transform.parent = minion.harvestSR.transform;
        minion.harvestGO = food;
        yield return new WaitForSeconds(0.75f);
        //food.transform.parent = minion.harvestSR.transform;
        //minion.harvestGO = food;

        if (!upgradedFood)
        {
            if (!_cardResultDoubleFoodValueActivated && !_cardResultReduceFoodValueMinus50Activated)
                minion.MoveToPrincess(MinionsManager.Instance.zoneSpeed, (int)foodValue);
            else if (!_cardResultDoubleFoodValueActivated && _cardResultReduceFoodValueMinus50Activated)
                minion.MoveToPrincess(MinionsManager.Instance.zoneSpeed, (int)(_cardResultReduceFoodValueMinus50));
            else if (_cardResultDoubleFoodValueActivated && !_cardResultReduceFoodValueMinus50Activated)
                minion.MoveToPrincess(MinionsManager.Instance.zoneSpeed, (int)(_cardResultDoubleFoodValue));
            else if (_cardResultDoubleFoodValueActivated && _cardResultReduceFoodValueMinus50Activated)
                minion.MoveToPrincess(MinionsManager.Instance.zoneSpeed, (int)(_cardResultDoubleFoodValue));
        }
        else
        {
            if (!_cardResultDoubleFoodValueActivated && !_cardResultReduceFoodValueMinus50Activated)
                minion.MoveToPrincess(MinionsManager.Instance.zoneSpeed, (int)foodValue * 2);
            else if (!_cardResultDoubleFoodValueActivated && _cardResultReduceFoodValueMinus50Activated)
                minion.MoveToPrincess(MinionsManager.Instance.zoneSpeed, (int)(_cardResultReduceFoodValueMinus50) * 2);
            else if (_cardResultDoubleFoodValueActivated && !_cardResultReduceFoodValueMinus50Activated)
                minion.MoveToPrincess(MinionsManager.Instance.zoneSpeed, (int)(_cardResultDoubleFoodValue) * 2);
            else if (_cardResultDoubleFoodValueActivated && _cardResultReduceFoodValueMinus50Activated)
                minion.MoveToPrincess(MinionsManager.Instance.zoneSpeed, (int)(_cardResultDoubleFoodValue) * 2);
        }
    }

    [HideInInspector]
    public bool didUserSkipHarvestForGem = false;

    void CheckGemsChance()
    {
        if (!didUserSkipHarvestForGem)
        {
            System.Random random = new System.Random();
            if (random.Next(0, 300) <= _findGemsChance)
            {
                UIManager.Instance.DisplayGem(spawnGemsPos.position, 1);
                GameManager.Instance.Gems += 1;
                AchievementsManager.Instance.SendAchievement(AchievementName.GemsMagnat, 1, AchievementType.Countable);
                SoundManager.Instance.PlaySound(Sounds.RandomGemEarned);
            }
        }
    }

    public void MinionAvailable()
    {
		if (state == State.Busy) {

			state = State.Ready;
            progressBarLabel.text = progressBarLabelText;
            progressBarRefill.DOFillAmount (0, 0);
			progressBarGO.SetActive (false);
            //houseSprites[0].DOFade(1, 0);
            houseSprites [upgradeIndex].DOFade (1, 0);

            UpgradesShopUI.Instance.CheckHouseLimitTriedToClick();
            isRefilling = false;
        }
    }

    public bool ZoneIsFull()
    {
        return minionsInZone >= limites[limitesIndex];
    }

    public void OnClick_AddMinionToZone()
    {
        currPrice = (int)(addWorkerPrice.startFoodPrice * Mathf.Pow(1.15f, minionsInZone + 1));
        addWorkerPrice.food = currPrice;

        minionsInZone++;
        if (minionsInZone % 10 == 0) UpgradeHouse(); // 0-10 minions - display house 1, 10-20 minions - display house 2 and so on
        SoundManager.Instance.PlaySound(Sounds.Upgrade);
        harverstTime += 0.25f;
        
        if (minionsInZone < 10)
        {
            addWorkerPrice.gems = 1;
        }
        else if(minionsInZone >= 10 && minionsInZone < 20)
        {
            addWorkerPrice.gems = 2;
        }
        else if(minionsInZone >= 20 && minionsInZone < 30)
        {
            addWorkerPrice.gems = 3;
        }
        else if(minionsInZone >= 30 && minionsInZone < 40)
        {
            addWorkerPrice.gems = 4;
        }
        else
        {
            addWorkerPrice.gems = 5;
        }
        UpgradesShopUI.Instance.UpdatePricesDisplay();


        //after user got 15/15 minions on the first house we should add a function like "daily gift view" with "epic upgrade" . We should interrupt the gameplay from the user to show him the second tab of the shop upgrade panel
        if (minionsInZone == 15 && zoneIndex == 0)
        {
            epicShop.OnClick_OpenEpicShopPanel();
        }
    }

    public void UpgradeFood()
    {
        _upgradeFoodActivated = true;
        SoundManager.Instance.PlaySound(Sounds.Upgrade);
    }

    public void UpgradeSpeed()
    {
        minionsSpeedWithHarvest *= 2;
        SoundManager.Instance.PlaySound(Sounds.Upgrade);
    }

    void UpgradeHouse()
    {
        upgradeIndex++;
        SoundManager.Instance.PlaySound(Sounds.HouseUpgraded);
        UpgradeHouseApply();
    }

    void UpgradeHouseApply()
    {
        if (activated)
        {
            if (upgradeIndex >= 3) upgradeIndex = 3;
            for (int i = 0; i < upgradedZoneSpritesGO.Length; i++)
            {
                upgradedZoneSpritesGO[i].SetActive(false);
            }
            upgradedZoneSpritesGO[upgradeIndex].SetActive(true);
        }
    }

    #region Upgrade functions

    public void WalkingSpeedUpgrade()
    {
        minionsSpeedWithHarvest += (minionsSpeedWithHarvest / 100);
        IncreasePrice(ref walkingSpeedPrice);
    }

    public void FoodPriceValueUpgrade()
    {
        foodValue += (foodValue / 20);
        IncreasePrice(ref foodPriceValue);
    }

    public void IncreaseHouseLimitUpgrade()
    {
        isUpgrading = true;
        houseSprites[0].DOFade(transparencyStrength, 0);
        harvestProgressBar.Activate(HouseLimitUpgradeTimerEnded, (int)houseLimitUpgradeTime, "upgrade", PBType.Upgrade);

        if (limitesIndex < limites.Length)
        {
            string saveName = "limitesIndex" + zoneIndex.ToString();
            PlayerPrefs.SetInt(saveName, limitesIndex + 1);
        }

        SetGemsPricesForIncreaseHouseLimit();
        UIManager.Instance.CloseAllPanels();
        if (zoneIndex == 0)
        {
            RateUs.Instance.OpenRateUsPanel();
        }
    }

    void SetGemsPricesForIncreaseHouseLimit()
    {
        switch (limitesIndex)
        {
            case 0:
                increaseHouseLimitPrice.gems = 5;
                break;
            case 1:
                increaseHouseLimitPrice.gems = 5;
                break;
            case 2:
                increaseHouseLimitPrice.gems = 5;
                break;
            case 3:
                increaseHouseLimitPrice.gems = 5;
                break;
            case 4:
                increaseHouseLimitPrice.gems = 5;
                break;
            case 5:
                increaseHouseLimitPrice.gems = 5;
                break;
        }
    }

    public void HouseLimitUpgradeTimerEnded()
    {
        if (limitesIndex < limites.Length)
            limitesIndex++;

        houseSprites[0].DOFade(1, 0);
        IncreaseHouseLimitPrice(ref increaseHouseLimitPrice);
        SoundManager.Instance.PlaySound(Sounds.HouseUpgradeFinished);
        isUpgrading = false;
    }

    public void IncreaseGemsChanceUpgrade()
    {
        currGemsReward++;

        if(currGemsReward >= 5)
        {
            isFindGemsMaximum = true;
            UpgradesShopUI.Instance.FindGameChanceIsMaximum();
        }

        if(currGemsReward < 6)
        {
            IncreasePrice(ref findGemsChancePrice);
        }
    }

    #endregion

    void IncreasePrice(ref ShopUpgradePrice shopUpgradedPrice)
    {
        shopUpgradedPrice.food = (shopUpgradedPrice.startFoodPrice * 
            Mathf.Pow(1.15f, shopUpgradedPrice.upgradesCount + 1));

        shopUpgradedPrice.gems = (shopUpgradedPrice.startGemsPrice *
            Mathf.Pow(1.15f, shopUpgradedPrice.upgradesCount + 1));

        shopUpgradedPrice.upgradesCount++;
        if (shopUpgradedPrice.currLevel < shopUpgradedPrice.levelMax)
            shopUpgradedPrice.currLevel++;
        shopUpgradedPrice.levelLabel.text = shopUpgradedPrice.currLevel.ToString() + "/" + 
            shopUpgradedPrice.levelMax.ToString();

        SetGemsPricesForIncreaseHouseLimit();

        UpgradesShopUI.Instance.UpdatePricesDisplay();
    }

    void IncreaseHouseLimitPrice(ref ShopUpgradePrice shopUpgradedPrice)
    {
        shopUpgradedPrice.food = shopUpgradedPrice.startFoodPrice * ((shopUpgradedPrice.upgradesCount + 1) + 1.12f);
        shopUpgradedPrice.gems = shopUpgradedPrice.startGemsPrice * ((shopUpgradedPrice.upgradesCount + 1) + 1.12f);
        shopUpgradedPrice.upgradesCount++;

        if (shopUpgradedPrice.currLevel < shopUpgradedPrice.levelMax)
            shopUpgradedPrice.currLevel++;
        //shopUpgradedPrice.levelLabel.text = shopUpgradedPrice.currLevel.ToString() + "/" +
        //    shopUpgradedPrice.levelMax.ToString();

        shopUpgradedPrice.levelLabel.text = (limitesIndex + 1).ToString() + "/" +
            shopUpgradedPrice.levelMax.ToString();

        SetGemsPricesForIncreaseHouseLimit();

        UpgradesShopUI.Instance.UpdatePricesDisplay();
    }

    private bool _refillTimerFlag = false;

    public bool isRefilling = false;
    public void RefillProgressBar()
    {
        if (!_refillTimerFlag && harvestProgressBar.currPBType != PBType.Upgrade)
        {
            isRefilling = true;
            _refillTimerFlag = true;
            state = State.Busy;
            progressBarGO.SetActive(true);
            harvestProgressBar.HideSkipForGemBtn();
            progressBarLabel.text = "   refill";
            progressBarRefill.gameObject.SetActive(true);
            progressBarRefill.DOFillAmount(1, refillTimer);
            //houseSprites[0].DOFade(0.5f, 0);
            houseSprites[upgradeIndex].DOFade(0.5f, 0);

            _currMinions.Clear();
            Invoke("MinionAvailable", refillTimer);
        }
        else if (harvestProgressBar.currPBType == PBType.Upgrade)
        {
            state = State.Ready;
            _currMinions.Clear();

        }
    }

    #region Card Game Functions

    #region Double Food Value
    private bool _cardResultDoubleFoodValueActivated = false;
    private float _cardResultDoubleFoodValue = 0;
    public void CardResultDoubleFoodValue(bool isDouble, int duration)
    {
        _cardResultDoubleFoodValueActivated = true;

        if(!isDouble)
            _cardResultDoubleFoodValue = foodValue + foodValue/2;
        else
            _cardResultDoubleFoodValue = foodValue * 2;

        Invoke("DoubleFoodValueEnded", duration);
    }
    void DoubleFoodValueEnded()
    {
        _cardResultDoubleFoodValueActivated = false;
    }
    #endregion

    #region Reduce House Time
    private bool _cardResultHarvestTimeActivated = false;
    private float _cardResultHarvestTime = 0;
    public void CardResultReduceHouseTime50(bool isDouble, int duration)
    {
        _cardResultHarvestTimeActivated = true;

        if (!isDouble)
            _cardResultHarvestTime = harverstTime / 2;
        else if (isDouble)
            _cardResultHarvestTime = harverstTime / 8;

        Invoke("CardResultHarvestTimeEnded", duration);
    }
    void CardResultHarvestTimeEnded()
    {
        _cardResultHarvestTimeActivated = false;
    }
    #endregion

    #region Plus25CurrFoodAsExtraFood

    private bool _cardResultPlus25ExtraActivated = false;
    private int _cardResultPlus25ExtraCounter = 0;
    private int _cardResultPlusCounter = 0;
    public void CardResultPlus25CurrFoodAsExtraFood(bool isDouble, int duration)
    {
        _cardResultPlus25ExtraActivated = true;
        if (isDouble) _cardResultPlus25ExtraCounter = 2;
        else _cardResultPlus25ExtraCounter = 4;
        Invoke("CardResultPlus25CurrFoodAsExtraFoodEnded", duration);
    }

    void CardResultPlus25CurrFoodAsExtraFoodEnded()
    {
        _cardResultPlus25ExtraActivated = false;
    }

    #endregion

    #region DoubleHouseTimings
    [HideInInspector]
    public bool cardResultDoubleHouseTimingsActivated = false;
    private float _cardResultDoubleHouseHarvestTime = 0;
    public void CardResultDoubleHouseTimings(int duration)
    {
        cardResultDoubleHouseTimingsActivated = true;
        _cardResultDoubleHouseHarvestTime = harverstTime * 2;
        Invoke("CardResultDoubleHouseTimingsEnded", duration);
    }

    void CardResultDoubleHouseTimingsEnded()
    {
        cardResultDoubleHouseTimingsActivated = false;
    }

    #endregion

    #region ReduceFoodValueMinus50

    private bool _cardResultReduceFoodValueMinus50Activated = false;
    private float _cardResultReduceFoodValueMinus50 = 0;
    public void CardResultReduceFoodValueMinus50(int duration)
    {
        _cardResultReduceFoodValueMinus50Activated = true;
        _cardResultReduceFoodValueMinus50 = foodValue / 2;
        Invoke("ReduceFoodValueMinus50", duration);
    }
    void ReduceFoodValueMinus50()
    {
        _cardResultReduceFoodValueMinus50Activated = false;
    }

    #endregion

    #endregion

    #region Reduce Fishing Time

    private float _cachedHarvestTime = 0;
    public void ReduceFishingTimeStarted()
    {
        _cachedHarvestTime = harverstTime;
        harverstTime = harverstTime - (harverstTime / 4);
        string saveName = "_cachedHarvestTime" + zoneIndex.ToString();
        PlayerPrefs.SetFloat(saveName, _cachedHarvestTime);
    }
    public void ReduceFishingTimeEnded()
    {
        string saveName = "_cachedHarvestTime" + zoneIndex.ToString();
        _cachedHarvestTime = PlayerPrefs.GetFloat(saveName);
        if (_cachedHarvestTime != 0)
            harverstTime = _cachedHarvestTime;
    }

    #endregion

    #region Food Boost

    private float _cachedFoodValue = 0;

    public void FoodBoostStarted()
    {
        _cachedFoodValue = foodValue;
        foodValue = foodValue + (foodValue / 4);
        string saveName = "_cachedFoodValue" + zoneIndex.ToString();
        PlayerPrefs.SetFloat(saveName, _cachedFoodValue);
    }

    public void FoodBoostEnded()
    {
        string saveName = "_cachedFoodValue" + zoneIndex.ToString();
        _cachedFoodValue = PlayerPrefs.GetFloat(saveName);
        if (_cachedFoodValue != 0)
            foodValue = _cachedFoodValue;
    }

    #endregion

    public void UpdateUpgradesLevelLabels()
    {
        addWorkerPrice.levelLabel.text = addWorkerPrice.currLevel.ToString() +
            "/" + addWorkerPrice.levelMax.ToString();
        //increaseHouseLimitPrice.levelLabel.text = (increaseHouseLimitPrice).ToString() +
        //"/" + increaseHouseLimitPrice.levelMax.ToString();
        increaseHouseLimitPrice.levelLabel.text = (limitesIndex + 1).ToString() +
        "/" + increaseHouseLimitPrice.levelMax.ToString();
        foodPriceValue.levelLabel.text = foodPriceValue.currLevel.ToString() +
            "/" + foodPriceValue.levelMax.ToString();
        findGemsChancePrice.levelLabel.text = findGemsChancePrice.currLevel.ToString() +
            "/" + findGemsChancePrice.levelMax.ToString();
        walkingSpeedPrice.levelLabel.text = walkingSpeedPrice.currLevel.ToString() +
            "/" + walkingSpeedPrice.levelMax.ToString();
    }

    void CalculateHarvestTime()
    {
        for (int i = 0; i < minionsInZone; i++)
        {
            harverstTime += 0.25f;
        }
    }
}

[System.Serializable]
public class ShopUpgradePrice
{
    public float startFoodPrice;
    public float startGemsPrice;
    public float food; // save
    public float gems; // save
    public int upgradesCount; // save

    public int levelMax = 20;
    public int currLevel; // save
    public UnityEngine.UI.Text levelLabel;
}


