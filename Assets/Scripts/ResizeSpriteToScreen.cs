﻿using UnityEngine;
using System.Collections;

public class ResizeSpriteToScreen : MonoBehaviour {

    void Start()
    {
        var sr = GetComponent<SpriteRenderer>();
        if (sr == null) return;

        transform.localScale = new Vector3(1, 1, 1);

        var width = sr.sprite.bounds.size.x;
        var height = sr.sprite.bounds.size.y;

        double worldScreenHeight = Camera.main.orthographicSize * 2.0f;
        double worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

        transform.localScale = new Vector3((float)(worldScreenWidth / width), transform.localScale.y, transform.localScale.z);
        transform.localScale = new Vector3(transform.localScale.x, (float)(worldScreenHeight / height), transform.localScale.z);
    }
}
