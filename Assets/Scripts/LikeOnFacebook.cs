﻿using UnityEngine;
using System.Collections;

public class LikeOnFacebook : MonoBehaviour {

    public string facebookPageURL;
    public int gemsReward = 1;
    public GameObject likeBtnGO, likeRewardPanelGO;
    public UnityEngine.UI.Text rewardLabel;

    public GameObject freeGemsVideoBtnGO, likeFBBtnGO;
    public UnityEngine.UI.Text likeUsRewardText;
    public GameObject proposalLikeFBPanel, gemsShopGO;
    public GameObject backroundGO;

    private bool _likeBtnWasPressedBefore;

    void OnEnable()
    {
        if (PlayerPrefs.GetInt("LikeOnFacebook") == 1)
            _likeBtnWasPressedBefore = true;

        if(_likeBtnWasPressedBefore)
        {
            if(PlayerPrefs.GetInt("LikeRewardPressed") == 0)
            {
                RewardWithDelay();
            }
        }
    }

    public void DisplayBtnInShop()
    {
        print("DisplayBtnInShop");
        likeFBBtnGO.SetActive(!_likeBtnWasPressedBefore);
        freeGemsVideoBtnGO.SetActive(_likeBtnWasPressedBefore);
        likeUsRewardText.text = _likeBtnWasPressedBefore ? "+3" : "+10";
    }

    public void OnClick_Like()
    {
        print("DisplayBtnInShop");
        Shop.Instance.OnClick_OpenGemsShop(false);
        proposalLikeFBPanel.SetActive(true);
    }

    public void OnClick_NoLike()
    {
        backroundGO.SetActive(false);
        proposalLikeFBPanel.SetActive(false);
    }

    public void OnClick_GoToFBPage()
    {
        PlayerPrefs.SetInt("LikeOnFacebook", 1);
        Application.OpenURL(facebookPageURL);
        Invoke("RewardWithDelay", 0.25f);
    }

    void RewardWithDelay()
    {
        proposalLikeFBPanel.SetActive(false);
        likeBtnGO.SetActive(false);
        likeRewardPanelGO.SetActive(true);
        likeRewardPanelGO.GetComponent<Animator>().SetTrigger("Appear");
        rewardLabel.text = "+" + gemsReward.ToString();
        UIManager.Instance.OnPanelOpen(true); 
        _likeBtnWasPressedBefore = true;
        likeFBBtnGO.SetActive(false);
        likeUsRewardText.text = "+3";
        freeGemsVideoBtnGO.SetActive(true);
    }

    public void OnClick_RewardOK()
    {
        AnalyticsManager.Instance.FacebookLikePressed();
        GameManager.Instance.Gems += gemsReward;
        AchievementsManager.Instance.SendAchievement(AchievementName.GemsMagnat, gemsReward, AchievementType.Countable);
        PlayerPrefs.SetInt("LikeRewardPressed", 1);
        backroundGO.SetActive(false);
        likeRewardPanelGO.SetActive(false);
        UIManager.Instance.OnPanelOpen(false);
        SoundManager.Instance.PlaySound(Sounds.Panel);
    }
}
