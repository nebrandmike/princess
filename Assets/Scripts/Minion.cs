﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Minion : MonoBehaviour
{
    // Let's use pattern State for minion
    public enum State
    {
        Birth,           // minion is born
        Free,            // minion is free (idle)
        MoveToNewPoint,  // walking around princess
        Busy,            // minion is busy, working in zones
        MoveToPrincess   // bring food to the princess
    }
    public State state;
    public float speed = 0.05f;
    public float minWaitingTime = 1.5f, maxWaitingTime = 5; // range for waiting in idle state
                                                            //(before minion change his position around the princess
    public SpriteRenderer harvestSR; // food that minion will bring to the princess

    private Animator _animator; // link to minion's animator, caching pattern (good for performance)
    private int idleState = Animator.StringToHash("Base Layer.Idle");
    //private int idleBackState = Animator.StringToHash("Base Layer.IdleBack");
    private int courseState = Animator.StringToHash("Base Layer.Cource");
    private int courseBackState = Animator.StringToHash("Base Layer.CourceBack");
    private int blinkState = Animator.StringToHash("Base Layer.Blink");
    private Transform _destinationPoint; // point to minion moving
    private float _zoneSpeed = 0.05f;    // speed to zone
    private Zone _currentZone;           // index of zone, minion work in
    private int _foodValue;              // how much food minion brings to princess from zone
    private bool _isBlinking;

    public GameObject harvestGO;

    void Start()
    {
        MinionsManager.Instance.AddMinion(this);
        _animator = GetComponent<Animator>();
        speed = MinionsManager.Instance.minionsSpeed;
        Invoke("Waiting", 1);
    }

    void Waiting()
    {
        if (state != State.Busy)
        {
            state = State.Free;
            _animator.goToStateIfNotAlreadyThere(courseState);
            CheckIfSlowMinionsActivated();
            CheckIfImproveMinionsSpeedActivated();
            Invoke("MoveToNewPoint", Random.Range(minWaitingTime, maxWaitingTime));
        }
    }

    /// <summary>
    /// Walking around the princess
    /// </summary>
    void MoveToNewPoint()
    {
        if (state == State.Free)
        {
            _destinationPoint = MinionsManager.Instance.RandomWaypoint();
            _animator.applyRootMotion = true;
            state = State.MoveToNewPoint;
            if (Random.Range(0, 100) < 75)
            {
                _animator.goToStateIfNotAlreadyThere(blinkState);
                _isBlinking = true;
                Invoke("ResetBlinkingFlag", 0.5f);
            }
        }
    }

    void ResetBlinkingFlag()
    {
        _isBlinking = false;
    }

    /// <summary>
    /// I use state pattern for minion behaviour
    /// </summary>
    void FixedUpdate()
    {
        if(state == State.MoveToNewPoint && _destinationPoint != null)
        {
            if (Vector3.Distance(transform.position, _destinationPoint.position) > 0.1f)
            {
                if(!_slowMinionsActivated && !_improvedSpeedActivated)
                    transform.position = Vector3.MoveTowards(transform.position, _destinationPoint.position, speed);
                else if(_slowMinionsActivated && !_improvedSpeedActivated)
                    transform.position = Vector3.MoveTowards(transform.position, _destinationPoint.position, _slowMinionsSpeed);
                else if(!_slowMinionsActivated && _improvedSpeedActivated)
                    transform.position = Vector3.MoveTowards(transform.position, _destinationPoint.position, _improvedSpeed);

                if (!_isBlinking)
                    _animator.goToStateIfNotAlreadyThere(courseState);  
            }
            else
            {
                Waiting();
            }
        }

        if(state == State.Busy && _destinationPoint != null)
        {
            if (Vector3.Distance(transform.position, _destinationPoint.position) > 0.1f)
            {
                if(!_slowMinionsActivated && !_improvedSpeedActivated)
                    transform.position = Vector3.MoveTowards(transform.position, _destinationPoint.position, _zoneSpeed);
                else if(_slowMinionsActivated && !_improvedSpeedActivated)
                    transform.position = Vector3.MoveTowards(transform.position, _destinationPoint.position, _slowMinionsZoneSpeed);
                else if(!_slowMinionsActivated && _improvedSpeedActivated)
                    transform.position = Vector3.MoveTowards(transform.position, _destinationPoint.position, _improvedZonesSpeed);

                _animator.goToStateIfNotAlreadyThere(courseState);
            }
            else
            {
                _currentZone.DestinationReached(this);
                _destinationPoint = null;
            }
        }

        if(state == State.MoveToPrincess && _destinationPoint != null)
        {
            if (Vector3.Distance(transform.position, _destinationPoint.position) > 1.25f)
            {
                if(!_slowMinionsActivated && !_improvedSpeedActivated)
                    transform.position = Vector3.MoveTowards(transform.position, _destinationPoint.position, _zoneSpeed);
                else if(_slowMinionsActivated && !_improvedSpeedActivated)
                    transform.position = Vector3.MoveTowards(transform.position, _destinationPoint.position, _slowMinionsZoneSpeed);
                else if(!_slowMinionsActivated && _improvedSpeedActivated)
                    transform.position = Vector3.MoveTowards(transform.position, _destinationPoint.position, _improvedZonesSpeed);

                _animator.goToStateIfNotAlreadyThere(courseBackState);
            }
            else
            {
                state = State.MoveToNewPoint;
                float rndFoodPos = Random.Range(-1.5f, 1.5f);
                UIManager.Instance.DisplayFoodForPrincessToEat(_foodValue, new Vector3(transform.position.x + rndFoodPos,
                    transform.position.y, transform.position.z));

                harvestGO.transform.DOJump(Princess.Instance.transform.position, 0.75f, 1, 0.75f);
                harvestGO.transform.DOScale(0, 0.75f);
                Destroy(harvestGO, 0.75f);
                if(harvestSR.transform.childCount > 0)
                {
                    Destroy(harvestSR.transform.GetChild(0).gameObject, 0f);
                    for (int i = 0; i < harvestGO.transform.childCount; i++)
                    {
                        Destroy(harvestGO.transform.GetChild(i));
                    }
                }
                _destinationPoint = MinionsManager.Instance.RandomWaypoint();
                SoundManager.Instance.PlaySound(Sounds.PrincessEat);
                _currentZone.RefillProgressBar();
                
                transform.DORotate(Vector3.zero, 0);

                if (Tutorial.Instance.activated && Tutorial.Instance.stepsCounter == 3)
                {
                    Invoke("ShowTut", 1);
                }
                if (Tutorial.Instance.activated && Tutorial.Instance.stepsCounter == 5)
                {
                    Invoke("ShowTut", 1);
                }
                if (Tutorial.Instance.activated && Tutorial.Instance.stepsCounter == 8)
                {
                    Invoke("ShowTut", 1);
                }
            }
        }
    }

    void ShowTut()
    {
        Tutorial.Instance.ShowTutorialStep();
    }

    /// <summary>
    /// Called from zone to any free minion
    /// </summary>
    /// <param name="destination"></param>
    /// <param name="zoneSpeed"></param>
    /// <param name="zone"></param>
    public void MoveToZone(Transform destination, float zoneSpeed, Zone zone)
    {
        state = State.Busy;
        _destinationPoint = destination;
        _zoneSpeed = zoneSpeed;
        _animator.applyRootMotion = true;
        if(Random.Range(0,100) < 50)
        {
            _animator.goToStateIfNotAlreadyThere(blinkState);
        }
        _currentZone = zone;
    }
    /// <summary>
    /// Called from zone after minion get a food to bring it to the princess
    /// </summary>
    /// <param name="food"></param>
    /// <param name="newSpeed"></param>
    /// <param name="foodValue"></param>
    public void MoveToPrincess(float newSpeed, int foodValue)
    {
        state = State.MoveToPrincess;
        _zoneSpeed = newSpeed;
        _destinationPoint = Princess.Instance.transform;
        _foodValue = foodValue;
        transform.DORotate(Vector3.zero, 0);
    }

    #region Card Result Slow Minions Speed

    private bool _slowMinionsActivated = false;
    private float _slowMinionsSpeed = 0;
    private float _slowMinionsZoneSpeed = 0;
    public void CardResultSlowMinionsSpeedStarted()
    {
        _slowMinionsActivated = true;
        _slowMinionsSpeed = MinionsManager.Instance.minionsSpeed / 2;
        _slowMinionsZoneSpeed = MinionsManager.Instance.zoneSpeed / 2;
    }

    public void CardResultSlowMinionsSpeedEnded()
    {
        _slowMinionsActivated = false;
    }

    void CheckIfSlowMinionsActivated()
    {
        if (CardsManager.Instance.doubleSlowMinionsActivated)
            CardResultSlowMinionsSpeedStarted();
    }

    void CheckIfImproveMinionsSpeedActivated()
    {
        if(EpicShopManager.Instance.improveMinionsSpeedActivated)
        {
            ImproveSpeedStarted();
        }
    }

    #endregion

    #region Epic Upgrade: Improve minions speed 25%

    private bool _improvedSpeedActivated = false;
    private float _improvedSpeed = 0;
    private float _improvedZonesSpeed = 0;

    public void ImproveSpeedStarted()
    {
        _improvedSpeedActivated = true;
        _improvedSpeed = speed + (speed / 4);
        _improvedZonesSpeed = _zoneSpeed + (_zoneSpeed / 4);
    }
    public void ImproveSpeedEnded()
    {
        _improvedSpeedActivated = false;
    }

    #endregion
}
