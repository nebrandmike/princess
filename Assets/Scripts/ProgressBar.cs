﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public delegate void TimerEndedDelegate();

public enum PBType
{
    Harvest,
    Upgrade
}

public class ProgressBar : MonoBehaviour
{
    public GameObject progressBarGO;

    public Image harvestImg;
    public Image refillImg;
    public Image upgradeImg;

    private float _speed = 30;
    public GameObject skipForGemBtnGO;
    public Text label;
    public Transform spawnMinus1Pos;
    private string _harvestLabel;
    [HideInInspector] public float currentAmount;
    [HideInInspector] public bool activated;
    public bool debugTimer;
    public bool isSkipForGemsEnabled = true;
    public Zone currZone;

    private float _timer;
    private TimerEndedDelegate _callback;
    public PBType currPBType;
        
    public void Activate(TimerEndedDelegate del, int seconds, string harvestLabel, PBType type)
    {
        progressBarGO.SetActive(true);

        currPBType = type;
        if (currPBType == PBType.Harvest)
        {
            upgradeImg.fillAmount = 0;
        }
        else if (currPBType == PBType.Upgrade)
        {
            harvestImg.fillAmount = 0;
        }

        _callback = del;
        if(seconds != 0)
            _speed = 100 / seconds;

        activated = true;
        currentAmount = 1;
        _harvestLabel = harvestLabel;
        isTimerEnded = false;
        if (GameManager.Instance.Gems > 0 && isSkipForGemsEnabled && !EpicShopManager.Instance.superModeActivated)
            skipForGemBtnGO.SetActive(true);
        else
            skipForGemBtnGO.SetActive(false);
    }

    void FixedUpdate()
    {
        if(activated)
        {
            if(currentAmount < 100)
            {
                progressBarGO.SetActive(true);
                currentAmount += _speed * Time.fixedDeltaTime;

                if (currPBType == PBType.Harvest)
                    harvestImg.fillAmount = currentAmount / 100;
                else if (currPBType == PBType.Upgrade)
                    upgradeImg.fillAmount = currentAmount / 100;

                _timer += Time.fixedDeltaTime;
                label.text = _harvestLabel + " " + ((int)(currentAmount)).ToString() + "%";
                if(refillImg != null)
                    refillImg.gameObject.SetActive(false);
            }
            else
            {
                TimerEnded();
            }
        }
    }

    bool isTimerEnded = false;

    void TimerEnded()
    {
        if (!isTimerEnded)
        {
            isTimerEnded = true;

            if (debugTimer)
                print(_timer);

            currentAmount = 100;
            harvestImg.fillAmount = 0;
            progressBarGO.SetActive(false);
            _callback();
            if(currPBType == PBType.Harvest)
                Invoke("ResetActivatedFlag", GameManager.Instance.zones[0].refillTimer);
            else if(currPBType == PBType.Upgrade)
            {
                SoundManager.Instance.PlaySound(Sounds.HousePlus5Completed);
                activated = false;
            }
        }
    }

    // called when user press limit +5 while house is busy
    public void PlayCallback()
    {
        if (_callback != null)
        {
            _callback();
            currZone.SendMinionsToPrincess();
        }
        else
        {
            currZone.SendMinionsToPrincess();
        }
    }

    void ResetActivatedFlag()
    {
        activated = false;
    }

    public void TapOnHouse()
    { 
        if(EpicShopManager.Instance.superModeActivated)
        {
            currZone.didUserSkipHarvestForGem = true;
            StartCoroutine(ResetSkipHarvestForGemFlag(currZone));

            SoundManager.Instance.PlaySound(Sounds.SkipForGem);
            skipForGemBtnGO.GetComponent<Animator>().SetTrigger("Pressed");

            if (currPBType == PBType.Harvest)
            {
                AnalyticsManager.Instance.SpendGemForSkipHarvestProgressBar();
                currZone.EnableUpgradedFood();
            }

            Invoke("HideSkipForGemBtn", 0.25f);
            TimerEnded();
            return;
        }

        if (activated)
        {
            if (!currZone.cardResultDoubleHouseTimingsActivated)
                currentAmount += GameManager.Instance.tappsZonesSpeed * 2; // 10 by default
            else
                currentAmount += GameManager.Instance.tappsZonesSpeed;  // 5 by default

            UIManager.Instance.DisplayMinus1(spawnMinus1Pos.position);
            AchievementsManager.Instance.SendAchievement(AchievementName.TapOnHouse, 1, AchievementType.Countable);
            SoundManager.Instance.PlaySound(Sounds.TapOnHouse);
        }
    }

    public void OnClick_SkipForGem()
    {
        if (_callback == null)
            print("callback is null");

        if(GameManager.Instance.Gems > 0)
        {
            currZone.didUserSkipHarvestForGem = true;
            StartCoroutine(ResetSkipHarvestForGemFlag(currZone));

            SoundManager.Instance.PlaySound(Sounds.SkipForGem);
            GameManager.Instance.Gems -= 1;
            skipForGemBtnGO.GetComponent<Animator>().SetTrigger("Pressed");

            if (currPBType == PBType.Upgrade)
            {
                AnalyticsManager.Instance.SpendGemForSkipUpgradeProgressBar();
            }
            else if (currPBType == PBType.Harvest)
            {
                AnalyticsManager.Instance.SpendGemForSkipHarvestProgressBar();
                currZone.EnableUpgradedFood();
            }

            Invoke("HideSkipForGemBtn", 0.25f);
            TimerEnded();
        }
    }

    IEnumerator ResetSkipHarvestForGemFlag(Zone zone)
    {
        yield return new WaitForSeconds(2);
        zone.didUserSkipHarvestForGem = false;
    }

    public void HideSkipForGemBtn()
    {
        if(!currZone.isUpgrading)
            skipForGemBtnGO.SetActive(false);
    }
}
