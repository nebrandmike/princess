﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayerPrefs = PreviewLabs.PlayerPrefs;
using System;

public class PlayerPrefsSaver : MonoBehaviour
{
    private bool _defaultSavesActivated;

    public void Save()
    {
        if (!_defaultSavesActivated)
        {
            PlayerPrefs.SetInt("GameManager.Instance.Food", GameManager.Instance.Food);
            PlayerPrefs.SetInt("GameManager.Instance.Gems", GameManager.Instance.Gems);
            PlayerPrefs.SetInt("MinionsManager.Instance.TotalMinions", MinionsManager.Instance.TotalMinions());
            PlayerPrefs.SetFloat("Princess.Instance.currPrice", Princess.Instance.currPrice);

            List<int> zonesUpgradesIndex = new List<int>();
            List<int> zonesCurrPrice = new List<int>();
            List<int> minionsInZone = new List<int>();
            List<int> limitesIndex = new List<int>();
            List<bool> isFindGemsMaximum = new List<bool>();
            List<int> currGemsReward = new List<int>();
            List<float> addWorkerCurrFoodPrice = new List<float>();
            List<float> addWorkerCurrGemsPrice = new List<float>();
            List<int> addWorkerUpgradesCount = new List<int>();
            List<int> addWorkerCurrLevel = new List<int>();
            List<float> incLimitCurrFoodPrice = new List<float>();
            List<float> incLimitCurrGemsPrice = new List<float>();
            List<int> incLimitUpgradesCount = new List<int>();
            List<int> incLimitCurrLevel = new List<int>();
            List<float> foodPriceCurrFoodPrice = new List<float>();
            List<float> foodPriceCurrGemsPrice = new List<float>();
            List<int> foodPriceUpgradesCount = new List<int>();
            List<int> foodPriceCurrLevel = new List<int>();
            List<float> findGemsCurrFoodPrice = new List<float>();
            List<float> findGemsCurrGemsPrice = new List<float>();
            List<int> findGemsUpgradesCount = new List<int>();
            List<int> findGemsCurrLevel = new List<int>();
            List<float> walkingSpeedCurrFoodPrice = new List<float>();
            List<float> walkingSpeedCurrGemsPrice = new List<float>();
            List<int> walkingSpeedUpgradesCount = new List<int>();
            List<int> walkingSpeedCurrLevel = new List<int>();

            for (int i = 0; i < GameManager.Instance.zones.Count; i++)
            {
                zonesUpgradesIndex.Add(GameManager.Instance.zones[i].upgradeIndex);
                zonesCurrPrice.Add(GameManager.Instance.zones[i].currPrice);
                minionsInZone.Add(GameManager.Instance.zones[i].minionsInZone);
                limitesIndex.Add(GameManager.Instance.zones[i].limitesIndex);
                isFindGemsMaximum.Add(GameManager.Instance.zones[i].isFindGemsMaximum);
                currGemsReward.Add(GameManager.Instance.zones[i].currGemsReward);
                

                addWorkerCurrFoodPrice.Add(GameManager.Instance.zones[i].addWorkerPrice.food);
                addWorkerCurrGemsPrice.Add(GameManager.Instance.zones[i].addWorkerPrice.gems);
                addWorkerUpgradesCount.Add(GameManager.Instance.zones[i].addWorkerPrice.upgradesCount);
                addWorkerCurrLevel.Add(GameManager.Instance.zones[i].addWorkerPrice.currLevel);

                incLimitCurrFoodPrice.Add(GameManager.Instance.zones[i].increaseHouseLimitPrice.food);
                incLimitCurrGemsPrice.Add(GameManager.Instance.zones[i].increaseHouseLimitPrice.gems);
                incLimitUpgradesCount.Add(GameManager.Instance.zones[i].increaseHouseLimitPrice.upgradesCount);
                incLimitCurrLevel.Add(GameManager.Instance.zones[i].increaseHouseLimitPrice.currLevel);

                foodPriceCurrFoodPrice.Add(GameManager.Instance.zones[i].foodPriceValue.food);
                foodPriceCurrGemsPrice.Add(GameManager.Instance.zones[i].foodPriceValue.gems);
                foodPriceUpgradesCount.Add(GameManager.Instance.zones[i].foodPriceValue.upgradesCount);
                foodPriceCurrLevel.Add(GameManager.Instance.zones[i].foodPriceValue.currLevel);

                findGemsCurrFoodPrice.Add(GameManager.Instance.zones[i].findGemsChancePrice.food);
                findGemsCurrGemsPrice.Add(GameManager.Instance.zones[i].findGemsChancePrice.gems);
                findGemsUpgradesCount.Add(GameManager.Instance.zones[i].findGemsChancePrice.upgradesCount);
                findGemsCurrLevel.Add(GameManager.Instance.zones[i].findGemsChancePrice.currLevel);

                walkingSpeedCurrFoodPrice.Add(GameManager.Instance.zones[i].walkingSpeedPrice.food);
                walkingSpeedCurrGemsPrice.Add(GameManager.Instance.zones[i].walkingSpeedPrice.gems);
                walkingSpeedUpgradesCount.Add(GameManager.Instance.zones[i].walkingSpeedPrice.upgradesCount);
                walkingSpeedCurrLevel.Add(GameManager.Instance.zones[i].walkingSpeedPrice.currLevel);
            }

            PlayerPrefsX.SetIntArray("zonesUpgradesIndex", zonesUpgradesIndex.ToArray());
            PlayerPrefsX.SetIntArray("zonesCurrPrice", zonesCurrPrice.ToArray());
            PlayerPrefsX.SetIntArray("minionsInZone", minionsInZone.ToArray());
            PlayerPrefsX.SetIntArray("limitesIndex", limitesIndex.ToArray());
            PlayerPrefsX.SetBoolArray("isFindGemsMaximum", isFindGemsMaximum.ToArray());
            PlayerPrefsX.SetIntArray("currGemsReward", currGemsReward.ToArray());
            PlayerPrefsX.SetFloatArray("addWorkerCurrFoodPrice", addWorkerCurrFoodPrice.ToArray());
            PlayerPrefsX.SetFloatArray("addWorkerCurrGemsPrice", addWorkerCurrGemsPrice.ToArray());
            PlayerPrefsX.SetIntArray("addWorkerUpgradesCount", addWorkerUpgradesCount.ToArray());
            PlayerPrefsX.SetIntArray("addWorkerCurrLevel", addWorkerCurrLevel.ToArray());
            PlayerPrefsX.SetFloatArray("incLimitCurrFoodPrice", incLimitCurrFoodPrice.ToArray());
            PlayerPrefsX.SetFloatArray("incLimitCurrGemsPrice", incLimitCurrGemsPrice.ToArray());
            PlayerPrefsX.SetIntArray("incLimitUpgradesCount", incLimitUpgradesCount.ToArray());
            PlayerPrefsX.SetIntArray("incLimitCurrLevel", incLimitCurrLevel.ToArray());
            PlayerPrefsX.SetFloatArray("foodPriceCurrFoodPrice", foodPriceCurrFoodPrice.ToArray());
            PlayerPrefsX.SetFloatArray("foodPriceCurrGemsPrice", foodPriceCurrGemsPrice.ToArray());
            PlayerPrefsX.SetIntArray("foodPriceUpgradesCount", foodPriceUpgradesCount.ToArray());
            PlayerPrefsX.SetIntArray("foodPriceCurrLevel", foodPriceCurrLevel.ToArray());
            PlayerPrefsX.SetFloatArray("findGemsCurrFoodPrice", findGemsCurrFoodPrice.ToArray());
            PlayerPrefsX.SetFloatArray("findGemsCurrGemsPrice", findGemsCurrGemsPrice.ToArray());
            PlayerPrefsX.SetIntArray("findGemsUpgradesCount", findGemsUpgradesCount.ToArray());
            PlayerPrefsX.SetIntArray("findGemsCurrLevel", findGemsCurrLevel.ToArray());
            PlayerPrefsX.SetFloatArray("walkingSpeedCurrFoodPrice", walkingSpeedCurrFoodPrice.ToArray());
            PlayerPrefsX.SetFloatArray("walkingSpeedCurrGemsPrice", walkingSpeedCurrGemsPrice.ToArray());
            PlayerPrefsX.SetIntArray("walkingSpeedUpgradesCount", walkingSpeedUpgradesCount.ToArray());
            PlayerPrefsX.SetIntArray("walkingSpeedCurrLevel", walkingSpeedCurrLevel.ToArray());

            PlayerPrefs.SetInt("CatLevels.Instance.catLevel", CatLevels.Instance.catLevel);

            PlayerPrefs.SetInt("unlockHouseCurrLevel", AchievementsManager.Instance.achievements[0].currLevel);
            PlayerPrefs.SetInt("unlockHouseCurrValue", AchievementsManager.Instance.achievements[0].currValue);

            PlayerPrefs.SetInt("foodMagnatCurrLevel", AchievementsManager.Instance.achievements[1].currLevel);
            PlayerPrefs.SetInt("foodMagnatCurrValue", AchievementsManager.Instance.achievements[1].currValue);

            PlayerPrefs.SetInt("gemsMagnatHouseCurrLevel", AchievementsManager.Instance.achievements[2].currLevel);
            PlayerPrefs.SetInt("gemsMagnatCurrValue", AchievementsManager.Instance.achievements[2].currValue);

            PlayerPrefs.SetInt("bornMinionsHouseCurrLevel", AchievementsManager.Instance.achievements[3].currLevel);
            PlayerPrefs.SetInt("bornMinionsCurrValue", AchievementsManager.Instance.achievements[3].currValue);

            PlayerPrefs.SetInt("tapOnHouseCurrLevel", AchievementsManager.Instance.achievements[4].currLevel);
            PlayerPrefs.SetInt("tapOnHouseCurrValue", AchievementsManager.Instance.achievements[4].currValue);

            List<bool> unlockedAchievements = new List<bool>();
            for (int i = 0; i < AchievementsManager.Instance.unlockedAchievements.Length; i++)
                unlockedAchievements.Add(AchievementsManager.Instance.unlockedAchievements[i]);
            PlayerPrefsX.SetBoolArray("unlockedAchievements", unlockedAchievements.ToArray());

            List<bool> achievementsFinished = new List<bool>();
            for (int i = 0; i < AchievementsManager.Instance.achievementsFinished.Length; i++)
                achievementsFinished.Add(AchievementsManager.Instance.achievementsFinished[i]);
            PlayerPrefsX.SetBoolArray("achievementsFinished", achievementsFinished.ToArray());

            PlayerPrefs.Flush();
        }
    }

    public void Load(bool fromScratch)
    {
        if (fromScratch)
        {
            GameManager.Instance.SetStartFoodAndGems();
            return;
        }
        else
        {
            GameManager.Instance.Food = PlayerPrefs.GetInt("GameManager.Instance.Food");
            GameManager.Instance.Gems = PlayerPrefs.GetInt("GameManager.Instance.Gems");
            Princess.Instance.LoadMinions(PlayerPrefs.GetInt("MinionsManager.Instance.TotalMinions"));
            Princess.Instance.currPrice = PlayerPrefs.GetFloat("Princess.Instance.currPrice");

            int[] zonesUpgradesIndex = PlayerPrefsX.GetIntArray("zonesUpgradesIndex");
            int[] zonesCurrPrice = PlayerPrefsX.GetIntArray("zonesCurrPrice");
            int[] minionsInZone = PlayerPrefsX.GetIntArray("minionsInZone");
            int[] limitesIndex = PlayerPrefsX.GetIntArray("limitesIndex");
            bool[] isFindGemsMaximum = PlayerPrefsX.GetBoolArray("isFindGemsMaximum");
            int[] currGemsReward = PlayerPrefsX.GetIntArray("currGemsReward");
            float[] addWorkerCurrFoodPrice = PlayerPrefsX.GetFloatArray("addWorkerCurrFoodPrice");
            float[] addWorkerCurrGemsPrice = PlayerPrefsX.GetFloatArray("addWorkerCurrGemsPrice");
            int[] addWorkerUpgradesCount = PlayerPrefsX.GetIntArray("addWorkerUpgradesCount");
            int[] addWorkerCurrLevel = PlayerPrefsX.GetIntArray("addWorkerCurrLevel");
            float[] incLimitCurrFoodPrice = PlayerPrefsX.GetFloatArray("incLimitCurrFoodPrice");
            float[] incLimitCurrGemsPrice = PlayerPrefsX.GetFloatArray("incLimitCurrGemsPrice");
            int[] incLimitUpgradesCount = PlayerPrefsX.GetIntArray("incLimitUpgradesCount");
            int[] incLimitCurrLevel = PlayerPrefsX.GetIntArray("incLimitCurrLevel");
            float[] foodPriceCurrFoodPrice = PlayerPrefsX.GetFloatArray("foodPriceCurrFoodPrice");
            float[] foodPriceCurrGemsPrice = PlayerPrefsX.GetFloatArray("foodPriceCurrGemsPrice");
            int[] foodPriceUpgradesCount = PlayerPrefsX.GetIntArray("foodPriceUpgradesCount");
            int[] foodPriceCurrLevel = PlayerPrefsX.GetIntArray("foodPriceCurrLevel");
            float[] findGemsCurrFoodPrice = PlayerPrefsX.GetFloatArray("findGemsCurrFoodPrice");
            float[] findGemsCurrGemsPrice = PlayerPrefsX.GetFloatArray("findGemsCurrGemsPrice");
            int[] findGemsUpgradesCount = PlayerPrefsX.GetIntArray("findGemsUpgradesCount");
            int[] findGemsCurrLevel = PlayerPrefsX.GetIntArray("findGemsCurrLevel");
            float[] walkingSpeedCurrFoodPrice = PlayerPrefsX.GetFloatArray("walkingSpeedCurrFoodPrice");
            float[] walkingSpeedCurrGemsPrice = PlayerPrefsX.GetFloatArray("walkingSpeedCurrGemsPrice");
            int[] walkingSpeedUpgradesCount = PlayerPrefsX.GetIntArray("walkingSpeedUpgradesCount");
            int[] walkingSpeedCurrLevel = PlayerPrefsX.GetIntArray("walkingSpeedCurrLevel");
            float[] foodValue = PlayerPrefsX.GetFloatArray("foodValue");

            for (int i = 0; i < GameManager.Instance.zones.Count; i++)
            {
                GameManager.Instance.zones[i].upgradeIndex = zonesUpgradesIndex[i];

                GameManager.Instance.zones[i].currPrice = zonesCurrPrice[i];
                GameManager.Instance.zones[i].minionsInZone = minionsInZone[i];
                GameManager.Instance.zones[i].limitesIndex = limitesIndex[i];
                GameManager.Instance.zones[i].isFindGemsMaximum = isFindGemsMaximum[i];
                GameManager.Instance.zones[i].currGemsReward = currGemsReward[i];

                GameManager.Instance.zones[i].addWorkerPrice.food = addWorkerCurrFoodPrice[i];
                GameManager.Instance.zones[i].addWorkerPrice.gems = addWorkerCurrGemsPrice[i];
                GameManager.Instance.zones[i].addWorkerPrice.upgradesCount = addWorkerUpgradesCount[i];
                GameManager.Instance.zones[i].addWorkerPrice.currLevel = addWorkerCurrLevel[i];

                GameManager.Instance.zones[i].increaseHouseLimitPrice.food = incLimitCurrFoodPrice[i];
                GameManager.Instance.zones[i].increaseHouseLimitPrice.gems = incLimitCurrGemsPrice[i];
                GameManager.Instance.zones[i].increaseHouseLimitPrice.upgradesCount = incLimitUpgradesCount[i];
                GameManager.Instance.zones[i].increaseHouseLimitPrice.currLevel = incLimitCurrLevel[i];

                GameManager.Instance.zones[i].foodPriceValue.food = foodPriceCurrFoodPrice[i];
                GameManager.Instance.zones[i].foodPriceValue.gems = foodPriceCurrGemsPrice[i];
                GameManager.Instance.zones[i].foodPriceValue.upgradesCount = foodPriceUpgradesCount[i];
                GameManager.Instance.zones[i].foodPriceValue.currLevel = foodPriceCurrLevel[i];

                GameManager.Instance.zones[i].findGemsChancePrice.food = findGemsCurrFoodPrice[i];
                GameManager.Instance.zones[i].findGemsChancePrice.gems = findGemsCurrGemsPrice[i];
                GameManager.Instance.zones[i].findGemsChancePrice.upgradesCount = findGemsUpgradesCount[i];
                GameManager.Instance.zones[i].findGemsChancePrice.currLevel = findGemsCurrLevel[i];

                GameManager.Instance.zones[i].walkingSpeedPrice.food = walkingSpeedCurrFoodPrice[i];
                GameManager.Instance.zones[i].walkingSpeedPrice.gems = walkingSpeedCurrGemsPrice[i];
                GameManager.Instance.zones[i].walkingSpeedPrice.upgradesCount = walkingSpeedUpgradesCount[i];
                GameManager.Instance.zones[i].walkingSpeedPrice.currLevel = walkingSpeedCurrLevel[i];
            }

            CatLevels.Instance.catLevel = PlayerPrefs.GetInt("CatLevels.Instance.catLevel");

            AchievementsManager.Instance.achievements[0].currLevel = PlayerPrefs.GetInt("unlockHouseCurrLevel");
            AchievementsManager.Instance.achievements[0].currValue = PlayerPrefs.GetInt("unlockHouseCurrValue");

            AchievementsManager.Instance.achievements[1].currLevel = PlayerPrefs.GetInt("foodMagnatCurrLevel");
            AchievementsManager.Instance.achievements[1].currValue = PlayerPrefs.GetInt("foodMagnatCurrValue");

            AchievementsManager.Instance.achievements[2].currLevel = PlayerPrefs.GetInt("gemsMagnatHouseCurrLevel");
            AchievementsManager.Instance.achievements[2].currValue = PlayerPrefs.GetInt("gemsMagnatCurrValue");

            AchievementsManager.Instance.achievements[3].currLevel = PlayerPrefs.GetInt("bornMinionsHouseCurrLevel");
            AchievementsManager.Instance.achievements[3].currValue = PlayerPrefs.GetInt("bornMinionsCurrValue");

            AchievementsManager.Instance.achievements[4].currLevel = PlayerPrefs.GetInt("tapOnHouseCurrLevel");
            AchievementsManager.Instance.achievements[4].currValue = PlayerPrefs.GetInt("tapOnHouseCurrValue");

            bool[] unlockedAchievements = PlayerPrefsX.GetBoolArray("unlockedAchievements");
            for (int i = 0; i < AchievementsManager.Instance.unlockedAchievements.Length; i++)
                AchievementsManager.Instance.unlockedAchievements[i] = unlockedAchievements[i];

            bool[] achievementsFinished = PlayerPrefsX.GetBoolArray("achievementsFinished");
            for (int i = 0; i < AchievementsManager.Instance.achievementsFinished.Length; i++)
                AchievementsManager.Instance.achievementsFinished[i] = achievementsFinished[i];
        }
    }

    [ExecuteInEditMode]
    public void SetDefaultSaves()
    {
        _defaultSavesActivated = true;

        PlayerPrefs.SetInt("GameManager.Instance.Food", GameManager.Instance.startFood);
        PlayerPrefs.SetInt("GameManager.Instance.Gems", GameManager.Instance.startGems);
        PlayerPrefs.SetInt("MinionsManager.Instance.TotalMinions", 0);
        PlayerPrefs.SetFloat("Princess.Instance.currPrice", Princess.Instance.startPrice);

        List<int> zonesUpgradesIndex = new List<int>();
        List<int> zonesCurrPrice = new List<int>();
        List<int> minionsInZone = new List<int>();
        List<int> limitesIndex = new List<int>();
        List<bool> isFindGemsMaximum = new List<bool>();
        List<int> currGemsReward = new List<int>();
        List<float> addWorkerCurrFoodPrice = new List<float>();
        List<float> addWorkerCurrGemsPrice = new List<float>();
        List<int> addWorkerUpgradesCount = new List<int>();
        List<int> addWorkerCurrLevel = new List<int>();
        List<float> incLimitCurrFoodPrice = new List<float>();
        List<float> incLimitCurrGemsPrice = new List<float>();
        List<int> incLimitUpgradesCount = new List<int>();
        List<int> incLimitCurrLevel = new List<int>();
        List<float> foodPriceCurrFoodPrice = new List<float>();
        List<float> foodPriceCurrGemsPrice = new List<float>();
        List<int> foodPriceUpgradesCount = new List<int>();
        List<int> foodPriceCurrLevel = new List<int>();
        List<float> findGemsCurrFoodPrice = new List<float>();
        List<float> findGemsCurrGemsPrice = new List<float>();
        List<int> findGemsUpgradesCount = new List<int>();
        List<int> findGemsCurrLevel = new List<int>();
        List<float> walkingSpeedCurrFoodPrice = new List<float>();
        List<float> walkingSpeedCurrGemsPrice = new List<float>();
        List<int> walkingSpeedUpgradesCount = new List<int>();
        List<int> walkingSpeedCurrLevel = new List<int>();

        for (int i = 0; i < GameManager.Instance.zones.Count; i++)
        {
            zonesUpgradesIndex.Add(0);

            minionsInZone.Add(0);
            limitesIndex.Add(0);
            isFindGemsMaximum.Add(false);
            currGemsReward.Add(1);

            addWorkerCurrFoodPrice.Add(GameManager.Instance.zones[i].addWorkerPrice.startFoodPrice);
            addWorkerCurrGemsPrice.Add(GameManager.Instance.zones[i].addWorkerPrice.startGemsPrice);
            addWorkerUpgradesCount.Add(0);
            addWorkerCurrLevel.Add(1);

            incLimitCurrFoodPrice.Add(GameManager.Instance.zones[i].increaseHouseLimitPrice.startFoodPrice);
            incLimitCurrGemsPrice.Add(GameManager.Instance.zones[i].increaseHouseLimitPrice.startGemsPrice);
            incLimitUpgradesCount.Add(0);
            incLimitCurrLevel.Add(1);

            foodPriceCurrFoodPrice.Add(GameManager.Instance.zones[i].foodPriceValue.startFoodPrice);
            foodPriceCurrGemsPrice.Add(GameManager.Instance.zones[i].foodPriceValue.startGemsPrice);
            foodPriceUpgradesCount.Add(1);
            foodPriceCurrLevel.Add(1);

            findGemsCurrFoodPrice.Add(GameManager.Instance.zones[i].findGemsChancePrice.startFoodPrice);
            findGemsCurrGemsPrice.Add(GameManager.Instance.zones[i].findGemsChancePrice.startGemsPrice);
            findGemsUpgradesCount.Add(0);
            findGemsCurrLevel.Add(1);

            walkingSpeedCurrFoodPrice.Add(GameManager.Instance.zones[i].walkingSpeedPrice.startFoodPrice);
            walkingSpeedCurrGemsPrice.Add(GameManager.Instance.zones[i].walkingSpeedPrice.startGemsPrice);
            walkingSpeedUpgradesCount.Add(0);
            walkingSpeedCurrLevel.Add(1);
        }

        PlayerPrefs.SetInt("CatLevels.Instance.catLevel", 1);

        PlayerPrefs.SetInt("unlockHouseCurrLevel", 0);
        PlayerPrefs.SetInt("unlockHouseCurrValue", 0);

        PlayerPrefs.SetInt("foodMagnatCurrLevel", 0);
        PlayerPrefs.SetInt("foodMagnatCurrValue", 0);

        PlayerPrefs.SetInt("gemsMagnatHouseCurrLevel", 0);
        PlayerPrefs.SetInt("gemsMagnatCurrValue", 0);

        PlayerPrefs.SetInt("bornMinionsHouseCurrLevel", 0);
        PlayerPrefs.SetInt("bornMinionsCurrValue", 0);

        PlayerPrefs.SetInt("tapOnHouseCurrLevel", 0);
        PlayerPrefs.SetInt("tapOnHouseCurrValue", 0);

        List<bool> unlockedAchievements = new List<bool>();
        for (int i = 0; i < 10; i++)
            unlockedAchievements.Add(false);
        PlayerPrefsX.SetBoolArray("unlockedAchievements", unlockedAchievements.ToArray());

        List<bool> achievementsFinished = new List<bool>();
        for (int i = 0; i < 10; i++)
            achievementsFinished.Add(false);
        PlayerPrefsX.SetBoolArray("achievementsFinished", achievementsFinished.ToArray());

        PlayerPrefs.DeleteAll();
    }

    public void DeleteAllPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
    }
}
