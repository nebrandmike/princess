﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class SaveHelper : MonoBehaviour {

    public static SaveHelper Instance;

    public PlayerPrefsSaver playerPrefsSaver;

    public bool saveEachXSeconds = true;
    public float saveDurationSec = 180;

    public enum SaveType
    {
        Binary,
        PlayerPrefs
    }
    public SaveType saveType;

    void Awake()
    {
        Instance = this; 
    }

    void Start()
    {
        if (PlayerPrefs.GetInt("Tutorial") == 1)
        {
            LoadFromFile();
        }
        else
        {
            GameManager.Instance.SetStartFoodAndGems();
        }
    }

    #region SAVE

    void OnApplicationExit()
    {
        if (!defaultSavesEnabled)
        {
            if (PlayerPrefs.GetInt("Tutorial") == 1)
            {
                SaveToFile();
            }
        }
    }

    void OnApplicationQuit()
    {
        if (!defaultSavesEnabled)
        {
            if (PlayerPrefs.GetInt("Tutorial") == 1)
            {
                SaveToFile();
            }
        }
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus)
        {
            if (!defaultSavesEnabled && PlayerPrefs.GetInt("Tutorial") == 1)
            {
                SaveToFile();
            }
        }
        else
        {
            FyberManager.Instance.RequestVirtualCurrencyWithDelay();
        }
    }

    public void SaveToFile()
    {
        Debug.Log("save to file");
        if (saveType == SaveType.Binary)
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(Application.persistentDataPath + "/playerInfo2.dat");

            PlayerData data = new PlayerData();

            data.food = GameManager.Instance.Food;
            data.gems = GameManager.Instance.Gems;
            data.totalMinions = MinionsManager.Instance.TotalMinions();
            data.currBornMinionsPrice = Princess.Instance.currPrice;

            for (int i = 0; i < GameManager.Instance.zones.Count; i++)
            {
                data.upgradeHouseIndex[i] = GameManager.Instance.zones[i].upgradeIndex;

                data.price[i] = GameManager.Instance.zones[i].currPrice;
                data.minionsInZone[i] = GameManager.Instance.zones[i].minionsInZone;
                data.limitesIndex[i] = GameManager.Instance.zones[i].limitesIndex;
                data.isFindGemsMaximum[i] = GameManager.Instance.zones[i].isFindGemsMaximum;
                data.currGemsReward[i] = GameManager.Instance.zones[i].currGemsReward;
                data.catLevel = CatLevels.Instance.catLevel;

                data.addWorkerCurrFoodPrice[i] = GameManager.Instance.zones[i].addWorkerPrice.food;
                data.addWorkerCurrGemsPrice[i] = GameManager.Instance.zones[i].addWorkerPrice.gems;
                data.addWorkerUpgradesCount[i] = GameManager.Instance.zones[i].addWorkerPrice.upgradesCount;
                data.addWorkerCurrLevel[i] = GameManager.Instance.zones[i].addWorkerPrice.currLevel;

                data.incLimitCurrFoodPrice[i] = GameManager.Instance.zones[i].increaseHouseLimitPrice.food;
                data.incLimitCurrGemsPrice[i] = GameManager.Instance.zones[i].increaseHouseLimitPrice.gems;
                data.incLimitUpgradesCount[i] = GameManager.Instance.zones[i].increaseHouseLimitPrice.upgradesCount;
                data.incLimitCurrLevel[i] = GameManager.Instance.zones[i].increaseHouseLimitPrice.currLevel;

                data.foodPriceCurrFoodPrice[i] = GameManager.Instance.zones[i].foodPriceValue.food;
                data.foodPriceCurrGemsPrice[i] = GameManager.Instance.zones[i].foodPriceValue.gems;
                data.foodPriceUpgradesCount[i] = GameManager.Instance.zones[i].foodPriceValue.upgradesCount;
                data.foodPriceCurrLevel[i] = GameManager.Instance.zones[i].foodPriceValue.currLevel;

                data.findGemsCurrFoodPrice[i] = GameManager.Instance.zones[i].findGemsChancePrice.food;
                data.findGemsCurrGemsPrice[i] = GameManager.Instance.zones[i].findGemsChancePrice.gems;
                data.findGemsUpgradesCount[i] = GameManager.Instance.zones[i].findGemsChancePrice.upgradesCount;
                data.findGemsCurrLevel[i] = GameManager.Instance.zones[i].findGemsChancePrice.currLevel;

                data.walkingSpeedCurrFoodPrice[i] = GameManager.Instance.zones[i].walkingSpeedPrice.food;
                data.walkingSpeedCurrGemsPrice[i] = GameManager.Instance.zones[i].walkingSpeedPrice.gems;
                data.walkingSpeedUpgradesCount[i] = GameManager.Instance.zones[i].walkingSpeedPrice.upgradesCount;
                data.walkingSpeedCurrLevel[i] = GameManager.Instance.zones[i].walkingSpeedPrice.currLevel;

                data.foodValue[i] = GameManager.Instance.zones[i].foodValue;
            }

            data.unlockHouseCurrLevel = AchievementsManager.Instance.achievements[0].currLevel;
            data.unlockHouseCurrValue = AchievementsManager.Instance.achievements[0].currValue;

            data.foodMagnatCurrLevel = AchievementsManager.Instance.achievements[1].currLevel;
            data.foodMagnatCurrValue = AchievementsManager.Instance.achievements[1].currValue;

            data.gemsMagnatHouseCurrLevel = AchievementsManager.Instance.achievements[2].currLevel;
            data.gemsMagnatCurrValue = AchievementsManager.Instance.achievements[2].currValue;

            data.bornMinionsHouseCurrLevel = AchievementsManager.Instance.achievements[3].currLevel;
            data.bornMinionsCurrValue = AchievementsManager.Instance.achievements[3].currValue;

            data.tapOnHouseCurrLevel = AchievementsManager.Instance.achievements[4].currLevel;
            data.tapOnHouseCurrValue = AchievementsManager.Instance.achievements[4].currValue;

            data.unlockedAchievements = AchievementsManager.Instance.unlockedAchievements;
            data.achievementsFinished = AchievementsManager.Instance.achievementsFinished;

            bf.Serialize(file, data);
            file.Close();
        }
        else
        {
            PlayerPrefs.SetInt("PlayerPrefsSavesExist", 2);
            playerPrefsSaver.Save();
        }
    }

    #endregion

    #region LOAD

    void LoadFromFile()
    {
        if (saveType == SaveType.Binary)
        {
            Debug.Log("load from file");
            if (File.Exists(Application.persistentDataPath + "/playerInfo2.dat"))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(Application.persistentDataPath + "/playerInfo2.dat", FileMode.Open);
                PlayerData data = (PlayerData)bf.Deserialize(file);
                file.Close();

                GameManager.Instance.Food = data.food;
                GameManager.Instance.Gems = data.gems;
                Princess.Instance.LoadMinions(data.totalMinions);
                Princess.Instance.currPrice = data.currBornMinionsPrice;
                CatLevels.Instance.catLevel = data.catLevel;

                for (int i = 0; i < GameManager.Instance.zones.Count; i++)
                {
                    GameManager.Instance.zones[i].upgradeIndex = data.upgradeHouseIndex[i];

                    GameManager.Instance.zones[i].currPrice = data.price[i];
                    GameManager.Instance.zones[i].minionsInZone = data.minionsInZone[i];
                    GameManager.Instance.zones[i].limitesIndex = data.limitesIndex[i];
                    GameManager.Instance.zones[i].isFindGemsMaximum = data.isFindGemsMaximum[i];
                    GameManager.Instance.zones[i].currGemsReward = data.currGemsReward[i];

                    GameManager.Instance.zones[i].addWorkerPrice.food = data.addWorkerCurrFoodPrice[i];
                    GameManager.Instance.zones[i].addWorkerPrice.gems = data.addWorkerCurrGemsPrice[i];
                    GameManager.Instance.zones[i].addWorkerPrice.upgradesCount = data.addWorkerUpgradesCount[i];
                    GameManager.Instance.zones[i].addWorkerPrice.currLevel = data.addWorkerCurrLevel[i];

                    GameManager.Instance.zones[i].increaseHouseLimitPrice.food = data.incLimitCurrFoodPrice[i];
                    GameManager.Instance.zones[i].increaseHouseLimitPrice.gems = data.incLimitCurrGemsPrice[i];
                    GameManager.Instance.zones[i].increaseHouseLimitPrice.upgradesCount = data.incLimitUpgradesCount[i];
                    GameManager.Instance.zones[i].increaseHouseLimitPrice.currLevel = data.incLimitCurrLevel[i];

                    GameManager.Instance.zones[i].foodPriceValue.food = data.foodPriceCurrFoodPrice[i];
                    GameManager.Instance.zones[i].foodPriceValue.gems = data.foodPriceCurrGemsPrice[i];
                    GameManager.Instance.zones[i].foodPriceValue.upgradesCount = data.foodPriceUpgradesCount[i];
                    GameManager.Instance.zones[i].foodPriceValue.currLevel = data.foodPriceCurrLevel[i];

                    GameManager.Instance.zones[i].findGemsChancePrice.food = data.findGemsCurrFoodPrice[i];
                    GameManager.Instance.zones[i].findGemsChancePrice.gems = data.findGemsCurrGemsPrice[i];
                    GameManager.Instance.zones[i].findGemsChancePrice.upgradesCount = data.findGemsUpgradesCount[i];
                    GameManager.Instance.zones[i].findGemsChancePrice.currLevel = data.findGemsCurrLevel[i];

                    GameManager.Instance.zones[i].walkingSpeedPrice.food = data.walkingSpeedCurrFoodPrice[i];
                    GameManager.Instance.zones[i].walkingSpeedPrice.gems = data.walkingSpeedCurrGemsPrice[i];
                    GameManager.Instance.zones[i].walkingSpeedPrice.upgradesCount = data.walkingSpeedUpgradesCount[i];
                    GameManager.Instance.zones[i].walkingSpeedPrice.currLevel = data.walkingSpeedCurrLevel[i];

                    GameManager.Instance.zones[i].foodValue = data.foodValue[i];
                }

                AchievementsManager.Instance.achievements[0].currLevel = data.unlockHouseCurrLevel;
                AchievementsManager.Instance.achievements[0].currValue = data.unlockHouseCurrValue;

                AchievementsManager.Instance.achievements[1].currLevel = data.foodMagnatCurrLevel;
                AchievementsManager.Instance.achievements[1].currValue = data.foodMagnatCurrValue;

                AchievementsManager.Instance.achievements[2].currLevel = data.gemsMagnatHouseCurrLevel;
                AchievementsManager.Instance.achievements[2].currValue = data.gemsMagnatCurrValue;

                AchievementsManager.Instance.achievements[3].currLevel = data.bornMinionsHouseCurrLevel;
                AchievementsManager.Instance.achievements[3].currValue = data.bornMinionsCurrValue;

                AchievementsManager.Instance.achievements[4].currLevel = data.tapOnHouseCurrLevel;
                AchievementsManager.Instance.achievements[4].currValue = data.tapOnHouseCurrValue;

                AchievementsManager.Instance.unlockedAchievements = data.unlockedAchievements;
                AchievementsManager.Instance.achievementsFinished = data.achievementsFinished;
            }
            
        }
        else
        {
            if (PlayerPrefs.GetInt("PlayerPrefsSavesExist") == 0)
            {
                CheckIfOldSavesExist();
            }
            else
                playerPrefsSaver.Load(false);
        }
    }

    void CheckIfOldSavesExist()
    {
        Debug.Log("CheckIfOldSavesExist");

        if (File.Exists(Application.persistentDataPath + "/playerInfo2.dat"))
        {
            Debug.Log("old saves exist!");

            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/playerInfo2.dat", FileMode.Open);
            PlayerData data = (PlayerData)bf.Deserialize(file);
            file.Close();

            PlayerPrefs.SetInt("PlayerPrefsSavesExist", 2);

            GameManager.Instance.Food = data.food;
            GameManager.Instance.Gems = data.gems;
            Princess.Instance.LoadMinions(data.totalMinions);
            Princess.Instance.currPrice = data.currBornMinionsPrice;
            CatLevels.Instance.catLevel = data.catLevel;

            for (int i = 0; i < GameManager.Instance.zones.Count; i++)
            {
                GameManager.Instance.zones[i].upgradeIndex = data.upgradeHouseIndex[i];

                GameManager.Instance.zones[i].currPrice = data.price[i];
                GameManager.Instance.zones[i].minionsInZone = data.minionsInZone[i];
                GameManager.Instance.zones[i].limitesIndex = data.limitesIndex[i];
                GameManager.Instance.zones[i].isFindGemsMaximum = data.isFindGemsMaximum[i];
                GameManager.Instance.zones[i].currGemsReward = data.currGemsReward[i];

                GameManager.Instance.zones[i].addWorkerPrice.food = data.addWorkerCurrFoodPrice[i];
                GameManager.Instance.zones[i].addWorkerPrice.gems = data.addWorkerCurrGemsPrice[i];
                GameManager.Instance.zones[i].addWorkerPrice.upgradesCount = data.addWorkerUpgradesCount[i];
                GameManager.Instance.zones[i].addWorkerPrice.currLevel = data.addWorkerCurrLevel[i];

                GameManager.Instance.zones[i].increaseHouseLimitPrice.food = data.incLimitCurrFoodPrice[i];
                GameManager.Instance.zones[i].increaseHouseLimitPrice.gems = data.incLimitCurrGemsPrice[i];
                GameManager.Instance.zones[i].increaseHouseLimitPrice.upgradesCount = data.incLimitUpgradesCount[i];
                GameManager.Instance.zones[i].increaseHouseLimitPrice.currLevel = data.incLimitCurrLevel[i];

                GameManager.Instance.zones[i].foodPriceValue.food = data.foodPriceCurrFoodPrice[i];
                GameManager.Instance.zones[i].foodPriceValue.gems = data.foodPriceCurrGemsPrice[i];
                GameManager.Instance.zones[i].foodPriceValue.upgradesCount = data.foodPriceUpgradesCount[i];
                GameManager.Instance.zones[i].foodPriceValue.currLevel = data.foodPriceCurrLevel[i];

                GameManager.Instance.zones[i].findGemsChancePrice.food = data.findGemsCurrFoodPrice[i];
                GameManager.Instance.zones[i].findGemsChancePrice.gems = data.findGemsCurrGemsPrice[i];
                GameManager.Instance.zones[i].findGemsChancePrice.upgradesCount = data.findGemsUpgradesCount[i];
                GameManager.Instance.zones[i].findGemsChancePrice.currLevel = data.findGemsCurrLevel[i];

                GameManager.Instance.zones[i].walkingSpeedPrice.food = data.walkingSpeedCurrFoodPrice[i];
                GameManager.Instance.zones[i].walkingSpeedPrice.gems = data.walkingSpeedCurrGemsPrice[i];
                GameManager.Instance.zones[i].walkingSpeedPrice.upgradesCount = data.walkingSpeedUpgradesCount[i];
                GameManager.Instance.zones[i].walkingSpeedPrice.currLevel = data.walkingSpeedCurrLevel[i];

                GameManager.Instance.zones[i].foodValue = data.foodValue[i];
            }

            AchievementsManager.Instance.achievements[0].currLevel = data.unlockHouseCurrLevel;
            AchievementsManager.Instance.achievements[0].currValue = data.unlockHouseCurrValue;

            AchievementsManager.Instance.achievements[1].currLevel = data.foodMagnatCurrLevel;
            AchievementsManager.Instance.achievements[1].currValue = data.foodMagnatCurrValue;

            AchievementsManager.Instance.achievements[2].currLevel = data.gemsMagnatHouseCurrLevel;
            AchievementsManager.Instance.achievements[2].currValue = data.gemsMagnatCurrValue;

            AchievementsManager.Instance.achievements[3].currLevel = data.bornMinionsHouseCurrLevel;
            AchievementsManager.Instance.achievements[3].currValue = data.bornMinionsCurrValue;

            AchievementsManager.Instance.achievements[4].currLevel = data.tapOnHouseCurrLevel;
            AchievementsManager.Instance.achievements[4].currValue = data.tapOnHouseCurrValue;

            AchievementsManager.Instance.unlockedAchievements = data.unlockedAchievements;
            AchievementsManager.Instance.achievementsFinished = data.achievementsFinished;


#if UNITY_EDITOR
            File.Delete(Application.persistentDataPath + "/playerInfo2.dat");
#endif
        }
        else
        {
            PlayerPrefs.SetInt("PlayerPrefsSavesExist", 2);
            playerPrefsSaver.Load(true);
        }
    }

    #endregion

    #region SET DEFAULT SAVES

    [SerializeField] bool defaultSavesEnabled = false;
    [ExecuteInEditMode]
    public void SetDefaultSaves()
    {
        defaultSavesEnabled = true;
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/playerInfo2.dat");

        PlayerData data = new PlayerData();

        data.food = GameManager.Instance.startFood;
        data.gems = GameManager.Instance.startGems;
        data.totalMinions = 0;
        data.currBornMinionsPrice = Princess.Instance.startPrice;
        data.foodValue = new float[] { 10, 20, 40, 80, 160, 320, 740 };

        for (int i = 0; i < GameManager.Instance.zones.Count; i++)
        {
            data.upgradeHouseIndex[i] = 0;

            data.minionsInZone[i] = 0;
            data.limitesIndex[i] = 0;
            data.isFindGemsMaximum[i] = false;
            data.currGemsReward[i] = 1;
            data.catLevel = 1;

            data.addWorkerCurrFoodPrice[i] = GameManager.Instance.zones[i].addWorkerPrice.startFoodPrice;
            data.addWorkerCurrGemsPrice[i] = GameManager.Instance.zones[i].addWorkerPrice.startGemsPrice;
            data.addWorkerUpgradesCount[i] = 0;
            data.addWorkerCurrLevel[i] = 1;

            data.incLimitCurrFoodPrice[i] = GameManager.Instance.zones[i].increaseHouseLimitPrice.startFoodPrice;
            data.incLimitCurrGemsPrice[i] = GameManager.Instance.zones[i].increaseHouseLimitPrice.startGemsPrice;
            data.incLimitUpgradesCount[i] = 0;
            data.incLimitCurrLevel[i] = 1;

            data.foodPriceCurrFoodPrice[i] = GameManager.Instance.zones[i].foodPriceValue.startFoodPrice;
            data.foodPriceCurrGemsPrice[i] = GameManager.Instance.zones[i].foodPriceValue.startGemsPrice;
            data.foodPriceUpgradesCount[i] = 1;
            data.foodPriceCurrLevel[i] = 1;

            data.findGemsCurrFoodPrice[i] = GameManager.Instance.zones[i].findGemsChancePrice.startFoodPrice;
            data.findGemsCurrGemsPrice[i] = GameManager.Instance.zones[i].findGemsChancePrice.startGemsPrice;
            data.findGemsUpgradesCount[i] = 0;
            data.findGemsCurrLevel[i] = 1;

            data.walkingSpeedCurrFoodPrice[i] = GameManager.Instance.zones[i].walkingSpeedPrice.startFoodPrice;
            data.walkingSpeedCurrGemsPrice[i] = GameManager.Instance.zones[i].walkingSpeedPrice.startGemsPrice;
            data.walkingSpeedUpgradesCount[i] = 0;
            data.walkingSpeedCurrLevel[i] = 1;
        }

        data.unlockHouseCurrLevel = 0;
        data.unlockHouseCurrValue = 0;

        data.foodMagnatCurrLevel = 0;
        data.foodMagnatCurrValue = 0;

        data.gemsMagnatHouseCurrLevel = 0;
        data.gemsMagnatCurrValue = 0;

        data.bornMinionsHouseCurrLevel = 0;
        data.bornMinionsCurrValue = 0;

        data.tapOnHouseCurrLevel = 0;
        data.tapOnHouseCurrValue = 0;

        data.unlockedAchievements = new bool[] { false, false, false, false, false, false, false, false, false, false };
        data.achievementsFinished = new bool[] { false, false, false, false, false, false, false, false, false, false };

        bf.Serialize(file, data);
        file.Close();

        playerPrefsSaver.SetDefaultSaves();
    }

    #endregion
}

[Serializable]
public class PlayerData
{
    public int food;
    public int gems;
    public int totalMinions;
    public float currBornMinionsPrice;
    public int catLevel;

    // zone script variables

    public int[] upgradeHouseIndex = { 0, 0, 0, 0, 0, 0, 0 };

    public int[] price = { 0, 0, 0, 0, 0, 0, 0 };
    public int[] minionsInZone = { 0, 0, 0, 0, 0, 0, 0 };
    public int[] limitesIndex = { 0, 0, 0, 0, 0, 0, 0 };
    public bool[] isFindGemsMaximum = { false, false, false, false, false, false, false };
    public int[] currGemsReward = { 0, 0, 0, 0, 0, 0, 0 };

    public float[] addWorkerCurrFoodPrice = { 0, 0, 0, 0, 0, 0, 0 };
    public float[] addWorkerCurrGemsPrice = { 0, 0, 0, 0, 0, 0, 0 };
    public int[] addWorkerUpgradesCount = { 0, 0, 0, 0, 0, 0, 0 };
    public int[] addWorkerCurrLevel = { 0, 0, 0, 0, 0, 0, 0 };

    public float[] incLimitCurrFoodPrice = { 0, 0, 0, 0, 0, 0, 0 };
    public float[] incLimitCurrGemsPrice = { 0, 0, 0, 0, 0, 0, 0 };
    public int[] incLimitUpgradesCount = { 0, 0, 0, 0, 0, 0, 0 };
    public int[] incLimitCurrLevel = { 0, 0, 0, 0, 0, 0, 0 };

    public float[] foodPriceCurrFoodPrice = { 0, 0, 0, 0, 0, 0, 0 };
    public float[] foodPriceCurrGemsPrice = { 0, 0, 0, 0, 0, 0, 0 };
    public int[] foodPriceUpgradesCount = { 0, 0, 0, 0, 0, 0, 0 };
    public int[] foodPriceCurrLevel = { 0, 0, 0, 0, 0, 0, 0 };

    public float[] findGemsCurrFoodPrice = { 0, 0, 0, 0, 0, 0, 0 };
    public float[] findGemsCurrGemsPrice = { 0, 0, 0, 0, 0, 0, 0 };
    public int[] findGemsUpgradesCount = { 0, 0, 0, 0, 0, 0, 0 };
    public int[] findGemsCurrLevel = { 0, 0, 0, 0, 0, 0, 0 };

    public float[] walkingSpeedCurrFoodPrice = { 0, 0, 0, 0, 0, 0, 0 };
    public float[] walkingSpeedCurrGemsPrice = { 0, 0, 0, 0, 0, 0, 0 };
    public int[] walkingSpeedUpgradesCount = { 0, 0, 0, 0, 0, 0, 0 };
    public int[] walkingSpeedCurrLevel = { 0, 0, 0, 0, 0, 0, 0 };

    public float[] foodValue = { 10, 20, 40, 80, 160, 320, 740 };

    // achievements
    public int unlockHouseCurrLevel;
    public int unlockHouseCurrValue;

    public int foodMagnatCurrLevel;
    public int foodMagnatCurrValue;

    public int gemsMagnatHouseCurrLevel;
    public int gemsMagnatCurrValue;

    public int bornMinionsHouseCurrLevel;
    public int bornMinionsCurrValue;

    public int tapOnHouseCurrLevel;
    public int tapOnHouseCurrValue;
    
    public bool[] unlockedAchievements = { false, false, false, false, false, false, false, false, false, false };
    public bool[] achievementsFinished = { false, false, false, false, false, false, false, false, false, false };
}
