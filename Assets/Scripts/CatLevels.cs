﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class CatLevels : MonoBehaviour {

    public static CatLevels Instance;

    public float shakeDuration = 1;
    public float shakeStrength = 1;
    public GameObject gemPrefab;
    public Transform gemsSpawnPos;

    public GameObject progressBarGO;
    public UnityEngine.UI.Image growProgressBarImg;

    public int catLevel = 1;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        Invoke("SetInitialLevel", 0.1f);
    }

    void SetInitialLevel()
    {
        catLevel = PlayerPrefs.GetInt("catLevel");
        if (catLevel == 0) catLevel = 1;
        float growSize = 0;
        for (int i = 0; i <= catLevel; i++)
        {
            //transform.DOScale(transform.localScale.x + 0.05f, 0);
            growSize += 0.025f;
        }
        transform.localScale = new Vector3(transform.localScale.x + growSize, transform.localScale.y + growSize, 1);
    }

    public void CheckCatLevel()
    {
        if ((MinionsManager.Instance.TotalMinions() + 1) % 5 == 0)
            StartCoroutine(Upgrade());
    }

    IEnumerator Upgrade()
    {
        yield return new WaitForSeconds(0.5f);
        Princess.Instance.canBorn = false;
        Invoke("GrowProgressBarDisplay", shakeStrength);
    }

    void GrowProgressBarDisplay()
    {
        progressBarGO.SetActive(true);
        growProgressBarImg.DOFillAmount(1, MinionsManager.Instance.TotalMinions());
        StartCoroutine(GrowEnded());
    }

    IEnumerator GrowEnded()
    {
        yield return new WaitForSeconds(MinionsManager.Instance.TotalMinions());
        transform.DOShakePosition(shakeDuration, shakeStrength);
        SoundManager.Instance.PlaySound(Sounds.CatUpgradedShake);
        yield return new WaitForSeconds(shakeDuration);
        SoundManager.Instance.PlaySound(Sounds.CatUpgradedBloop);
        transform.DOScale(transform.localScale.x + 0.025f, 0.5f);
        growProgressBarImg.DOFillAmount(0, 0);
        progressBarGO.SetActive(false);
        Princess.Instance.canBorn = true;
        UIManager.Instance.DisplayGem(gemsSpawnPos.position, 1);
        GameManager.Instance.Gems += 1;
        AchievementsManager.Instance.SendAchievement(AchievementName.GemsMagnat, 1, AchievementType.Countable);
        catLevel++;
        PlayerPrefs.SetInt("catLevel", catLevel);
    }
}
