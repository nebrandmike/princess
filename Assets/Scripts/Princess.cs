﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class Princess : MonoBehaviour {

    public static Princess Instance;

    public int startPrice = 25;
    [HideInInspector] public float currPrice = 25;

    public GameObject[] minionPrefabs;
    public Animator mountAnim;

    #region Born Variables
    public GameObject bornProgressBarGO;
    public Image bornProgressBarImg;
    public GameObject bornProgressBarLabel;
    public float bornTimer = 2.5f;
    [Header("must be true in release version")]
    public bool bornTimerEnabled = false;
    private bool _isBorn;
    [HideInInspector]
    public bool canBorn = true;
    #endregion

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        Invoke("UpdateMininosPrice", 0.1f);
    }

    void UpdateMininosPrice()
    {
        UIManager.Instance.UpdateMinionPrice();
    }

    /// <summary>
    /// Can we buy new minion or not
    /// </summary>
    /// <returns></returns>
    public bool MinionAvailable()
    {
        return GameManager.Instance.Food >= currPrice;
    }

    /// <summary>
    /// Price for current minion
    /// </summary>
    /// <returns></returns>
    public float CurrentMinionPrice()
    {
        return currPrice;
    }

    /// <summary>
    /// Called by click on princess sprite (2d sprite must contains BoxCollider2D component)
    /// </summary>
    void OnMouseDown()
    {
        TryToBuyMinion();
    }

    public void TryToBuyMinion()
    {
        //print("MinionAvailable() " + MinionAvailable() + "UIManager.Instance.AnyPanelOpened()" + UIManager.Instance.AnyPanelOpened() + "canBorn" + canBorn);

        // if we can create minion and upgrades panel doesn't open - let's do it
        if (MinionAvailable() && !UIManager.Instance.AnyPanelOpened() && canBorn)
        {
            if (!Tutorial.Instance.activated)
                OnClick_CreateMinion();
            else
            {
                if (Tutorial.Instance.canBornMinion())
                    OnClick_CreateMinion();
            }
        }
    }

    public void OnClick_CreateMinion()
    {
        if (!bornTimerEnabled)
        {
            UIManager.Instance.FishBubbleFadeOn(0.5f);
            GameManager.Instance.Food -= (int)CurrentMinionPrice();
            SoundManager.Instance.PlaySound(Sounds.Click);
            BornMinion();
        }
        else
        {
            if (!_isBorn && MinionsManager.Instance.TotalMinions() <= 100)
            {
                _isBorn = true;
                UIManager.Instance.FishBubbleFadeOn(0.5f);
                GameManager.Instance.Food -= (int)CurrentMinionPrice();
                transform.DOScale(transform.localScale.x + 0.075f, 0.2f).OnComplete(() =>
                    transform.DOScale(transform.localScale.x - 0.075f, 0.2f));

                SoundManager.Instance.PlaySound(Sounds.Click);
                bornProgressBarGO.SetActive(true);
                if (MinionsManager.Instance.TotalMinions() > 0)
                {
                    bornProgressBarImg.DOFillAmount(1, bornTimer * MinionsManager.Instance.TotalMinions());
                    Invoke("BornMinion", bornTimer * MinionsManager.Instance.TotalMinions());
                }
                else if (MinionsManager.Instance.TotalMinions() == 0)
                {
                    bornProgressBarImg.DOFillAmount(1, 1);
                    Invoke("BornMinion", 1);
                }
                CheckTutoursSteps();
            }
        }
    }

    void CheckTutoursSteps()
    {
        if (Tutorial.Instance.activated && Tutorial.Instance.stepsCounter == 1)
        {
            Tutorial.Instance.HideTutorialStep();
            Invoke("ShowTut", 1);
        }
        if (Tutorial.Instance.activated && Tutorial.Instance.stepsCounter == 3)
        {
            Tutorial.Instance.HideTutorialStep();
            Invoke("ShowTut", 1);
        }
    }

    void BornMinion()
    {
        UIManager.Instance.DisplayMinusFoodOnPrincess((int)CurrentMinionPrice());
        currPrice = (startPrice * Mathf.Pow(1.15f, MinionsManager.Instance.TotalMinions() + 1));
        UIManager.Instance.UpdateMinionPrice();
        Lean.LeanPool.Spawn(minionPrefabs[Random.Range(0, minionPrefabs.Length)]); // spawn minion
        SoundManager.Instance.PlaySound(Sounds.MinionBorn);
        
        bornProgressBarImg.DOFillAmount(0, 0);
        bornProgressBarGO.SetActive(false);
        UIManager.Instance.FishBubbleFadeOn(1);
        CatLevels.Instance.CheckCatLevel();
        AchievementsManager.Instance.SendAchievement(AchievementName.BornMinions, 1, AchievementType.Countable);
        _isBorn = false;
    }

    public void LoadMinions(int count)
    {
        for (int i = 0; i < count; i++)
        {
            Minion minion = Instantiate(minionPrefabs[Random.Range(0, minionPrefabs.Length)],
                MinionsManager.Instance.waypoints[Random.Range(0, MinionsManager.Instance.waypoints.Length)].position,
                Quaternion.identity).GetComponent<Minion>(); // spawn minion
        }
    }

    void ShowTut()
    {
        Tutorial.Instance.ShowTutorialStep();
    }

    /// <summary>
    /// Animation on princess mount (we are using animator component to play it)
    /// </summary>
    public void AnimateOpeningMount()
    {
        mountAnim.SetTrigger("start");
    }
}
