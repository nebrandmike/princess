﻿using UnityEngine;
using System.Collections;

public static class AnimatorExtensions {

    public static void goToStateIfNotAlreadyThere(this Animator self, int stateHash)
    {
        //if (self.GetCurrentAnimatorStateInfo(0).nameHash != stateHash)
        if (self.GetCurrentAnimatorStateInfo(0).fullPathHash != stateHash)
            self.Play(stateHash);
    }
}
