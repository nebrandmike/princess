﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Utilities
{
    private static System.Random rng = new System.Random();
    public static void Shuffle<T>(this IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

    public static string NiceFormat(float money)
    {
        string niceFormat = "";
        if(money >= 1000 && money < 1000000)
        {
            float newMoney = money / 1000;
            string str = string.Format("{0:F1}", newMoney);
            return str + "k";
        }
        else if(money >= 1000000)
        {
            float newMoney = money / 1000000;
            string str = string.Format("{0:F1}", newMoney);
            return str + "m";
        }
        return ((int)money).ToString();
    }
}
