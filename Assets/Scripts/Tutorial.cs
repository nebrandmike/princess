﻿using UnityEngine;
using System.Collections;

public class Tutorial : MonoBehaviour {

    public static Tutorial Instance;

    public GameObject[] stepsGO;
    public GameObject[] fingersGO;
    public int stepsCounter;
    public bool activated;
    public Canvas worldCanvas;
    public GameObject tapOnHouseGO, tapOnHouseFingerGO;
    public GameObject backgroundGO;
    public GameObject catMaskGO, houseMaskGO, upgradeBtnMaskGO, tutCompletedMaskGO, tutFilledMaskGO, hideBtnGO;
    public GameObject sendMinionsToWork_PanelGO;

    void Awake()
    {
        Instance = this;
        if (PlayerPrefs.GetInt("Tutorial") == 0 && !GameManager.Instance.skipTutorial)
        {
            activated = true;
            worldCanvas.sortingLayerName = "UI";
            worldCanvas.sortingOrder = 6;
            ShowTutorialStep();
        }
    }

    public void ShowTutorialStep()
    {
        houseMaskGO.SetActive(false);
        catMaskGO.SetActive(false);
        upgradeBtnMaskGO.SetActive(false);
        tutCompletedMaskGO.SetActive(false);
        tutFilledMaskGO.SetActive(false);
        hideBtnGO.SetActive(true);
        for (int i = 0; i < stepsGO.Length; i++)
        {
            if (stepsCounter == 0) houseMaskGO.SetActive(true);
            else if (stepsCounter == 1) catMaskGO.SetActive(true);
            else if (stepsCounter == 2)
            {
                houseMaskGO.SetActive(true);
                sendMinionsToWork_PanelGO.SetActive(true);
            }
            else if (stepsCounter == 3) catMaskGO.SetActive(true);
            else if (stepsCounter == 4) houseMaskGO.SetActive(true);
            else if (stepsCounter == 5)
            {
                houseMaskGO.SetActive(true);
                hideBtnGO.SetActive(false);
            }
            else if (stepsCounter == 6) upgradeBtnMaskGO.SetActive(true);
            else if (stepsCounter == 7) houseMaskGO.SetActive(true);
            else if (stepsCounter == 8) tutCompletedMaskGO.SetActive(true);

            stepsGO[i].SetActive(false);
            if (fingersGO[i] != null) fingersGO[i].SetActive(false);
        }
        stepsGO[stepsCounter].SetActive(true);
        if (fingersGO[stepsCounter] != null) fingersGO[stepsCounter].SetActive(true);
        if(stepsCounter != 0)
            SoundManager.Instance.PlaySound(Sounds.Panel);
    }

    public void HideTutorialStep()
    {
        for (int i = 0; i < stepsGO.Length; i++)
        {
            stepsGO[i].SetActive(false);
            if (fingersGO[i] != null) fingersGO[i].SetActive(false);
        }
        AnalyticsManager.Instance.TutourStepCompleted(stepsCounter);
        stepsCounter++;
        backgroundGO.SetActive(false);
        HideMasks();
    }

    public void OnClick_TutorialCompleted()
    {
        for (int i = 0; i < stepsGO.Length; i++)
        {
            stepsGO[i].SetActive(false);
            if (fingersGO[i] != null) fingersGO[i].SetActive(false);
        }
        activated = false;
        PlayerPrefs.SetInt("Tutorial", 1);
        GameManager.Instance.Gems += 5;
        AchievementsManager.Instance.SendAchievement(AchievementName.GemsMagnat, 5, AchievementType.Countable);
        worldCanvas.sortingLayerName = "Default";
        worldCanvas.sortingOrder = 7;
        SoundManager.Instance.PlaySound(Sounds.TutPlus5Gems);
        UIManager.Instance.OnPanelOpen(false);
        tutCompletedMaskGO.SetActive(false);
        hideBtnGO.SetActive(false);
    }

    public void TapOnHouseHint(bool display)
    {
        tapOnHouseGO.SetActive(display);
        tapOnHouseFingerGO.SetActive(display);
    }

    public bool canBornMinion()
    { 
        if (stepsCounter == 1) return true;
        if (stepsCounter == 2) return false;
        if (stepsCounter == 3)
        {
            if (!stepsGO[3].activeSelf)
                return false;
            else
                return true;
        }
        if (stepsCounter == 4) return false;
        if (stepsCounter == 5) return false;
        if (MinionsManager.Instance.FreeMinions() > 0) return true;

        return false;
    }

    void HideMasks()
    {
        catMaskGO.SetActive(false);
        houseMaskGO.SetActive(false);
        upgradeBtnMaskGO.SetActive(false);
        tutCompletedMaskGO.SetActive(false);
        if (stepsCounter != 3 && stepsCounter != 5)
        {
            if (stepsCounter < 8) tutFilledMaskGO.SetActive(true);
        }
        else
        {
            houseMaskGO.SetActive(true);
        }
    }
}
