﻿using UnityEngine;
using System.Collections;

public class VideoLogo : MonoBehaviour {

    public PlayVideo playVideo;
    public float videoDuration;
    public bool loadGameSceneOnEnd;
    public GameObject logoCanvasGO;
    public int howOftenDisplayLogoOnStart = 2;

    private int _playLogoOnStartCounter;
    private bool isPlaying;
    private bool _canStartVideo;

    void Awake()
    {
        _playLogoOnStartCounter = PlayerPrefs.GetInt("_playLogoOnStartCounter");
    }

    void Start()
    {
        if (GameManager.Instance.playLogo)
        {
            logoCanvasGO.SetActive(true);
            //Invoke("CanStartVideo", 2);
            if(_playLogoOnStartCounter == 0)
                CanStartVideo();
            else
            {
                if (_playLogoOnStartCounter % howOftenDisplayLogoOnStart == 0)
                    CanStartVideo();
                else
                    ShowGame();
            }
        }
        else
        {
            ShowGame();
        }
        _playLogoOnStartCounter++;
        PlayerPrefs.SetInt("_playLogoOnStartCounter", _playLogoOnStartCounter);
    }

    void CanStartVideo()
    {
        _canStartVideo = true;
    }

    void Update()
    {
        if(!Application.isShowingSplashScreen && GameManager.Instance.playLogo && _canStartVideo)
        {
            if(!isPlaying)
            {
                isPlaying = true;
                _canStartVideo = false;
                playVideo.PlayStreaming();
                //GetComponent<AudioSource>().Play();

                if (loadGameSceneOnEnd)
                    Invoke("LoadGameScene", videoDuration);
                else
                    Invoke("ShowGame", videoDuration);
            }
        }
    }

    void LoadGameScene()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Game");
    }  

    void ShowGame()
    {
        logoCanvasGO.SetActive(false);
        MusicManager.Instance.canPlay = true;
    }
}
