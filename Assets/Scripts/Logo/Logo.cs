﻿using UnityEngine;
using System.Collections;

public class Logo : MonoBehaviour {

    public GameObject[] steps;
    public float timeBetweenMinions = 0.5f;
    public float timeBetweenTextDisplay = 0.5f;
    public AudioClip[] sounds;

    void Start()
    {
        StartCoroutine(LogoCoroutine());
    }

    IEnumerator LogoCoroutine()
    {
        yield return new WaitForSeconds(timeBetweenMinions);
        steps[0].SetActive(true);
        GetComponent<AudioSource>().PlayOneShot(sounds[0]);
        yield return new WaitForSeconds(timeBetweenMinions);
        steps[1].SetActive(true);
        GetComponent<AudioSource>().PlayOneShot(sounds[1]);
        yield return new WaitForSeconds(timeBetweenMinions);
        steps[2].SetActive(true);
        GetComponent<AudioSource>().PlayOneShot(sounds[2]);
        yield return new WaitForSeconds(timeBetweenTextDisplay);
        steps[3].SetActive(true);
        GetComponent<AudioSource>().PlayOneShot(sounds[3]);
        Application.LoadLevel("Game");
    }
}
