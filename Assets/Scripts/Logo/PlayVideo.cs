using UnityEngine;
using System.Collections;
using System.IO;

public class PlayVideo : MonoBehaviour
{
    private string streamingPath = string.Empty;
    private string urlStreamPath = string.Empty;

    private string streamingFileMp4Name = "test001.mp4";
    private string streamingFileTxtName = "Hello007.txt";

    void Start()
    {
#if UNITY_EDITOR
        streamingPath = Application.dataPath + "/StreamingAssets/";
#elif UNITY_ANDROID
        streamingPath = "jar:file://"+ Application.dataPath + "!/assets/";
#elif UNITY_IOS
        streamingPath = "file:" + Application.dataPath + "/Raw/";
#else
        //Desktop (Mac OS or Windows)
        streamingPath = Application.dataPath + "/StreamingAssets/";
#endif


#if UNITY_EDITOR
        urlStreamPath = "file:///" + this.streamingPath;
#elif UNITY_ANDROID
        urlStreamPath = this.streamingPath;
#elif UNITY_IOS
        urlStreamPath = this.streamingPath;
#else
        //Desktop (Mac OS or Windows)
        urlStreamPath = "file:///" + this.streamingPath;
#endif

    }

//    void OnGUI()
//    {
//        if (GUI.Button(new Rect(10, 10, 100, 50), "CopyToLocal"))
//        {
//            string fileURL = this.urlStreamPath + streamingFileMp4Name;
//            StartCoroutine(_CopyMP4File(fileURL));
//            StartCoroutine(CopyTxtFileExample());
//        }

//        if (GUI.Button(new Rect(10, 70, 100, 50), "Play"))
//        {
//            string videoPath = Application.persistentDataPath + "/" + "test001.mp4";
//#if UNITY_IOS
//            videoPath = "file://" + videoPath;
//#endif
//            Handheld.PlayFullScreenMovie(videoPath, Color.black, FullScreenMovieControlMode.CancelOnInput);
//            Debug.Log("!! play video with local file path: " + videoPath);
//            // 在安卓上面可以直接播放本地文件
//            // 在IOS上面需要指定file:// 
//            // 都能播放指定的://视频 http://101.199.231.42:8187/voice/hello.mp4
//            // Handheld.PlayFullScreenMovie()
//        }

//        if (GUI.Button(new Rect(10, 120, 100, 50), "PLayStreaming"))
//        {
//            Handheld.PlayFullScreenMovie("test001.mp4", Color.black, FullScreenMovieControlMode.CancelOnInput);
//            Debug.Log("!! play file on streaming file !!");
//        }

//        if (GUI.Button(new Rect(10, 200, 100, 50), "LoadLocalFile"))
//        {
//            string filePath = Application.persistentDataPath + "/" + streamingFileTxtName;
//            string content = System.IO.File.ReadAllText(filePath);
//            Debug.Log("Local txt file content:" + content);

//            string videoPath = Application.persistentDataPath + "/" + streamingFileMp4Name;
//            if (System.IO.File.Exists(videoPath))
//                Debug.Log("exist local file " + videoPath);
//        }
//    }

    public void OnClick_Full()
    {
        Handheld.PlayFullScreenMovie("test001.mp4", Color.black, FullScreenMovieControlMode.Full);
        Debug.Log("!! play file on streaming file !!");
    }

    public void OnClick_Cancel()
    {
        Handheld.PlayFullScreenMovie("test001.mp4", Color.black, FullScreenMovieControlMode.CancelOnInput);
        Debug.Log("!! play file on streaming file !!");
    }

    public void OnClick_Hidden()
    {
        Handheld.PlayFullScreenMovie("test001.mp4", Color.black, FullScreenMovieControlMode.Hidden);
        Debug.Log("!! play file on streaming file !!");
    }

    public void OnClick_Minimal()
    {
        Handheld.PlayFullScreenMovie("test001.mp4", Color.black, FullScreenMovieControlMode.Minimal);
        Debug.Log("!! play file on streaming file !!");
    }

    public void PlayStreaming()
    {
        Handheld.PlayFullScreenMovie("test001.mp4", Color.black, FullScreenMovieControlMode.Hidden);
        Debug.Log("!! play file on streaming file !!");
    }

    IEnumerator _CopyMP4File(string fileURL)
    {
        Debug.Log("[_CopyMP4File] URL: " + fileURL);
        WWW www = new WWW(fileURL);
        yield return www;
        string targetFile = Application.persistentDataPath + "/" + streamingFileMp4Name;
        // save file
        using (BinaryWriter writer = new BinaryWriter(File.Open(targetFile, FileMode.Create)))
        {
            writer.Write(www.bytes);
        }
        Debug.Log("[_CopyMP4File] !! save video to local file:" + targetFile);
    }

    void PlayCoolVideo(string videoPath)
    {
        Handheld.PlayFullScreenMovie(videoPath);
    }

    IEnumerator CopyTxtFileExample()
    {
        string filePath = urlStreamPath + streamingFileTxtName;
        Debug.Log("[CopyTxtFileExample]" + filePath);
        WWW www = new WWW(filePath);
        yield return www;
        Debug.Log("[CopyTxtFileExample] !! MyFile.txt content : " + www.text);
        // save to local file
        string localFile = Application.persistentDataPath + "/" + streamingFileTxtName;
        using (BinaryWriter writer = new BinaryWriter(File.Open(localFile, FileMode.Create)))
        {
            writer.Write(www.bytes);
        }
    }
}
