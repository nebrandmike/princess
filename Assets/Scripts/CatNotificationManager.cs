﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.SimpleAndroidNotifications;
using System;

public class CatNotificationManager : MonoBehaviour {

    public static CatNotificationManager Instance;

    public List<string> hours24Messages;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {

#if UNITY_ANDROID
        NotificationManager.CancelAll();
#endif

#if UNITY_IPHONE
        UnityEngine.iOS.NotificationServices.RegisterForNotifications(UnityEngine.iOS.NotificationType.Alert |
            UnityEngine.iOS.NotificationType.Badge | UnityEngine.iOS.NotificationType.Sound);

        UnityEngine.iOS.NotificationServices.ClearLocalNotifications();
        UnityEngine.iOS.NotificationServices.CancelAllLocalNotifications();
#endif
        Send24Hours();
        SendSevenDays();
    }

    void Send24Hours()
    {
        DateTime dt = DateTime.Now.AddHours(24);
        TimeSpan ts = dt - DateTime.Now;

#if UNITY_ANDROID
        NotificationManager.Send(ts, hours24Messages[UnityEngine.Random.Range(0, hours24Messages.Count)], "Princess Cat Nom Nom", new Color(1, 0.3f, 0.15f));
#endif

#if UNITY_IPHONE
        UnityEngine.iOS.LocalNotification notif = new UnityEngine.iOS.LocalNotification();
        notif.fireDate = DateTime.Now.AddSeconds(76800);
        notif.alertBody = hours24Messages[UnityEngine.Random.Range(0, hours24Messages.Count)];
        UnityEngine.iOS.NotificationServices.ScheduleLocalNotification(notif);
#endif
    }

    void SendSevenDays()
    {
        DateTime dt = DateTime.Now.AddDays(7);
        TimeSpan ts = dt - DateTime.Now;

#if UNITY_ANDROID
        NotificationManager.Send(ts, hours24Messages[UnityEngine.Random.Range(0, hours24Messages.Count)], "Princess Cat Nom Nom", new Color(1, 0.3f, 0.15f));
#endif

#if UNITY_IPHONE
        UnityEngine.iOS.LocalNotification notif = new UnityEngine.iOS.LocalNotification();
        notif.fireDate = DateTime.Now.AddSeconds(537600);
        notif.alertBody = hours24Messages[UnityEngine.Random.Range(0, hours24Messages.Count)];
        UnityEngine.iOS.NotificationServices.ScheduleLocalNotification(notif);
#endif
    }
}
