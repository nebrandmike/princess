﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Localization;

public class TestLocalization : MonoBehaviour {

    public LeanLocalization leanLocalization;
    public UnityEngine.UI.Text label;

    void Start()
    {
        print(LeanLocalization.CurrentLanguage);
    }

    public void OnClick_1()
    {
        if (LeanLocalization.GetTranslationText("phrase") != null)
            label.text = LeanLocalization.GetTranslationText("phrase");
        else
            label.text = "phrase 111";
    }

    public void OnClick_2()
    {
        if (LeanLocalization.GetTranslationText("any sentence") != null)
            label.text = LeanLocalization.GetTranslationText("any sentence");
        else
            label.text = "any sentence 111";
    }

    public void SetEng()
    {
        LeanLocalization.CurrentLanguage = "English";
    }

    public void SetRus()
    {
        LeanLocalization.CurrentLanguage = "Russian";
    }

    public void SetGerman()
    {
        LeanLocalization.CurrentLanguage = "German";
    }

    public void SetOtherLNG()
    {
        LeanLocalization.CurrentLanguage = "asdfasdf";
    }
}
