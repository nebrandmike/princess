﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(AmazonManager))]
public class AmazonManagerEditor : Editor {

    private AmazonManager amazonManager;

    private void OnEnable()
    {
        amazonManager = (AmazonManager)target;
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if(GUILayout.Button("Enable"))
        {
            amazonManager.EnableAmazon(true);
        }
        if(GUILayout.Button("Disable"))
        {
            amazonManager.EnableAmazon(false);
        }
    }
}
