﻿using UnityEditor;
using UnityEngine;

public class PlayerPrefsCleaner
{
    [MenuItem("Tools/ResetGame/Reset All")]
    public static void Clean()
    {
        PlayerPrefs.DeleteAll();
        SaveHelper.Instance.SetDefaultSaves();
        Debug.Log("PlayerPrefs Cleared");
    }
}
