﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(RescinManager))]
public class RescinsManagerEditor : Editor {

    public static RescinsManagerEditor Instance;

    private RescinManager rescinManager;

    private void OnEnable()
    {
        Instance = this;
        rescinManager = (RescinManager)target;
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        if (GUILayout.Button("Apply current rescin"))
        {
            rescinManager.ApplyCurrentRescin();

            PlayerSettings.productName = rescinManager.nomNomGamesList[(int)rescinManager.currentNomNomGame].productName;
            PlayerSettings.bundleIdentifier = rescinManager.nomNomGamesList[(int)rescinManager.currentNomNomGame].bundleID;

            List<Texture2D> texturesLst = new List<Texture2D>();
            texturesLst.Add(rescinManager.nomNomGamesList[(int)rescinManager.currentNomNomGame].icon.texture);
            PlayerSettings.SetIconsForTargetGroup(BuildTargetGroup.Unknown, texturesLst.ToArray());

#if UNITY_ANDROID    
            PlayerSettings.bundleVersion = rescinManager.nomNomGamesList[(int)rescinManager.currentNomNomGame].versionAndroid;
            PlayerSettings.Android.bundleVersionCode = rescinManager.nomNomGamesList[(int)rescinManager.currentNomNomGame].bundleVersionCodeAndroid;
#endif

#if UNITY_IOS
            PlayerSettings.bundleVersion = rescinsManager.evolutionGamesList[(int)rescinsManager.evolutionGame].versionIOS;
            PlayerSettings.iOS.buildNumber = rescinsManager.evolutionGamesList[(int)rescinsManager.evolutionGame].bundleVersionCodeIOS;
#endif
        }
    }
}
